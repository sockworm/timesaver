

#include <cmath>
#include <string>



#include <SDL.h>
#include <SDL_image.h>
// #include <SDL_ttf.h>

#include "constants.hh"


#include "LTexture.hh"

#include "trackWP.hh"


#include "trainCar.hh"
#include "dataDisplay.hh"


dataDisplay::dataDisplay() {

    // Initialize whatever
    
    // dimensions
    set_dimensions();
}


/**
 * Initialize with a traincar
 */
dataDisplay::dataDisplay(trainCar* tc) {
    
    // Set pointer to locomotive
    mytc = tc;
    
    // Set the dimensions
    set_dimensions();
    
}


/**
 * Set dimensions of data display
 */
void dataDisplay::set_dimensions() { 
    
    my_x = 0.1 * SCREEN_WIDTH;
    my_y = 0.8 * SCREEN_HEIGHT;
    
    my_w = 0.8 * SCREEN_WIDTH;
    my_h = 0.2 * SCREEN_HEIGHT;
    
    
}



/** 
 * Draw the throttle bar
 */
void dataDisplay::draw_throttle_bar(SDL_Renderer* rr, int barX, int barY, int barW, int barH) {
    
    
    
    
    
    // white border
    // ============
    
    SDL_SetRenderDrawColor(rr, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_Rect outer_box = {barX, barY, barW, barH};
    SDL_RenderFillRect(rr, &outer_box);
    
    
    // black background
    // ================
    
    SDL_SetRenderDrawColor(rr, 0x00, 0x00, 0x00, 0xFF);
    SDL_Rect inner_box = {barX + 5, barY + 5, barW - 10, barH - 10};
    SDL_RenderFillRect(rr, &inner_box);
    
    
    // for each notch
    int xpos = barX + 5;
    draw_throttle_bar_idle(rr, xpos, barY + 10, (barW - 10) / 9, barH - 20, mytc->getThrottle() == 0);
    xpos += (barW - 10) / 9;
    
    for (int incr_i = 1; incr_i <= 8; incr_i++) {
    
        
            
        draw_throttle_bar_notch(rr, xpos, barY + 10, (barW - 10) / 9, barH - 20, mytc->getThrottle() >= incr_i);
        
        xpos += (barW - 10) / 9;

        
    }
    
}


/**
 * Draw a single notch
 */
void dataDisplay::draw_throttle_bar_notch(SDL_Renderer* rr, int xx, int yy, int ww, int hh, bool isOn) {
    
    
    if (isOn) {
        SDL_SetRenderDrawColor(rr, 0x00, 0xFF, 0x00, 0xFF);
    }
    else {
        SDL_SetRenderDrawColor(rr, 0x00, 0x44, 0x00, 0xFF);
    }
    
    
    SDL_Rect notch_box = {xx + 2, yy, ww - 2, hh};
    SDL_RenderFillRect(rr, &notch_box);
    
    
    
}


/**
 * Draw the idle notch
 */
void dataDisplay::draw_throttle_bar_idle(SDL_Renderer* rr, int xx, int yy, int ww, int hh, bool isOn) {
    
    if (isOn) {
        SDL_SetRenderDrawColor(rr, 0xFF, 0x00, 0x00, 0xFF);
    }
    else {
        SDL_SetRenderDrawColor(rr, 0x44, 0x00, 0x00, 0xFF);
    }
    
    SDL_Rect notch_box = {xx + 2, yy, ww - 2, hh};
    SDL_RenderFillRect(rr, &notch_box);

}

/**
 * Draw the direction indicator
 */
void dataDisplay::draw_direction(SDL_Renderer* rr, int barX, int barY, int barW, int barH) {
    
//     char dir_label;
//     
//     if (mytc->getDirection()) {
//         // forward direction
//         dir_label = 'F';
//     }
//     else {
//         // reverse direction
//         dir_label = 'R';
//     }
//     
    
           // white border
    // ============
    
    SDL_SetRenderDrawColor(rr, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_Rect outer_box = {barX, barY, barW, barH};
    SDL_RenderFillRect(rr, &outer_box);
    
    
    // black background
    // ================
    
    SDL_SetRenderDrawColor(rr, 0x00, 0x00, 0x00, 0xFF);
    SDL_Rect inner_box = {barX + 5, barY + 5, barW - 10, barH - 10};
    SDL_RenderFillRect(rr, &inner_box);
    
    
    // FIXME: direction indicator uses a throttle_bar_notch display for now
    draw_throttle_bar_notch(rr, barX + 10, barY + 10, barW - 20, barH - 20, mytc->getDirection());
    
}




/** 
 * Draw the background rectangle
 */
void dataDisplay::draw_bg_rect(SDL_Renderer* rr) {
    
    // Set color to black
    SDL_SetRenderDrawColor(rr, 0x00, 0x00, 0x44, 0xAA);

    // Set rectangle to main dimensions
    SDL_Rect main_box = {my_x, my_y, my_w, my_h};
    
    // Draw rectangle
    SDL_RenderFillRect(rr, &main_box);
    
}



/** Draw a filled bar (like a simple progress bar)
 */
void dataDisplay::draw_filled_bar(SDL_Renderer* rr, int xx, int yy, int length, int height, double curval, double maxval) {
    
    // Error checking
    if (maxval <= 0.0) {
        return;
    }
    
    
    bool saturated = false;
    
    // calculate the integer value for the end of the filled part
    double dl = (double) length; // FIXME: alter this to leave room for numerical display
    int pc = (int) curval * dl / maxval;
    
    if (pc > length) {
        pc = length;
        saturated = true;
    }
    
    if (pc < 0) {
        pc = 0;
    }
    
    SDL_SetRenderDrawColor(rr, 0xFF, 0xFF, 0xFF, 0xFF);
    
    // Draw a border box
    SDL_Rect border_box = {my_x + xx, my_y + yy, length, height};
    SDL_Rect progress_box = {my_x + xx, my_y + yy, pc, height};
    
    SDL_RenderDrawRect(rr, &border_box);
    SDL_RenderFillRect(rr, &progress_box);
    
    if (saturated) {
        
        // FIXME: box doesn't render at the correct position
        
        int dl_10 = (int) dl / 10.0;
        int sat_pos = my_x + length - dl_10;
        
        SDL_SetRenderDrawColor(rr, 0xFF, 0x00, 0x00, 0xFF);
        SDL_Rect saturated_box = {sat_pos, my_y + yy, dl_10, height};
        
        SDL_RenderFillRect(rr, &saturated_box);
        
    }
    
}



/** Draw a filled bar with a center value
 */
/* FIXME: finish draw_filled_bar_center()
void dataDisplay::draw_filled_bar_center(SDL_Renderer* rr, int xx, int yy, int length, int height, double curval, double minval, double maxval, double centerval) {
    
    
    // Error checking
    if (minval > maxval) {
        return;
    }
    
    if (centerval < minval) {
        return;
    }
    
    if (centerval > maxval) {
        return;
    }
    
    bool saturated_min = false;
    bool saturated_max = false;
    
    double dl = (double) length; // FIXME: alter this to leave room for numerical display
    
    int pcenter = (int) (centerval - minval) * dl / (maxval - minval);
    
//     int length_L = length - pcenter;
//     int length_R = length - length_L; 
    
    pcenter += xx;

    
    // height offset
    double dheight = (double) height * 0.1;
    int height_offset = (int) dheight;
    
    // calculate the integer value for the end of the filled part

    int pc = 0;
    SDL_Rect progress_box;

    
    if (curval >= centerval) {
        
        pc = (int) xx + (curval - centerval) / (maxval - centerval);
        progress_box = {my_x + xx, my_y + yy + height_offset, pc, height -height_offset};

    }
    else {
        
        pc = (int) (curval - centerval) / (centerval - minval);
        progress_box = {};
    
    if (pc > length) {
        pc = length;
        saturated = true;
    }
    
    if (pc < 0) {
        pc = 0;
    }
    
    SDL_SetRenderDrawColor(rr, 0x00, 0x00, 0x00, 0xFF);
    
    // Draw a border box
    SDL_Rect border_box = {my_x + xx, my_y + yy + height_offset, length, height - height_offset};
    
    SDL_RenderDrawRect(rr, &border_box);
    SDL_RenderFillRect(rr, &progress_box);
    
    if (saturated) {
        
        // FIXME: box doesn't render at the correct position
        
        int dl_10 = (int) dl / 10.0;
        int sat_pos = my_x + length - dl_10;
        
        SDL_SetRenderDrawColor(rr, 0xFF, 0x00, 0x00, 0xFF);
        SDL_Rect saturated_box = {sat_pos, my_y + yy, dl_10, height};
        
        SDL_RenderFillRect(rr, &saturated_box);
        
    }
    
}*/


/** Draw the entire data display
 */
void dataDisplay::draw(SDL_Renderer* rr) {
    
    // draw the background frame
    draw_bg_rect(rr);
    
    
    // Display the train car's normal reaction forces (3,4,5,6)
//     draw_filled_bar(rr, 10, 10, 400, 20, mytc->params[3], 1000000.0);
//     draw_filled_bar(rr, 10, 40, 400, 20, mytc->params[4], 1000000.0);
//     draw_filled_bar(rr, 10, 70, 400, 20, mytc->params[5], 1000000.0);
//     draw_filled_bar(rr, 10, 100, 400, 20, mytc->params[6], 1000000.0);
    
//     draw_filled_bar(rr, 10, 130, 400, 20, abs(mytc->params[7] + mytc->params[8] + mytc->params[9] + mytc->params[10]) / 4.0, 2000.0);
    
    
    draw_throttle_bar(rr, my_x + 10, my_y + my_h - 60, 0.5 * my_w, 50);
    
    
    draw_direction(rr, 0.5 * my_w + my_x + 20, my_y + my_h - 60, 100, 50); 
    
    
}