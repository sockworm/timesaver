/* Track model
 * 
 * 
 * Daniel Montrallo Flickinger, PhD
 * 
 */

#define LSEGDATA cpt.a.x(), cpt.a.y(), cpt.b.x(), cpt.b.y(), cpt.c.x(), cpt.c.y(), cpt.r


#define dotp(ux,uy,vx,vy)  ((ux) * (vx) + (uy) * (vy))
#define norm(vx,vy)   sqrt(dotp(vx,vy,vx,vy))
#define dist(ux,uy,vx,vy)  norm(ux-vx, uy-vy)


#include<cmath>
#include<iostream>
#include<string>


#include <SDL.h>
#include <SDL_image.h>

#include <Box2D/Box2D.h>


#include "constants.hh"


#include "LTexture.hh"

#include "trackWP.hh"

#include "trainCar.hh"
#include "trackModel.hh"

#include "lseg_trim_helper.hh"

// First model tracks as a series of line segments
// then round out the corners with circular arcs
// then transition to cornu spirals


// Look at flux/appdev/simulation/trajgen/lseg_trim.m for trimming line segments with circular radii


// FIXME: can't overload << for track waypoints in trackWP.cc, must be done here!
std::ostream& operator<<(std::ostream& out, const trackWP& wp) {
    return out << "[" << wp.x() << ", " << wp.y()
               <<"] dir: [" <<  wp.dir_x() << ", " << wp.dir_y() << "]";
}



/* trackSegment
 * ------------
 */
trackSegment::trackSegment() {
    // Create a default track segment (1m straight at 0,0)
    
    
    Ctype = STRAIGHT;
    
    isActive = true;
    isTurnout = false;
    
    pt_start = trackWP(0.0, 0.0, 1.0, 0.0);
    pt_end = trackWP(1.0, 0.0, 1.0, 0.0);
    
    my_length = 0.0;
    
    start_angle = 0.0;
    end_angle = 0.0;
    
    pt_center = trackWP(0.5, 0.0, 1.0, 0.0);
    
    gamma = 0.0;
    
    LHRH = true;
    radius = 0.0;
    
    l = 0.0;
}

std::ostream& operator<<(std::ostream& out, const trackSegment& ts) {
    
    std::string mytype;
    
    if (STRAIGHT == ts.get_type()) {
        mytype = "straight";
    }
    
    if (CIRCULAR == ts.get_type()) {
        mytype = "circular arc";
    }
    
    if (CORNU == ts.get_type()) {
        mytype = "cornu spiral";
    }
    
    return out << "Track Segment: [" <<  mytype << "]" << std::endl
               << "Start: " << ts.startpoint() << ", "
               << "End: " << ts.endpoint() << std::endl;
}


void trackSegment::init(const trackWP& start_WP, const trackWP& end_WP, b2World* bw, bool make_active) {
    // Create a new track segment - straight segment between two waypoints

//     std::cout << "NEW TRACK SEGMENT (straight w/ 2 waypoints)" << std::endl;
    
    myWorld = bw;
    
    Ctype = STRAIGHT;
    
    isActive = make_active;
    isTurnout = false;
    
    pt_start = start_WP;
    pt_end = end_WP;
   
    // Calculate direction of segment
    trackWP wp_dir = pt_end - pt_start;
    wp_dir.calc_norm();
    wp_dir.move_to_dir();
    
    
    pt_start.copy_dir(wp_dir);
    pt_end.copy_dir(wp_dir);
    
//     std::cout << "Start: " << pt_start << std::endl
//               << "End: " << pt_end << std::endl;
   
    trackWP lvect = pt_end - pt_start;
    my_length = lvect.calc_distance();
              
    start_angle = atan2(pt_end.y() - pt_start.y(), pt_end.x() - pt_start.x());
    end_angle = start_angle;
    
//     pt_center.x = pt_start.x + (pt_end.x - pt_start.x) / 2.0;
//     pt_center.y = pt_start.y + (pt_end.y - pt_start.y) / 2.0;
    
    pt_center = trackWP(pt_start.x() + (pt_end.x() - pt_start.x()) / 2.0,
                        pt_start.y() + (pt_end.y() - pt_start.y()) / 2.0);
    pt_center.copy_dir(wp_dir);
    
    
    gamma = 0.0;
    LHRH = true;
    radius = 0.0;
    
    l = 0.0;
    
    
    buildRails();

    if (!isActive) {
        setInactive();
    }
    
}

/**
 * Initialize rail constraints
 */
void trackSegment::buildRails() {
    

    // translate points into box2d vectors
    b2Vec2 sp_b2(pt_start.x(), pt_start.y());
    b2Vec2 ep_b2(pt_end.x(), pt_end.y());
    b2Vec2 cp_b2(pt_center.x(), pt_center.y());
    
    b2BodyDef bd;

    
    
    if (STRAIGHT == Ctype) {
        
        
        b2EdgeShape edge;

        
        
        // Calculate direction
        b2Vec2 dir = ep_b2 - sp_b2;
        float dir_norm = sqrt(dir.x * dir.x + dir.y * dir.y);
    
        dir.Set(dir.x / dir_norm, dir.y / dir_norm);
        
        // rotate pi/2 to get segment normal direction
        dir.Set(-dir.y, dir.x);
        
        
    
        
        // build endpoints
        b2Vec2 sp_a(sp_b2.x + dir.x * (RAIL_WIDTH_2 + RAIL_EPS),
                    sp_b2.y + dir.y * (RAIL_WIDTH_2 + RAIL_EPS));
        b2Vec2 ep_a(ep_b2.x + dir.x * (RAIL_WIDTH_2 + RAIL_EPS),
                    ep_b2.y + dir.y * (RAIL_WIDTH_2 + RAIL_EPS));
        
        b2Vec2 sp_b(sp_b2.x - dir.x * (RAIL_WIDTH_2 + RAIL_EPS),
                    sp_b2.y - dir.y * (RAIL_WIDTH_2 + RAIL_EPS));
        b2Vec2 ep_b(ep_b2.x - dir.x * (RAIL_WIDTH_2 + RAIL_EPS),
                    ep_b2.y - dir.y * (RAIL_WIDTH_2 + RAIL_EPS));
        
        // build edges
        rail_a = myWorld->CreateBody(&bd);
        edge.Set(sp_a, ep_a);
        rail_a->CreateFixture(&edge, 0.0f);
    
        rail_b = myWorld->CreateBody(&bd);
        edge.Set(sp_b, ep_b);
        rail_b->CreateFixture(&edge, 0.0f);
        
    }
    else if (CIRCULAR == Ctype) {
        
       
        b2ChainShape chain_a;
        b2ChainShape chain_b;
        
        
        const float eps_a = fabs(gamma / 100.0f);
        
        int idx_a = 0;
        b2Vec2 tmp_a[101];
        b2Vec2 tmp_b[101];
        
        b2Vec2 rad_init = sp_b2 - cp_b2; // initial radius vector
        float curve_radius = sqrt(rad_init.x * rad_init.x + rad_init.y * rad_init.y);
        b2Vec2 dir_init(rad_init.x / curve_radius, rad_init.y / curve_radius);
        
        b2Vec2 dirvec; // direction vector for current angle
        
        b2Vec2 radvec_a; // radius vector for current angle (left rail)
        b2Vec2 radvec_b; // radius vector for current angle (right rail)
        // at least with left handed curves

        
        if (LHRH) {
            
            if (gamma > eps_a) {
                for (float incr_a = 0.0; incr_a <= gamma; incr_a += eps_a) {
                
                    dirvec = calc_rotate(dir_init, incr_a);
                    
                    radvec_a.Set(dirvec.x * (curve_radius - RAIL_WIDTH_2 - RAIL_EPS),
                                 dirvec.y * (curve_radius - RAIL_WIDTH_2 - RAIL_EPS));
                    radvec_b.Set(dirvec.x * (curve_radius + RAIL_WIDTH_2 + RAIL_EPS),
                                 dirvec.y * (curve_radius + RAIL_WIDTH_2 + RAIL_EPS));
                    
                    tmp_a[idx_a] = cp_b2 + radvec_a;
                    tmp_b[idx_a] = cp_b2 + radvec_b;
                    ++idx_a;
               
                }
            }
        }
        else {
            
            if (gamma < -eps_a) {
                for (float incr_a = 0.0; incr_a >= gamma; incr_a -= eps_a) {
                
                    dirvec = calc_rotate(dir_init, incr_a);
                    radvec_a.Set(dirvec.x * (curve_radius - RAIL_WIDTH_2 - RAIL_EPS),
                                 dirvec.y * (curve_radius - RAIL_WIDTH_2 - RAIL_EPS));
                    radvec_b.Set(dirvec.x * (curve_radius + RAIL_WIDTH_2 + RAIL_EPS),
                                 dirvec.y * (curve_radius + RAIL_WIDTH_2 + RAIL_EPS));
                    
                    tmp_a[idx_a] = cp_b2 + radvec_a;
                    tmp_b[idx_a] = cp_b2 + radvec_b;
                    ++idx_a;
                }
                
            }
            
        }
        
                       
        // Create the left rail
        rail_a = myWorld->CreateBody(&bd);
        chain_a.CreateChain(tmp_a, idx_a);
        rail_a->CreateFixture(&chain_a, 0.0f);
        
        // Create the right rail
        rail_b = myWorld->CreateBody(&bd);
        chain_b.CreateChain(tmp_b, idx_a);
        rail_b->CreateFixture(&chain_b, 0.0f);
 
    }
        
    
    
}






/**
 * Initialize a curved segment
 */
void trackSegment::init_curved(const trackWP& sp, const trackWP& ep, double rad, double gam, bool lr, b2World* bw, bool make_active) {
    // Create a new track segment - circular arc segment between two waypoints

    myWorld = bw;

    
    Ctype = CIRCULAR;
    
    isActive = make_active;
    isTurnout = false;
    
    pt_start = sp;
    pt_end = ep;
    
    

    pt_center = sp;
    if (lr) {
        // Left hand curve
        pt_center.rotate(M_PI_2);
    }
    else {
        // right hand curve
        pt_center.rotate(-M_PI_2);
    }
    pt_center.travel(rad);
    pt_center.flip(); // direction points back to start point
    
    
    gamma = gam;
    
    LHRH = lr;
    radius = rad;
    
    
    // Calculate the length of this segment
    my_length = abs(gamma * radius);
    
    
    
    l = 0.0; // FIXME: deprecate l
    
    
    // ------------------------------------------------------------------------
    // Create edges in the b2d world

    buildRails();

    
    if (!isActive) {
        setInactive();
    }

    
  

}



/**
 * Quick function to rotate a box2D vector
 */
b2Vec2 trackSegment::calc_rotate(b2Vec2 in, float th) {
    // rotate a vector by angle theta
     
        
        
    b2Vec2 tmp(in.x * cos(th) - in.y * sin(th),
               in.x * sin(th) + in.y * cos(th));
        
    // normalize it, just in case
    float dist = sqrt(tmp.x * tmp.x + tmp.y * tmp.y);
        
    b2Vec2 out(tmp.x / dist, tmp.y / dist);
        
    return out;
}

/**
 * Calculate a curved track given a LH/RH, a radius, and a sweep angle
 */
trackWP trackSegment::calc_endpoint_curved(const trackWP& sp, bool LHRH, double tmp_radius, double gam) {

    // copy the start point
    trackWP ep = sp;
//     trackWP tmp = sp;
    
    
    double normal_rotation_angle = M_PI_2;
    
    if (!LHRH) {
        // right hand turn, change the angles
        normal_rotation_angle = -normal_rotation_angle;
        
        if (gam > 0.0) {
            // gamma should be negative for a RH curve!
            gam = -gam;
        }
    }
    
    // rotate to the normal direction
    ep.rotate(normal_rotation_angle);
    
    
    // move the temporary point to the curve center point
    ep.travel(tmp_radius);
    
    
    // flip the direction
    ep.flip();
    
    
    // rotate the point by sweep angle
    ep.rotate(gam);
    
    // travel to the endpoint
    ep.travel(tmp_radius);
    
    
    // rotate to the end direction
    ep.rotate(normal_rotation_angle);
    
    
    
    return ep;
    
}


/**
 * Create a standard 10m straight from a start point
 */
void trackSegment::init_straight_15(const trackWP& sp, b2World* bw, bool make_active) {
    
    // copy the start point, and travel it
    trackWP ep = sp;
    ep.travel(15.0);
    
    init(sp, ep, bw, make_active);
    
}


/**
 * Create a standard 20m straight from a start point
 */
void trackSegment::init_straight_20(const trackWP& sp, b2World* bw, bool make_active) {
    
    // copy the start point, and travel it
    trackWP ep = sp;
    ep.travel(20.0);
    
    init(sp, ep, bw, make_active);
    
}



/**
 * Create a standard 1/8 LH turn
 */
void trackSegment::init_curved_L8(const trackWP& sp, b2World* bw, bool make_active) {
    
    // Max the tracks match up with the standard 20 m straights
    // Minimum radius should be 145 m - 175 m
    // From CSX Transportation, Standard Specifications for the Design and Construction of Private Sidetracks, 2007
    // (can't fit that in this puzzle!)
    
//     double rad = 20.0 / cos(M_PI_4);
    double rad = 40.0 / cos(M_PI_4 / 2);
//     const double sweep_angle = M_PI_4;
    
    trackWP ep = calc_endpoint_curved(sp, true, rad, M_PI_4 / 2);
    
    
    init_curved(sp, ep, rad, M_PI_4 / 2, true, bw, make_active);
    
}


    
/**
 * Create a standard 1/8 RH turn
 */
void trackSegment::init_curved_R8(const trackWP& sp, b2World* bw, bool make_active) {
    
    // same as in L8:
//     double rad = 20.0 / cos(M_PI_4);
    double rad = 40.0 / cos(M_PI_4 / 2);
    
    trackWP ep = calc_endpoint_curved(sp, false, rad, M_PI_4 / 2);
    
    init_curved(sp, ep, rad, -M_PI_4 / 2, false, bw, make_active);
    
}






/**
 * Activate track segment
 */
void trackSegment::setActive() {
    
    isActive = true;
    
    rail_a->SetActive(true);
    rail_b->SetActive(true);

    
}


/**
 * Deactivate track segment
 */
void trackSegment::setInactive() {
    
    isActive = false;
    
    rail_a->SetActive(false);
    rail_b->SetActive(false);
    
    
}







/**
 * Calculate distance between track segment and a waypoint
 */
double trackSegment::calc_distance(const trackWP wp) {
    
    
    // FIXME: only works for a straight segment
    if (STRAIGHT == Ctype) {

    
        double v_x = pt_end.x() - pt_start.x();
        double v_y = pt_end.y() - pt_start.y();
    
        double w_x = wp.x() - pt_start.x();
        double w_y = wp.y() - pt_start.y();
    
        double d1 = dotp(w_x, w_y, v_x, v_y);
        if (d1 <= 0.0) {
            return dist(wp.x(), wp.y(), pt_start.x(), pt_start.y());
        }
    
        double d2 = dotp(v_x, v_y, v_x, v_y);
        if (d2 <= d1) {
            return dist(wp.x(), wp.y(), pt_end.x(), pt_end.y());
        }
    
        if (0 == d2) {
            // Degenerate line segment 0-0
            std::cout << "Warning zero length track segment!" << std::endl;
            return 0.0;
        }
    
        double e = d1 / d2;
    
        double e_x = pt_start.x() + e * v_x;
        double e_y = pt_start.y() + e * v_y;
    
        return dist(wp.x(), wp.y(), e_x, e_y);
    
    }
    else if (CIRCULAR == Ctype) {
        // FIXME: trackSegment::calc_distance does not support circular segments
        
//         std::cout << "ERROR: calc_distance, circular curves are not unsupported" << std::endl;
        
    }
    else if (CORNU == Ctype) {
        // FIXME: trackSegment::calc_distance does not support cornu spirals
        
//         std::cout << "ERROR: calc_distance, cornu sprials are not unsupported" << std::endl;
        
    }
    else {
        // FIXME: who knows what it is!
        std::cout << "ERROR: calc_distance, unsupported segment type" << std::endl;
    }
    
    
    return 0.0;
    
}

/**
 * Calculate the distance between a point and a track segment, projected perpendicular to the segment
 * wp - waypoint
 * wp_truck_a
 * wp_truck_b
 */
double trackSegment::calc_lateral_distance(const trackWP wp, const trackWP wp_truck_a, const trackWP wp_truck_b) {
   
    // FIXME: calc_lateral_distance only works for a straight segments
    // FIXME: upgrade this function to use waypoint direction data instead
   
    if (STRAIGHT == Ctype) {
        // Straight segment
    
        // Calculate scalar signed distance from the track segment to the waypoint
        double dist_num = (pt_start.y() - pt_end.y()) * wp.x() + 
                          (pt_end.x() - pt_start.x()) * wp.y() +
                          (pt_start.x() * pt_end.y() - pt_end.x() * pt_start.y());
                      
        double dist_denom = dist(pt_end.x(), pt_end.y(), pt_start.x(), pt_start.y());
    

    
        if (0 == dist_denom) {
            // Degenerate line segment 0-0 (who put that there??!)
            std::cout << "Warning zero length track segment!" << std::endl;
            return 0.0;
        }
    
        double dist_TS_wp = (dist_num / dist_denom);
    
    
        // Calculate the correct sign for the distance (flip the sign if the truck is oriented 180 degrees over)
    
        double w_x = wp_truck_b.x() - (wp_truck_a.x() - pt_start.x());
        double w_y = wp_truck_b.y() - (wp_truck_a.y() - pt_start.y());
    
        double v_x = pt_end.x() - pt_start.x();
        double v_y = pt_end.y() - pt_start.y();
    

    
        // calculate angle
        double th = atan2(v_x*w_y - w_x*v_y, v_x*w_x + v_y*w_y);
    
    //     if (th < 0.0) {
    //         // flip the sign
    //         dist_TS_wp = -dist_TS_wp;
    //     }
        
        
        if (0.0 == cos(th)) {
            return 0.0;
        }
        
        return dist_TS_wp / cos(th);
    
    }
    else if (CIRCULAR == Ctype) {
        // FIXME: does not support circular segments
        
        std::cout << "ERROR: calc_lateral_distance, circular curves are not unsupported" << std::endl;
        
    }
    else if (CORNU == Ctype) {
        // FIXME: does not support cornu spirals
        
        std::cout << "ERROR: calc_lateral_distance, cornu sprials are not unsupported" << std::endl;
        
    }
    else {
        // Who knows what the fuck it is!
        std::cout << "ERROR: calc_lateral_distance, unsupported segment type" << std::endl;
    }
    
    
    // Nothing to return
    return 0.0;
    
    
}




/**
 * Calculate the velocity between a point and a track segment, projected perpendicular to the segment
 * wp
 * vel
 */
double trackSegment::calc_lateral_velocity(const trackWP wp, const trackWP vel) {
    
    // Get closest point on segment to wp
    double tpos = calc_param_distance(wp);
    
    
    // project velocity on to lateral distance direction
    return calc_norm_projection(vel, tpos);
    
}





/** project a vector along a track segment
 */
double trackSegment::calc_projection(trackWP vect, double tpos) {

    trackWP ts_dir = calc_pt_from_param_distance(tpos);
//     ts_dir.calc_norm();    
    
    return dotp(vect.x(), vect.y(), ts_dir.dir_x(), ts_dir.dir_y());
        
}



/**
 * Calculate a project normal to the track segment
 */
double trackSegment::calc_norm_projection(trackWP vect, double tpos) {

    trackWP ts_dir = calc_norm_direction(tpos);
    
    return dotp(vect.x(), vect.y(), ts_dir.x(), ts_dir.y());
    
}





/** 
 * Calculate the direction normal to the track segment
 */
trackWP trackSegment::calc_norm_direction(double tpos) {
    
    trackWP ts_dir = calc_pt_from_param_distance(tpos);
    ts_dir.rotate(M_PI_2);
    
    return ts_dir;

    
}


/** Calculate the closest point on a segment
 */
trackWP trackSegment::calc_closest_point(trackWP wp) {
    
    trackWP wp_out;
    
    // FIXME: calculate closest point works only for straight segments
    
    
    if (STRAIGHT == Ctype) {

        // project pt_start to wp, and get the point on the segment
        wp_out = calc_pt_from_param_distance(calc_projection(wp - pt_start, 0.0));
        
    }
    else if (CIRCULAR == Ctype) {
        // FIXME: does not support circular segments
        
        std::cout << "ERROR: calc_closest_point, circular curves are not unsupported" << std::endl;
        
    }
    else if (CORNU == Ctype) {
        // FIXME: does not support cornu spirals
        
        std::cout << "ERROR: calc_closest_point, cornu sprials are not unsupported" << std::endl;
        
    }
    else {
        // FIXME: who knows what it is!
        std::cout << "ERROR: calc_closest_point, unsupported segment type" << std::endl;
    }
    
    
    return wp_out;
    
}
    
    
    
    
/** Calculate the distance along a track segment
 */
double trackSegment::calc_param_distance(trackWP wp) {
 
    trackWP pt_on_segment = calc_closest_point(wp);
    double out_dist = 0.0;
    
 
    if (STRAIGHT == Ctype) {
        
        // track point minus start point
        trackWP pt_dist = pt_on_segment - pt_start;
        out_dist = sqrt(pt_dist.x()*pt_dist.x() + pt_dist.y()*pt_dist.y());
        
    }
    else if (CIRCULAR == Ctype) {
        // FIXME: does not support circular segments
        
        std::cout << "ERROR: calc_param_distance, circular curves are not unsupported" << std::endl;
        
    }
    else if (CORNU == Ctype) {
        // FIXME: does not support cornu spirals
        
        std::cout << "ERROR: calc_param_distance, cornu sprials are not unsupported" << std::endl;
        
    }
    else {
        // FIXME: who knows what it is!
        std::cout << "ERROR: calc_param_distance, unsupported segment type" << std::endl;
    }
    
    return out_dist;
    
}



/** Calculate the point and direction corresponding to a distance along a track segment
 */
trackWP trackSegment::calc_pt_from_param_distance(const double tpos) {
    
    trackWP out_pt = pt_start;
    
    // If tpos is less than 0 or greater than length of segment, return here
    if (tpos <= 0.0) {
        return pt_start;
    }
    if (tpos > my_length) {
        return pt_end;
    }
    
    
    
    if (STRAIGHT == Ctype) {
        
        // multiply distance tpos by direction
        out_pt.travel(tpos);
        
    }
    else if (CIRCULAR == Ctype) {
        // FIXME: does not support circular segments
        
        out_pt = pt_center;
        
        // sweep angle to tpos
        double gamma_tpos = tpos / radius;
        double normal_rotation_angle = M_PI_2;
        
        if (!LHRH) {
            // right hand curve
            gamma_tpos = -gamma_tpos;
            normal_rotation_angle = -M_PI_2;
        }
            
  
            
        out_pt.rotate(gamma_tpos);
        out_pt.travel(radius);
        out_pt.rotate(normal_rotation_angle);
            
   
        
        
    }
    else if (CORNU == Ctype) {
        // FIXME: does not support cornu spirals
        
//         std::cout << "ERROR: calc_pt_from_param_distance, cornu sprials are not unsupported" << std::endl;
        
    }
    else {
        // FIXME: who knows what it is!
        std::cout << "ERROR: calc_pt_from_param_distance, unsupported segment type" << std::endl;
    }
    
    
    
    return out_pt;
    
    
}




/** 
 * Draw a track segment
 * tie texture, and ballast texture
 */
void trackSegment::draw(SDL_Renderer* rr, LTexture* TT, LTexture* BT) {
    
    // rail width
//     const double rw = 0.71755; // m
    
    
    // Draw the ballast and ties first
    if (!isTurnout) {
        draw_ties(rr, TT, BT);
    }
    
    if (isActive) {
        SDL_SetRenderDrawColor(rr, 0x00, 0x00, 0x00, 0xFF);
    }
    else {
        SDL_SetRenderDrawColor(rr, 0xFF, 0x00, 0x00, 0xFF);
    }
    
    
    if (STRAIGHT == Ctype) {
        
        // Straight segments are easy
 
        
        // Get this segment's direction, and calculate the normal
        trackWP n_dir = calc_pt_from_param_distance(0.0);
        n_dir.set(0.0, 0.0);
        
        // Calculate normal direction
        n_dir.rotate(M_PI_2);
        
 
        
        n_dir.travel(RAIL_WIDTH_2);
        
        
 
        
        
        
        // left rail points
        trackWP LR_start = pt_start + n_dir;
        trackWP LR_end = pt_end + n_dir;
        
//         std::cout << "LR_start: " << LR_start
//                   << ",LR_end: " << LR_end << std::endl;
        
        // right rail points
        trackWP RR_start = pt_start - n_dir;
        trackWP RR_end = pt_end - n_dir;
        
        
//         std::cout << "RR_start: " << RR_start
//                   << "RR_end: " << RR_end << std::endl;
    
        SDL_RenderDrawLine(rr,
                           XtoSX(LR_start.x()),     
                           YtoSY(LR_start.y()),
                           XtoSX(LR_end.x()), 
                           YtoSY(LR_end.y()));
    
    
        SDL_RenderDrawLine(rr,
                           XtoSX(RR_start.x()),     
                           YtoSY(RR_start.y()),
                           XtoSX(RR_end.x()), 
                           YtoSY(RR_end.y()));
    
    }
    else if (CIRCULAR == Ctype) {
        
        // FIXME: need to draw circular curves
//         std::cout << "ERROR: drawing circular tracks not implemented." << std::endl;
        
        double eps_a = 0.01; // small epsilon value

        // FIXME: find a faster way to parameterize a curved segment
        if (LHRH) {
            // left handed curve
            
            if (gamma > eps_a) {
                for (double incr_a = 0.0; incr_a <= gamma - eps_a; incr_a += eps_a) {
                    draw_rail_eps(rr, incr_a, eps_a);
                }
            }
            else {
                // problems!
                std::cout << "WARNING: not drawing LH curved track segment because sweep angle is either too small or negative." << std::endl;
            }
        }
        else {
            // right handed curve
            
            if (gamma < -eps_a) {
                for (double incr_a = 0.0; incr_a >= gamma + eps_a; incr_a -= eps_a) {
                    draw_rail_eps(rr, incr_a, eps_a);
                }
            }
            else {
                // problems!
                std::cout << "WARNING: not drawing RH curved track segment because sweep angle is either too small or positive." << std::endl;
            }
        }
        
        
        
    }
    else if (CORNU == Ctype) {
        
        // FIXME: need to draw cornu spirals
//         std::cout << "ERROR: drawing cornu spiral tracks not implemented." << std::endl;
        
    }
    
    

    
//     std::cout << "Drawing track segment: " << pt_start << " -> " << pt_end << std::endl;
    
//     std::cout << "   (Screen positions: ["
//               << XtoSX(pt_start.x()) << ", " << YtoSY(pt_start.y()) << "] -> ["
//               << XtoSX(pt_end.x()) << ", " << YtoSY(pt_end.y()) << "]" << std::endl;
    
}




/** 
 * draw ties
 * tie texture, ballast texture
 */
void trackSegment::draw_ties(SDL_Renderer* rr, LTexture* TT, LTexture* BT) {
   
    
    
    SDL_RendererFlip flipType = SDL_FLIP_NONE;
    
    SDL_Rect clipR = {0, 0, TT->getWidth(), TT->getHeight()};
    SDL_Point centerR = {TT->getWidth() / 2, TT->getHeight() / 2};

    SDL_Rect clipR_b = {0, 0, BT->getWidth(), BT->getHeight()};
    SDL_Point centerR_b = {BT->getWidth() / 2, BT->getHeight() / 2};
    
    

    double tpos = 0.0;
    for (tpos = TIE_PITCH; tpos < my_length; tpos += TIE_PITCH) {
        
        // TEXTURE:
        
        // get point and normal direction at tpos on segment
        trackWP n_pt = calc_pt_from_param_distance(tpos);
        n_pt.rotate(M_PI_2);
    
        // ballast
        BT->render(rr,
                   XtoSX(n_pt.x()) - centerR_b.x,
                   YtoSY(n_pt.y()) - centerR_b.y,
                   &clipR_b,
                   n_pt.calc_degrees(),
                   &centerR_b,
                   flipType);
        
        // tie
        TT->render(rr,
                   XtoSX(n_pt.x()) - centerR.x,
                   YtoSY(n_pt.y()) - centerR.y,
                   &clipR,
                   n_pt.calc_degrees(),
                   &centerR,
                   flipType);
        

    }
    
    
}





/**
 * Draw a very short length of a rail segment
 */
void trackSegment::draw_rail_eps(SDL_Renderer* rr, double current_angle, double eps_angle) {



    trackWP tmp_a = pt_center;
    trackWP tmp_b = pt_center;
            
    tmp_a.rotate(current_angle);
    
    if (LHRH) {
        tmp_b.rotate(current_angle + eps_angle);
    }
    else {
        tmp_b.rotate(current_angle - eps_angle);
    }
            
    
            
    tmp_a.travel(radius - RAIL_WIDTH_2);
    tmp_b.travel(radius - RAIL_WIDTH_2);
            
    // draw the rail
    SDL_RenderDrawLine(rr,
                       XtoSX(tmp_a.x()),
                       YtoSY(tmp_a.y()),
                       XtoSX(tmp_b.x()),
                       YtoSY(tmp_b.y()));
            
            
    tmp_a.travel(2 * RAIL_WIDTH_2);
    tmp_b.travel(2 * RAIL_WIDTH_2);
            
    // draw the other rail
    SDL_RenderDrawLine(rr,
                       XtoSX(tmp_a.x()),
                       YtoSY(tmp_a.y()),
                       XtoSX(tmp_b.x()),
                       YtoSY(tmp_b.y()));
    
    
    
}





/*
 * TRACK BUMPER
 */

/**
 * Default constructor
 */
trackBumper::trackBumper() {
    
    my_x = 0.0;
    my_y = 0.0;
    my_phi = 0.0;
    
    bumperBody = NULL;
    bumperTexture = NULL;
}



/**
 * Build a track bumper with position and angle, and add it to the world
 */
trackBumper::trackBumper(b2World* m_world, b2Vec2 bp, float phi, LTexture* bt) {
    // Add a bumper at the specified location
        
    bumperTexture = bt;
    
    my_x = bp.x;
    my_y = bp.y;
    my_phi = phi;
    
    
    b2BodyDef bd;
    bd.type = b2_staticBody;
    bd.position = bp;
    bd.angle = phi;
        
    bumperBody = m_world->CreateBody(&bd);
        
    b2PolygonShape boxShape;
    boxShape.SetAsBox(BUMPER_DEPTH / 2, BUMPER_WIDTH / 2); 
        
    b2FixtureDef bumperFixtureDef;
    bumperFixtureDef.shape = &boxShape;
    bumperFixtureDef.restitution = 0.0f;
        
    bumperFixtureDef.filter.categoryBits = BUMPER;
        
    bumperBody->CreateFixture(&bumperFixtureDef);
        
}



/**
 * Draw a track bumper
 */
void trackBumper::draw(SDL_Renderer* rr) {
    
    SDL_RendererFlip flipType = SDL_FLIP_NONE;
    SDL_Rect clipR = {0, 0, bumperTexture->getWidth(), bumperTexture->getHeight()};
    SDL_Point centerR = {bumperTexture->getWidth() / 2, bumperTexture->getHeight() / 2};
    
    bumperTexture->render(rr,
                          XtoSX(my_x) - centerR.x,
                          YtoSY(my_y) - centerR.y,
                          &clipR,
                          (int)(-my_phi * 180.0 / M_PI),
                          &centerR,
                          flipType);
    
    
}









/*
 * TURNOUT
 */



turnout::turnout() {
    // FIXME: do something with this constructor   
    
    segA = NULL;
    segB = NULL;
    
}


/**
 * Create a new turnout from two track segments
 */
turnout::turnout(trackSegment* TS_a, trackSegment* TS_b) {
    
    // Set the pointers to each segment
    // FIXME: error checking needed!
    segA = TS_a;
    segB = TS_b;
    
    // FIXME: check to make sure each segment is not already in a turnout
    
    std::cout << "turnout::turnout, segment waypoints:" << std::endl
              << segA->startpoint() << std::endl
              << segA->endpoint() << std::endl
              << segB->startpoint() << std::endl
              << segB->endpoint() << std::endl;
    
    // Set each segment to be part of a turnout
    segA->enableTurnout();
    segB->enableTurnout();
              
    // find the intersection point of these two segments
    set_intersection();
    
    // set the point for the center of the clicking region
    set_click_area();
    
}

/**
 * Constructor with a overridden intersection point
 */
turnout::turnout(trackSegment* TS_a, trackSegment* TS_b, const trackWP& ip) {
    
    turnout(TS_a, TS_b);
    
    set_intersection(ip);
    
}




std::ostream& operator<<(std::ostream& out, const turnout& tto) {
    return out << "Turnout:" << std::endl
               << "Intersection: " << tto.getIntersection() << std::endl;

    
}


/**
 * Find the point where the two segments intersect
 */
void turnout::set_intersection() {
    
    // FIXME: finish set_intersection
    
    trackWP tmp;
    
    // find point common to each segment
    if ((segA->startpoint().x() == segB->startpoint().x()) &&
        (segA->startpoint().y() == segB->startpoint().y())) {
        tmp = segA->startpoint();
    }
    
    if ((segA->startpoint().x() == segB->endpoint().x()) &&
        (segA->startpoint().y() == segB->endpoint().y())) {
        tmp = segA->startpoint();
    }
    
    if ((segA->endpoint().x() == segB->startpoint().x()) &&
        (segA->endpoint().y() == segB->startpoint().y())) {
        tmp = segA->endpoint();
    }
    
    
    
    // FIXME: set direction normal to track segment A, towards segment B
    
    
    intersection = tmp;
    
}


/**
 * Set where the clickable area to flip the turnout is located
 */
void turnout::set_click_area() {
 
    // Assume that the intersection is already set
    
    
    // copy the intersection
    click_pt = intersection;
    
    // get the normal direction
    // FIXME: figure out if this is a RH or LH turnout
    click_pt.rotate(M_PI_2);
    
    // move out by the 1.5 m
    click_pt.travel(2.5);
    
    // FIXME: set click area in turnout::set_click_area()
    click_W = 20;
    click_H = 20;
    
    
}
    
    
/**
 * Check for state consistency and stuff
 */
void turnout::check_segments() {
    
    // Check if both segments have the same state (!)
    if (segA->Active() == segB->Active()) {
        // set A to active and B to inactive by default
        segA->setActive();
        segB->setInactive();
    }
    
    // FIXME: check other things in turnout::check_segments() ?
    
    // FIXME: check that there is an intersection point
    
    // FIXME: check that directions of the common end points match
    // (if both segments are straight, let it slide for now!)
    
    
 
}
    
    
/**
 * Flip the turnout (swap the active/inactive segments)
 */
void turnout::flip() {


    // Check the state, just in case
    check_segments();
    
    
   
    // Do this the long way, just to be sure that state stays consistent!
    if (segA->Active()) {
        segA->setInactive();
        segB->setActive();
    }
    else {
        segA->setActive();
        segB->setInactive();
    }
    
    
}



/**
 * check if a point is within the bounds of the switch (click area) for this turnout
 */
bool turnout::check_click(double xp, double yp) {
    
    if ((xp >= click_pt.x() - click_W / SCREEN_SCALE) &&
        (xp <= click_pt.x() + click_W / SCREEN_SCALE)) {
        
        if ((yp >= click_pt.y() - click_H / SCREEN_SCALE) &&
            (yp <= click_pt.y() + click_H / SCREEN_SCALE)) {
            
            return true;
        }
        
    }
    
    return false;    
    
}



/**
 * Draw the turnout
 */
void turnout::draw(SDL_Renderer* rr) {

    // FIXME: draw the line out to the clickable area (turnout::draw())
    
    // FIXME: draw something in the clickable area (turnout::draw())
    
    // Assume segment A is the straight track, and B is the divergent track
    if (segA->Active()) {
        SDL_SetRenderDrawColor(rr, 0xFF, 0xFF, 0x00, 0xFF);
    }
    else {
        SDL_SetRenderDrawColor(rr, 0xFF, 0x00, 0x00, 0xFF);
    }
    
    // FIXME: turnout rectangle defaults to 20 x 20
    SDL_Rect myRect = {XtoSX(click_pt.x()) - click_W / 2, YtoSY(click_pt.y()) - click_H / 2, click_W, click_H};
    
    SDL_RenderFillRects(rr, &myRect, 1);
    
}









/*
 * TRACK
 */


track::track() {
    // create a new track
    
//     numWP = 0;
    numSegments = 0;
    numTurnouts = 0;
    numBumpers = 0;
    
    std::cout << "NEW TRACK (NULL)" << std::endl;
    
}


std::ostream& operator<<(std::ostream& out, const track& mt) {
    return out << "TRACK with " << mt.segment_count() << " segments and " << mt.turnout_count() << " turnouts." << std::endl;
}


void track::init(const trackWP& wp_start, const trackWP& wp_end, b2World* wl) {
    // create a new track, given an initial straight segment
    
    // set a pointer to the main world
    myWorld = wl;
    
    std::cout << "NEW TRACK" << std::endl;
    
//     TW[0] = wp_start;
//     TW[1] = wp_end;
    
    
    std::cout << "Waypoint 0: " << wp_start << std::endl
              << "Waypoint 1: " << wp_end << std::endl;
//     numWP = 2;
    
    TS[0].init(wp_start, wp_end, wl, true);
    
//     temp_segment.build_straight(wp_start, wp_end);
//     TS[0] = temp_segment;
    
    numSegments = 1;
    numTurnouts = 0;
    numBumpers = 0;
   
    std::cout << "Creating new track with start at " << wp_start
              << " and end at " << wp_end << std::endl;
              
    std::cout << TS[0];
    
    std::cout << "(end of track::init())" << std::endl;
              
              
    
}





/**
 * Add a turnout by finding two segments that share a common point
 */
bool track::add_turnout(const trackWP& wp) {

    bool set_A = false;
    bool set_B = false;
    
    trackSegment *tsA;
    trackSegment *tsB;
    
    
    // go through list of segments, and find the ones with endpoint matching wp
    for (int incr_i = 0; incr_i < numSegments; incr_i++) {
        
        trackWP tmp_wp = TS[incr_i].startpoint();
        bool made_match = false;
        
        if ((tmp_wp.x() == wp.x()) && (tmp_wp.y() == wp.y())) {
            made_match = true;
        }
        
        tmp_wp = TS[incr_i].endpoint();
        
        if ((tmp_wp.x() == wp.x()) && (tmp_wp.y() == wp.y())) {
            made_match = true;
        }
   
        
        if (made_match) {
            
            if (set_A) {
                tsB = &TS[incr_i];
                set_B = true;
            }
            else {
                tsA = &TS[incr_i];
                set_A = true;
            }
            
        }
   
   
   
        // found both already (assume that more than two don't share waypoints)
        if (set_A && set_B) {
            
            
            
            // add the turnout
            TTO[numTurnouts] = {tsA, tsB};
            numTurnouts++;
            // FIXME: check for maximum turnouts
            
            std::cout << "Adding turnout: found two segments" << std::endl
                      << tsA->startpoint() << std::endl
                      << tsA->endpoint() << std::endl
                      << tsB->startpoint() << std::endl
                      << tsB->endpoint() << std::endl;
            
            return true;
        }
        
        
    }
    
    // Did not find both segments
    return false;
    
    
    
}



/**
 * Add a turnout, given two track indexes
 */
void track::add_turnout(int idx_a, int idx_b) {
    
    // Bounds checking
    if (idx_a < 0) {
        idx_a = 0;
    }
    
    if (idx_b < 0) {
        idx_b = 0;
    }
    
    if (idx_a > numSegments - 1) {
        idx_a = numSegments - 1;
    }
    
    if (idx_b > numSegments - 1) {
        idx_b = numSegments - 1;
    }
    
    
    std::cout << "Adding turnout with segments " << idx_a << " and " << idx_b << std::endl;
    
    std::cout << "DEBUG: data for segment a: " << std::endl << TS[idx_a] << std::endl;
    std::cout << "DEBUG: data for segment b: " << std::endl << TS[idx_b] << std::endl;
    
    std::cout << "DEBUG: current numTurnouts: " << numTurnouts << std::endl;
    
    TTO[numTurnouts] = {&TS[idx_a], &TS[idx_b]};
    numTurnouts++;
    
}



/**
 * Add a turnout, given two track indexes and an intersection point
 */
void track::add_turnout(int idx_a, int idx_b, const trackWP& ip) {
    
    // Bounds checking
    if (idx_a < 0) {
        idx_a = 0;
    }
    
    if (idx_b < 0) {
        idx_b = 0;
    }
    
    if (idx_a > numSegments - 1) {
        idx_a = numSegments - 1;
    }
    
    if (idx_b > numSegments - 1) {
        idx_b = numSegments - 1;
    }
    
    
    std::cout << "Adding turnout [override intersection point] with segments " << idx_a << " and " << idx_b << std::endl;
    
    std::cout << "Intersection point set to " << ip << std::endl;
    
    TTO[numTurnouts] = {&TS[idx_a], &TS[idx_b], ip};
    numTurnouts++;
    
}



/**
 * Flip a turnout
 */
void track::flipTurnout(int idx) {
    
    if (idx < 0) {
        return;
    }
    
    if (idx >= numTurnouts) {
        return;
    }
    
    TTO[idx].flip();
    
}


/**
 * Find the turnout nearest the given position
 */
int track::findTurnout(double xpos, double ypos) {

    int idx = -1;
    
    for (int incr_t = 0; incr_t < numTurnouts; ++incr_t) {
        
        if (TTO[incr_t].check_click(xpos, ypos)) {
            idx = incr_t;
            break;
        }
    }
    
    return idx;
    
    
}


/**
 * Add a bumper to the track
 */
void track::addBumper(b2World* bw, b2Vec2 bp, float phi, LTexture* bt) {
    
    // FIXME: error check
    
    myBumpers[numBumpers] = {bw, bp, phi, bt};
    ++numBumpers;
    
    
}







/**
 * Get a pointer to the nth from last segment
 */
trackSegment* track::get_last_segment_ptr_n(int ii) {
 
    if (ii < 0) {
        ii = 0;
    }
    
    int get_i = numSegments - ii;
    
    
    if (get_i < 0) {
        get_i = 0;
    }
    
    return &TS[numSegments - get_i];
    
}



/** 
 * Add a segment to the track
 */
void track::add_segment(const trackSegment& new_TS) {
    
//     TW[numWP] = new_TS.endpoint();
//     numWP++; 
    
    TS[numSegments] = new_TS;
    numSegments++;
    
    std::cout << "Adding new segment to track: " << TS[numSegments-1] << std::endl;
              
    
    
}



/**
 * Add an inactive segment to the track
 */
void track::add_segment_inactive(trackSegment& new_TS) {
    
    new_TS.setInactive();
    
    TS[numSegments] = new_TS;
    numSegments++;
    
}


/** 
 * Add a straight segment by waypoint
 */
void track::add_straight_segment_by_WP(const trackWP& TS_pt_end) {

    // FIXME: need to rewrite this function to get the waypoint at the end of the track
    // create a new segment:
//     this->add_segment(trackSegment(TW[numWP-1], TS_pt_end));
    
}


/**
 * Add a standard half length straight segment to the end of the track
 */
void track::add_straight_15(bool make_active) {
    
    trackSegment ts_tmp;
    
    ts_tmp.init_straight_15(get_endpoint(), myWorld, make_active);
    add_segment(ts_tmp);
    
}



/**
 * Add a standard straight segment to the end of the track
 */
void track::add_straight_20(bool make_active) {
    
    trackSegment ts_tmp;
    
    ts_tmp.init_straight_20(get_endpoint(), myWorld, make_active);
    add_segment(ts_tmp);
    
}


/**
 * Add a standard 1/8 LH turn to the end of the track
 */
void track::add_curved_L8(bool make_active) {
    
    trackSegment ts_tmp;
    
    ts_tmp.init_curved_L8(get_endpoint(), myWorld, make_active);
    add_segment(ts_tmp);
    
}


/**
 * Add a standard 1/8 LH turn to the end of the track
 */
void track::add_curved_R8(bool make_active) {
    
    trackSegment ts_tmp;
    
    ts_tmp.init_curved_R8(get_endpoint(), myWorld, make_active);
    add_segment(ts_tmp);
    
}
    
    
/**
 * Find the segment closest to a point
 */
int track::find_closest_segment(const trackWP& wp) {
    
    // If there's only one segment:
    if (1 == numSegments) {
        
        std::cout << "track::find_closest_segment(): only one segment, so segment 0 is closest." << std::endl;
        
        return 0;
    }
    
    
    int closest_segment_i = 0;
    double min_distance = TS[0].calc_distance(wp);
 
    
    
    for (int incr_i = 1; incr_i < numSegments; ++incr_i) {
        
        double temp_distance = TS[incr_i].calc_distance(wp);
        
        if (temp_distance < min_distance) {
            min_distance = temp_distance;
            closest_segment_i = incr_i;
        }
    }
     
     
//     std::cout << "track::find_closest_segment(): Segment " << closest_segment_i << " is the closest segment." << std::endl;
    return closest_segment_i;
}



/**
 * Return a pointer to the closest track segment to the given waypoint
 */
trackSegment* track::get_closest_segment(const trackWP& wp) {
    
    // FIXME: add error checking to get_closest_segment()
    
    // find the closest segment
    trackSegment* ts_out = &TS[find_closest_segment(wp)];
    return ts_out;
    
    
}




/**
 * Find the waypoint limits in this track
 */
void track::find_limits(double& Xmax, double& Xmin, double& Ymax, double& Ymin) {
    
    Xmax = -10000.0;
    Xmin = 10000.0;
    
    Ymax = -10000.0;
    Ymin = 10000.0;
    
    
    for (int incr_t = 0; incr_t < numSegments; ++incr_t) {
    
        double sp_x = TS[incr_t].startpoint().x();
        double sp_y = TS[incr_t].startpoint().y();
        
        double ep_x = TS[incr_t].endpoint().x();
        double ep_y = TS[incr_t].endpoint().y();
        
        if (sp_x > Xmax) {
            Xmax = sp_x;
        }
        
        if (sp_x < Xmin) {
            Xmin = sp_x;
        }
        
        if (ep_x > Xmax) {
            Xmax = ep_x;
        }
        
        if (ep_x < Xmin) {
            Xmin = ep_x;
        }
        
        
        
        if (sp_y > Ymax) {
            Ymax = sp_y;
        }
        
        if (sp_y < Ymin) {
            Ymin = sp_y;
        }
        
        if (ep_y > Ymax) {
            Ymax = ep_y;
        }
        
        if (ep_y < Ymin) {
            Ymin = ep_y;
        }
        
    }
        

    
}







/** Draw the track
 * 
 */
void track::draw(SDL_Renderer* rr, LTexture* TT, LTexture* BT) {
    
//     std::cout << "Drawing track with " << numSegments << " segments" << std::endl;
    
    // Draw each of the track segments
    for (int incr_i = 0; incr_i < numSegments; ++incr_i) {
        TS[incr_i].draw(rr, TT, BT);
        
//         std::cout << TS[incr_i] << std::endl;
    }
    
    
    
    
    
    // Draw each of the turnouts
    for (int incr_j = 0; incr_j < numTurnouts; ++incr_j) {
        TTO[incr_j].draw(rr);
        
    }
    
    
    
    // Draw each of the bumpers
    for (int incr_b = 0; incr_b < numBumpers; ++incr_b) {
        myBumpers[incr_b].draw(rr);
    }
    
}


/** Print data for the track
 */
void track::print() {
    
    std::cout << "(track::print()) Track with " << numSegments << " segments and " 
              << numTurnouts << " turnouts" << std::endl;
    
    
    for (int incr_i = 0; incr_i < numSegments; ++incr_i) {
        std::cout << TS[incr_i] << std::endl;
    }
    
    
    for (int incr_j = 0; incr_j < numTurnouts; ++incr_j) {
        std::cout << TTO[incr_j] << std::endl;
    }
    
//     std::cout << "WAIT HERE" << std::endl;
//     SDL_Delay(5000);
    
}




