// Daniel Montrallo Flickinger, PhD
// May 2014


// Train car class

#include <cmath>

// #include <stdio.h>
#include <iostream>
#include <string>
#include <tr1/array>
// FIXME: change from tr1/array to array, after getting C++11 working



#include <SDL.h>
#include <SDL_image.h>
// #include <SDL_gfx.h>

#include <Box2D/Box2D.h>


#include "constants.hh"

#include "LTexture.hh"


#include "trackWP.hh"


#include "trackModel.hh"
#include "trainCar.hh"

// CouplerContactListener class
//=============================================================================

// Callback for begin contact
void CouplerContactListener::BeginContact(b2Contact* contact) {
    
    num_contacts = 0;
        
    bool fixA_isCoupler = false;
    bool fixB_isCoupler = false;
        
    b2Fixture* fixtureA = contact->GetFixtureA();
    b2Fixture* fixtureB = contact->GetFixtureB();
        
    b2Filter filterA = fixtureA->GetFilterData();
    b2Filter filterB = fixtureB->GetFilterData();
        
    fixA_isCoupler = (COUPLER == filterA.categoryBits);
    fixB_isCoupler = (COUPLER == filterB.categoryBits);
        
    if (fixA_isCoupler && fixB_isCoupler) {
        // Make the couplers lock
            
        myFixture_A[num_contacts] = fixtureA;
        myFixture_B[num_contacts] = fixtureB;
            
        ++num_contacts;
            
    }
        
       

}




// trainCar class
//=============================================================================


// Constructors
// ------------

// FIXME: add texture as input to constructors


/** Initialize parameters, and set states to zero
 */
trainCar::trainCar() {

    throttle = 0; // Throttle setting to idle
    brake = 8; // brake setting to maximum
    pw_direction = true; // forward direction
    
    
    // Set parameters:
    // NOTE: sync these values from util/car.py
    cL = LOCOMOTIVE_BOLSTER_LENGTH; // car length
    tL = LOCOMOTIVE_TRUCK_LENGTH; // truck length
    
    cFL = LOCOMOTIVE_LENGTH; // full car length
    cW = LOCOMOTIVE_WIDTH; // car width
    
    cM = LOCOMOTIVE_MASS; // car mass (111130.0 kg total)
    tM = LOCOMOTIVE_TRUCK_MASS; // truck mass
    
    // ///

    // initialize goal box
    my_goal_pt = {0.0, 0.0, 0.0, 0.0};
    my_goal_l = 1.0;
    my_goal_w = 1.0;

    // Set color to white
    carColor_R = 0xFF;
    carColor_G = 0xFF;
    carColor_B = 0xFF;


}


// FIXME: clean up constructors for trainCar class

/** Initialize with body position
 */
trainCar::trainCar(double X, double Y) {
    
    trainCar();
 
    
}
    

/** Initialize with body position and rotation
 */
trainCar::trainCar(double X, double Y, double PHI) {
    
    trainCar(X, Y);
  

}


/** Initialize with all states
 */
trainCar::trainCar(double X, double Y, double PHI, double TA, double TB) {
    
    trainCar(X, Y, PHI);
  
}



// TODO: create constructor that places car in the center of a given track segment








/**
 * Get the track velocity
 */
double trainCar::getTrackVelocity() {

    return sqrt(myBody->GetLinearVelocity().x * myBody->GetLinearVelocity().x +
                myBody->GetLinearVelocity().y * myBody->GetLinearVelocity().y);
    
    
    
}





/**
 * Set the throttle position
 */
void trainCar::setThrottle(unsigned short int tp) {
    
    if (tp > 8) {
        throttle = 8;
    }
    else {
        throttle = tp;
    }
    
    // FIXME: more error checking!

}



/**
 * Set the brake setting
 */
void trainCar::setBrake(unsigned short int br) {
    
    
    // FIXME: more error checking!
    if (br > 8) {
        br = 8;
    }
    else {
        brake = br;
    }
    
}

/**
 * Flip the traction direction
 */
void trainCar::flipDirection() {
 
    pw_direction = !pw_direction;
    
}

 




/**
 * Set the traction force
 */
void trainCar::setTractionForce(double tf) {
    
    applyWheelForce(tf);
 
}



/**
 * Set the friction force
 */
void trainCar::setFrictionForce() {
    
    // Get current track velocity
    const double vel = getTrackVelocity();
    
 
}

/**
 * Apply forces at the wheels
 */
void trainCar::applyWheelForce(double mf) {
    
    // get car direction (in world)
    const float dir_x = cos(myBody->GetAngle());
    const float dir_y = sin(myBody->GetAngle());

    myBody->ApplyForce(b2Vec2(mf * dir_x, mf * dir_y), myBody->GetWorldCenter(), true);
    // FIXME: have the friction force apply to the wheels/trucks instead of the main body
    
}



/*

void trainCar::updateKinematics() {
    
    // POSITIONS:
    
    // calculate bolster positions:
    Ao.set(q[0] - (cL / 2.0) * cos(q[2]),
           q[1] - (cL / 2.0) * sin(q[2]));
    
    Bo.set(q[0] + (cL / 2.0) * cos(q[2]),
           q[1] + (cL / 2.0) * sin(q[2]));
    
    // FIXME: calculate and set bolster velocities
    
    // calculate axle centers:
    my_axles[0].set(Ao.x() - (tL / 2.0) * cos(q[3]) * cos(q[2]),
                    Ao.y() - (tL / 2.0) * sin(q[3]) * sin(q[2]));
    
    my_axles[1].set(Ao.x() + (tL / 2.0) * cos(q[3]) * cos(q[2]),
                    Ao.y() + (tL / 2.0) * sin(q[3]) * sin(q[2]));
    
    my_axles[2].set(Bo.x() - (tL / 2.0) * cos(q[4]) * cos(q[2]),
                    Bo.y() - (tL / 2.0) * sin(q[4]) * sin(q[2]));
    
    my_axles[3].set(Bo.x() + (tL / 2.0) * cos(q[4]) * cos(q[2]),
                    Bo.y() + (tL / 2.0) * sin(q[4]) * sin(q[2]));
    
    // VELOCITIES:
    // NOTE: expressions generated by util/car.py, then modified
    my_axles[0].set_vel((-sin(q[2])*cos(q[3]) - sin(q[3])*cos(q[2]))*(-(tL / 2.0)*q[7] - (tL / 2.0)*q[8]) + q[5] + (cL / 2.0) *q[7]*sin(q[2]),
                        (-sin(q[2])*sin(q[3]) + cos(q[2])*cos(q[3]))*(-(tL / 2.0)*q[7] - (tL / 2.0)*q[8]) + q[6] - (cL / 2.0) *q[7]*cos(q[2]));

    my_axles[1].set_vel((-sin(q[2])*cos(q[3]) - sin(q[3])*cos(q[2]))*((tL / 2.0)*q[7] + (tL / 2.0)*q[8]) + q[5] + (cL / 2.0) *q[7]*sin(q[2]),
                        (-sin(q[2])*sin(q[3]) + cos(q[2])*cos(q[3]))*((tL / 2.0)*q[7] + (tL / 2.0)*q[8]) + q[6] - (cL / 2.0) *q[7]*cos(q[2]));

    my_axles[2].set_vel((-sin(q[2])*cos(q[4]) - sin(q[4])*cos(q[2]))*(-(tL / 2.0)*q[7] - (tL / 2.0)*q[9]) + q[5] - (cL / 2.0) *q[7]*sin(q[2]),
                        (-sin(q[2])*sin(q[4]) + cos(q[2])*cos(q[4]))*(-(tL / 2.0)*q[7] - (tL / 2.0)*q[9]) + q[6] + (cL / 2.0) *q[7]*cos(q[2]));

    my_axles[3].set_vel((-sin(q[2])*cos(q[4]) - sin(q[4])*cos(q[2]))*((tL / 2.0)*q[7] + (tL / 2.0)*q[9]) + q[5] - (cL / 2.0) *q[7]*sin(q[2]),
                        (-sin(q[2])*sin(q[4]) + cos(q[2])*cos(q[4]))*((tL / 2.0)*q[7] + (tL / 2.0)*q[9]) + q[6] + (cL / 2.0) *q[7]*cos(q[2]));

    
    
}*/




/** 
 * Teleport the car somewhere, given desired axle center positions
 */
/*
void trainCar::teleportInverseKinematics(const double tAa_x, const double tAa_y,
                                         const double tAb_x, const double tAb_y,
                                         const double tBa_x, const double tBa_y,
                                         const double tBb_x, const double tBb_y) {
    
    // Set axle centers
    double Aa_x = tAa_x;
    double Aa_y = tAa_y;
    
    double Ab_x = tAb_x;
    double Ab_y = tAb_y;
    
    double Ba_x = tBa_x;
    double Ba_y = tBa_y;
    
    double Bb_x = tBb_x;
    double Bb_y = tBb_y;
    
    
    my_axles[0].set(Aa_x, Aa_y);
    my_axles[1].set(Ab_x, Ab_y);
    my_axles[2].set(Ba_x, Ba_y);
    my_axles[3].set(Bb_x, Bb_y);
    
    // Calculate the inverse kinematics (expressions from util/inverseKinematics.py)
    
    // Car center position
    q[0] = (Aa_x + Ab_x + Ba_x + Bb_x)/4;
    q[1] = (Aa_y + Ab_y + Ba_y + Bb_y)/4; 
     
    
    double ABPx = Aa_x + Ab_x - Ba_x - Bb_x;
    double ABPy = Aa_y + Ab_y - Ba_y - Bb_y;
    
    // Car rotation
    q[2] = atan2(-(ABPy)/(2*cL),
                 -(ABPx)/(2*cL));
 
    if (0.0 == ABPy) {
        // Singularity when the trucks are inline horizontally
        q[3] = 0.0;
        q[4] = 0.0;
    }
    else if (0.0 == ABPx) {
        // Singularity when the trucks are inline vertically
        q[3] = M_PI_2;
        q[4] = M_PI_2;
    }
    else {
        
        // Truck A
        q[3] = atan2(-2*cL*(Aa_y - Ab_y)/(tL*ABPy),
                     -2*cL*(Aa_x - Ab_x)/(tL*ABPx));
     
        // Truck B
        q[4] = atan2(-2*cL*(Ba_y - Bb_y)/(tL*ABPy),
                     -2*cL*(Ba_x - Bb_x)/(tL*ABPx));
        
    }
    
    
}*/




/** Enforce the velocity constraints, set generalized speeds to match contact point velocities
 */
// trainCar::enforce_velocity_constraints() {
//     // FIXME: add inputs
// 
// 
// // FIXME: search and replace sq...    
// q[5] = q[6] + vAa_x/4 - vAa_y/4 + vAb_x/4 - vAb_y/4 + vBa_x/4 - vBa_y/4 + vBb_x/4 - vBb_y/4;
// 
// q[8] = (2*cL*cq2*vAa_x - 2*cL*cq2*vAa_y - 2*cL*cq2*vAb_x + 2*cL*cq2*vAb_y + 2*cL*sq2*vAa_x - 2*cL*sq2*vAa_y - 2*cL*sq2*vAb_x + 2*cL*sq2*vAb_y - cq2*cq3*tL*vAa_x + cq2*cq3*tL*vAa_y - cq2*cq3*tL*vAb_x + cq2*cq3*tL*vAb_y + cq2*cq3*tL*vBa_x - cq2*cq3*tL*vBa_y + cq2*cq3*tL*vBb_x - cq2*cq3*tL*vBb_y - cq2*sq3*tL*vAa_x + cq2*sq3*tL*vAa_y - cq2*sq3*tL*vAb_x + cq2*sq3*tL*vAb_y + cq2*sq3*tL*vBa_x - cq2*sq3*tL*vBa_y + cq2*sq3*tL*vBb_x - cq2*sq3*tL*vBb_y - cq3*sq2*tL*vAa_x + cq3*sq2*tL*vAa_y - cq3*sq2*tL*vAb_x + cq3*sq2*tL*vAb_y + cq3*sq2*tL*vBa_x - cq3*sq2*tL*vBa_y + cq3*sq2*tL*vBb_x - cq3*sq2*tL*vBb_y + sq2*sq3*tL*vAa_x - sq2*sq3*tL*vAa_y + sq2*sq3*tL*vAb_x - sq2*sq3*tL*vAb_y - sq2*sq3*tL*vBa_x + sq2*sq3*tL*vBa_y - sq2*sq3*tL*vBb_x + sq2*sq3*tL*vBb_y)/(2*cL*tL*(cq2**2*cq3 + cq2**2*sq3 + 2*cq2*cq3*sq2 + cq3*sq2**2 - sq2**2*sq3));
// 
// q[7] = (vAa_x - vAa_y + vAb_x - vAb_y - vBa_x + vBa_y - vBb_x + vBb_y)/(2*cL*(cq2 + sq2));
// 
// q[9] = (-(2*tL*(vAa_x - vAa_y - vBa_x + vBa_y)*(cq2*cq3 + cq2*sq3 + cq3*sq2 - sq2*sq3) - (vAa_x - vAa_y - vAb_x + vAb_y)*(2*cL*cq2 + 2*cL*sq2 + cq2*cq3*tL - cq2*cq4*tL + cq2*sq3*tL - cq2*sq4*tL + cq3*sq2*tL - cq4*sq2*tL - sq2*sq3*tL + sq2*sq4*tL))*(2*cL*cq2 + 2*cL*sq2 + cq2*cq4*tL + cq2*sq4*tL + cq4*sq2*tL - sq2*sq4*tL) + (2*tL*(vAa_x - vAa_y - vBb_x + vBb_y)*(cq2*cq3 + cq2*sq3 + cq3*sq2 - sq2*sq3) - (vAa_x - vAa_y - vAb_x + vAb_y)*(2*cL*cq2 + 2*cL*sq2 + cq2*cq3*tL + cq2*cq4*tL + cq2*sq3*tL + cq2*sq4*tL + cq3*sq2*tL + cq4*sq2*tL - sq2*sq3*tL - sq2*sq4*tL))*(2*cL*cq2 + 2*cL*sq2 - cq2*cq4*tL - cq2*sq4*tL - cq4*sq2*tL + sq2*sq4*tL))/(4*cL*tL**2*(cq2 + sq2)*(cq2*cq3 + cq2*sq3 + cq3*sq2 - sq2*sq3)*(cq2*cq4 + cq2*sq4 + cq4*sq2 - sq2*sq4));
// 
//     
//     
// }



/** Calculate X position of car on screen
 */
int trainCar::calcSX() {

    // DEPRECATED: get the x position from internal states
//     return XtoSX(q[0]);
    
    // Get the x position from the car body:
    return XtoSX(myBody->GetPosition().x);
    
}





/** Calculate Y position of car on screen
 */
int trainCar::calcSY() {

    // DEPRECATED: get the y position from internal states
//     return YtoSY(q[1]);
    
    // Get the y position from the car body
    return YtoSY(myBody->GetPosition().y);
}




/** Calculate PHI of car on screen
 */
int trainCar::calcSP() {

    // DEPRECATED: get the rotation from internal states
//     return (int)(-q[2] * 180.0 / M_PI);
    
    // Get the rotation from the car body
    return (int)(-myBody->GetAngle() * 180.0 / M_PI);

}






/** Dump params: calculate tractive force
 * 
 */




/**
 * Draw this train car
 */
void trainCar::draw(SDL_Renderer* rr) {
    
    SDL_RendererFlip flipType = SDL_FLIP_NONE;

    // Render the couplers
    SDL_Rect clipR = {0, 0, couplerTexture->getWidth(), couplerTexture->getHeight()};
    SDL_Point centerR = {couplerTexture->getWidth() / 2, couplerTexture->getHeight() / 2};
    
    couplerTexture->render(rr,
                           XtoSX(myCouplerF->GetPosition().x) - centerR.x,
                           YtoSY(myCouplerF->GetPosition().y) - centerR.y,
                           &clipR,
                           PHItoSPHI(myCouplerF->GetAngle()),
                           &centerR,
                           flipType);
    
    couplerTexture->render(rr,
                           XtoSX(myCouplerR->GetPosition().x) - centerR.x,
                           YtoSY(myCouplerR->GetPosition().y) - centerR.y,
                           &clipR,
                           PHItoSPHI(myCouplerR->GetAngle() + M_PI),
                           &centerR,
                           flipType);
                           
    
    // Render the train car to the screen
    SDL_Rect clipR_car = {0, 0, carTexture->getWidth(), carTexture->getHeight()};
    SDL_Point centerR_car = {carTexture->getWidth() / 2, carTexture->getHeight() / 2};
    
    carTexture->render(rr,
                       calcSX() - centerR_car.x,
                       calcSY() - centerR_car.y,
                       &clipR_car,
                       calcSP(),
                       &centerR_car,
                       flipType);

    
    
}



/**
 * Draw the goal region for this train car
 */
void trainCar::drawGoal(SDL_Renderer* rr) {

    // TODO: finish trainCar::drawGoal
    // FIXME: draw the goal region as lines
    
    
    
    // calculate corner points
    
    // draw 4 lines
    
}