// Daniel Montrallo Flickinger, PhD
// May 2014

// Test simulation of a single train car
// TODO: build unit tests in gtest


#include <iostream>
#include <iomanip>
#include <fstream>

#include <cmath>
#include <string>

// #include <gsl/gsl_odeiv2.h>

#include "SDL.h"
// #include "SDL2/SDL_image.h"

#include <Box2D/Box2D.h>


#include "constants.hh"

#include "LTexture.hh"

#include "trackWP.hh"


#include "trainCar.hh"

#include "mdlDerivatives.hh"
#include "carControl.hh"

#include "trackModel.hh"

// #include "img_loaders.hh"

#include "dataDisplay.hh"

#include "LTexture.hh"
#include "game_state.hh"

#define SAVE_DATA





// Progress bar
// static inline void loadBar(double x, double n, unsigned int w = 70) {
//     
//     if ((x != n) && (fmod(x, n/100.0) > 0.001))
//         return;
// 
//     
//     float ratio = x / n;
//     int c = ratio * w;
//     
//     std::cout << std::setw(3) << (int) (ratio*100) << "% [";
//     for (int x=0; x<c; x++)
//         std::cout << "=";
//     for (int x=c; x<w; x++)
//         std::cout << " ";
//     std::cout << "]\r" << std::flush;
//     
// }



#ifdef SAVE_DATA

// ostream sd_file;

#endif


int SCREEN_WIDTH = 800;
int SCREEN_HEIGHT = 400;
double SCREEN_SCALE = 5.5;


int main(int argc, char * argv[]) {

    // Wait for a quit
    bool quit = false;


    // Event handler
    SDL_Event event;


 


    // the big game state object
    // NOTE: initializes Box2D world too
    game_state gs;


    // Initializae game state
    if (gs.init() == false) {
        return 1;
    }






    // While we haven't quit
    while (!quit) {
        // While there's an event to handle
        while (SDL_PollEvent(&event) != 0) {

            // If the user hits the X window button
            if (event.type == SDL_QUIT) {
                quit = true;
            }
            else if (event.type == SDL_KEYDOWN) {
                switch (event.key.keysym.sym) {
                    case SDLK_q:
                        std::cout << "Quitting..." << std::endl;
                        quit = true;
                        break;
                        
                    case SDLK_0:
                        gs.send_control(0); // throttle to 0 (idle)
                        break;
                        
                    case SDLK_1:
                        gs.send_control(1); // throttle to 1
                        break;
                        
                    case SDLK_2:
                        gs.send_control(2); // throttle to 2
                        break;
                        
                    case SDLK_3:
                        gs.send_control(3); // throttle to 3
                        break;
                        
                    case SDLK_4:
                        gs.send_control(4); // throttle to 4
                        break;
                        
                    case SDLK_5:
                        gs.send_control(5); // throttle to 5
                        break;
                        
                    case SDLK_6:
                        gs.send_control(6); // throttle to 6
                        break;
                        
                    case SDLK_7:
                        gs.send_control(7); // throttle to 7
                        break;
                        
                    case SDLK_8:
                        gs.send_control(8); // throttle to 8
                        break;
                        
                    case SDLK_d:
                        gs.send_control(9); // flip direction
                        break;
                        
                    case SDLK_s:
                        gs.send_control(19); // all stop
                        break;
                        
                }
            }
            else if (SDL_MOUSEBUTTONDOWN == event.type) {
            
                // get mouse position
                int xpos, ypos;
                SDL_GetMouseState(&xpos, &ypos);
                 
               gs.process_click(xpos, ypos);
                
            }

        }


        if (!gs.update()) {
            std::cout << "Update failed!" << std::endl;
        }


        if (!gs.draw()) {
            std::cout << "Draw failed!" << std::endl;
        }

        


        SDL_Delay(1.0 / 60.0);
        
    }

    
    gs.close();

    
    return 0;
    
    
}