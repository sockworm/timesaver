

#include<cmath>
#include<iostream>
#include<string>

#include "constants.hh"

#include "trackWP.hh"



// FIXME: solve track turnout problem by having a 'track_active' attribute, so a track segment would get displayed, but not be seen in the dynamics

trackWP::trackWP() {
    my_x = 0.0;
    my_y = 0.0;
    
    my_dir_x = 0.0;
    my_dir_y = 0.0;
    
    my_vel_x = 0.0;
    my_vel_y = 0.0;
    
    my_acc_x = 0.0;
    my_acc_y = 0.0;
    
    
//     std::cout << "Creating zero track waypoint" << std::endl;
}

/** 
 * Create a new track waypoint (position only)
 */
trackWP::trackWP(double xx, double yy) {
    my_x = xx;
    my_y = yy;
    
    my_dir_x = 0.0;
    my_dir_y = 0.0;
    
    my_vel_x = 0.0;
    my_vel_y = 0.0;
    
    my_acc_x = 0.0;
    my_acc_y = 0.0;
//     std::cout << "Creating track waypoint [" << xx << ", " << yy << "]" << std::endl;
}


/** 
 * Create a new track waypoint (position and direction)
 */
trackWP::trackWP(double xx, double yy, double xx_dir, double yy_dir) {
    
    my_x = xx;
    my_y = yy;
    
    my_dir_x = xx_dir;
    my_dir_y = yy_dir;
    
    my_vel_x = 0.0;
    my_vel_y = 0.0;
    
    my_acc_x = 0.0;
    my_acc_y = 0.0;
    
}



/**
 * Create a new track waypoint (position, direction, and velocity)
 */
trackWP::trackWP(double xx, double yy, double xx_dir, double yy_dir, double xx_vel, double yy_vel) {
 
    my_x = xx;
    my_y = yy;
    
    my_dir_x = xx_dir;
    my_dir_y = yy_dir;
    
    my_vel_x = xx_vel;
    my_vel_y = yy_vel;
    
    my_acc_x = 0.0;
    my_acc_y = 0.0;
    
}


/**
 * Create a new track waypoint (position, direction, velocity, and acceleration)
 */
trackWP::trackWP(double xx, double yy, double xx_dir, double yy_dir, double xx_vel, double yy_vel, double xx_acc, double yy_acc) {
 
    my_x = xx;
    my_y = yy;
    
    my_dir_x = xx_dir;
    my_dir_y = yy_dir;
    
    my_vel_x = xx_vel;
    my_vel_y = yy_vel;
    
    my_acc_x = xx_acc;
    my_acc_y = yy_acc;
    
}


/** move x,y to dir_x, dir_y
 */
void trackWP::move_to_dir() {

    my_dir_x = my_x;
    my_dir_y = my_y;
    
}

/** 
 * Copy only the position fields from the input
 */
void trackWP::copy_pos(trackWP wp) {

    my_x = wp.x();
    my_y = wp.y();
    
}


/** 
 * Copy only the direction fields from the input
 */
void trackWP::copy_dir(trackWP wp) {
    
    my_dir_x = wp.dir_x();
    my_dir_y = wp.dir_y();
    
}



/** Travel a distance along waypoint direction, update position
 */
void trackWP::travel(double dist) {

    my_x += my_dir_x * dist;
    my_y += my_dir_y * dist;
    
}


/** rotate vector by angle
 */
void trackWP::rotate(double th) {

    double xx = my_dir_x;
    double yy = my_dir_y;
    
    my_dir_x = xx * cos(th) - yy * sin(th);
    my_dir_y = xx * sin(th) + yy * cos(th);
    
    
}


/** make vector into a unit vector
 */
void trackWP::calc_norm() {
    
    double L = calc_distance();
    
    my_x = my_x / L;
    my_y = my_y / L;
    
    
}


/** 
 * calculate distance from origin to this vector
 */
double trackWP::calc_distance() {
    
    return sqrt(my_x*my_x + my_y*my_y);
    
}


/**
 * Calculate angle in degrees of the direction
 */
int trackWP::calc_degrees() {
    
    return (int)(-atan2(my_dir_y, my_dir_x) * 180.0 / M_PI);
    
}


    
trackWP trackWP::operator+(const trackWP &b) {
    
    trackWP out;
    
    out.setx(my_x + b.x());
    out.sety(my_y + b.y());
    
    return out;
    
}

trackWP trackWP::operator-(const trackWP &b) {
    
    trackWP out;
    
    out.setx(my_x - b.x());
    out.sety(my_y - b.y());
    
    return out;
    
}
