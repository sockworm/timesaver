
#include<SDL.h>
#include<SDL_image.h>
// #include<SDL_ttf.h>

#include<stdio.h>
#include<string>


#include "constants.hh"


// #include "game_state.hh"
#include "LTexture.hh"

// extern SDL_Renderer *gRenderer;


LTexture::LTexture() {
    // Initialize
    mTexture = NULL;
    mWidth = 0;
    mHeight = 0;

}


LTexture::~LTexture() {
    // Deallocate
    free();

}


/**
 * Store data such as filename, and dimensions for this texture,
 * then load it from file
 */
bool LTexture::loadFromFile(SDL_Renderer *rr, std::string fname, double iWidth, const double iHeight) {
    
    filename = fname;
    itemWidth = iWidth;
    itemHeight = iHeight;
    
    // Make sure the renderer doesn't go out of scope!
    gsr = rr;
    
    
    // Load all the junk
    reload();
    
    return mTexture != NULL;

}



/**
 * (re)load the texture from the specified file
 */
void LTexture::reload() {


    
    // Get rid of preexisting texture
    free();

    // The final texture
    SDL_Texture *newTexture = NULL;

    // Load image at specified path
    SDL_Surface *initSurface = IMG_Load(filename.c_str());

    // Load the image again to initialize
    // FIXME: initialize a new surface without IMG_Load
    SDL_Surface *loadedSurface = IMG_Load(filename.c_str());

    
    
    if (NULL == loadedSurface) {
        printf("Loading %s failed. SDL_image error: %s\n", filename.c_str(), IMG_GetError());
    }
    else {
        
        
 
        
        // Calculate scale

            
        // calculate pixels/m for image         
        double scaling_factor_w = SCREEN_SCALE * itemWidth / initSurface->w;
        double scaling_factor_h = SCREEN_SCALE * itemHeight / initSurface->h;
            
        // FIXME: cannot scale up a texture, only down
        
        // Scale the image
        SDL_Rect scaledRect = {0, 0, (int)(initSurface->w * scaling_factor_w), (int)(initSurface->h * scaling_factor_h)};
        SDL_BlitScaled(initSurface, NULL, loadedSurface, &scaledRect);
    
        
        // Clip the surface if needed
        if (scaling_factor_w < 1.0) {
            loadedSurface->w = (int)(initSurface->w * scaling_factor_w);
        }
        if (scaling_factor_h < 1.0) {
            loadedSurface->h = (int)(initSurface->h * scaling_factor_h);
        }

        // Color key image (to cyan)
        SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));

        
        // Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface(gsr, loadedSurface);

        if (NULL == newTexture) {
            printf("Unable to optimize image %s. SDL error: %s\n", filename.c_str(), SDL_GetError());
        }
        else {
            
            // Get image dimensions
            mWidth = loadedSurface->w;
            mHeight = loadedSurface->h;
        }

        // Get rid of old loaded surface
        SDL_FreeSurface(loadedSurface);
    }


    mTexture = newTexture;


}


#ifdef _SDL_TTF_H

/** 
 * Creates image from font string
 */
bool LTexture::loadFromRenderedText(SDL_Renderer *gsr,
                                    TTF_Font *myfont,
                                    std::string textureText,
                                    SDL_Color textColor) {

    
    //Get rid of preexisting texture
    free();

    //Render text surface
    SDL_Surface* textSurface = TTF_RenderText_Solid(myfont, textureText.c_str(), textColor );
    
    
    if(NULL == textSurface) {
        std::cout << "Unable to render text surface! SDL_ttf Error: " << TTF_GetError() << std::endl;
    }
    else {
        //Create texture from surface pixels
        mTexture = SDL_CreateTextureFromSurface(gsr, textSurface);
        
        if(NULL == mTexture) {
            std::cout << "Unable to create texture from rendered text! SDL Error: " << SDL_GetError() << std::endl;
        }
        else {
            
            //Get image dimensions
            mWidth = textSurface->w;
            mHeight = textSurface->h;
        }

        //Get rid of old surface
        SDL_FreeSurface(textSurface);
    }
    
    //Return success
    return mTexture != NULL;
    
    
}

#endif


/**
 *  Create a blank texture
 */
bool LTexture::createBlank(SDL_Renderer *gsr, int width, int height, SDL_TextureAccess access )
{
    //Create uninitialized texture
    mTexture = SDL_CreateTexture( gsr, SDL_PIXELFORMAT_RGBA8888, access, width, height );
    if( mTexture == NULL )
    {
        printf( "Unable to create blank texture! SDL Error: %s\n", SDL_GetError() );
    }
    else
    {
        mWidth = width;
        mHeight = height;
    }

    return mTexture != NULL;
}






void LTexture::free() {

    // Free texture if it exists
    if (mTexture != NULL) {
        SDL_DestroyTexture(mTexture);
        mTexture = NULL;

        mWidth = 0;
        mHeight = 0;
    }

}



void LTexture::render(SDL_Renderer *gsr, int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip) {

    // Set rendering space and render to screen
    SDL_Rect renderQuad = {x, y, mWidth, mHeight};

    // Set clip rendering dimensions
    if (clip != NULL) {
        renderQuad.w = clip->w;
        renderQuad.h = clip->h;
    }


    // Render to screen
    SDL_RenderCopyEx(gsr, mTexture, clip, &renderQuad, angle, center, flip);

}




void LTexture::setAsRenderTarget(SDL_Renderer *gsr)
{
    //Make self render target
    SDL_SetRenderTarget( gsr, mTexture );
}


int LTexture::getWidth() {
    return mWidth;
}

int LTexture::getHeight() {
    return mHeight;
}


