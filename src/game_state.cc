/* Functions for game state: init, etc.
 * 
 * 
 * Daniel Montrallo Flickinger, PhD
 * 
 */


#include <SDL.h>
#include <SDL_image.h>

#include <Box2D/Box2D.h>


#include <stdio.h>
#include <iostream>
#include <string>

// #include <gsl/gsl_odeiv2.h>

#include "constants.hh"

#include "LTexture.hh"


#include "trackWP.hh"

#include "trainCar.hh"


#include "mdlDerivatives.hh"
#include "carControl.hh"

#include "trackModel.hh"


#include "dataDisplay.hh"

#include "game_state.hh"

// extern SDL_Renderer *gRenderer;

// game_state::game_state() {
game_state::game_state() : gravity(0.0f, 0.0f), world(gravity) {
   
    
    
}


/** 
 * Initialize the game state
 */
bool game_state::init() {
    
    // CHEAT AT DYNAMICS
    cheat_dynamics = false;

    // Initialize all SDL subsystems
    if (-1 == SDL_Init(SDL_INIT_VIDEO)) {
        std::cout << "SDL failed to initialize: SDL_Error: " << SDL_GetError() << std::endl;
        return false;
    }




    
    // Create window
    window = SDL_CreateWindow("Timesaver", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

   // error check for screen creation
    if (NULL == window) {
        std::cout << "Failed to create window: SDL error: " << SDL_GetError() << std::endl;
        return false;
    }


    last_screen_width = SCREEN_WIDTH;
    last_screen_height = SCREEN_HEIGHT;

    // Create a renderer for window
    // NOTE: SDL_RENDERER_SOFTWARE if GL is not supported
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    

    if (NULL == renderer) {
        std::cout << "Failed to create renderer: SDL error: " << SDL_GetError() << std::endl;
        return false;
    }

    // Initialize renderer color
    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        
    // Initialize PNG loading
    int imgFlags = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlags) & imgFlags)) {
        std::cout << "SDL_image did not initialize: SDL_image error: " << IMG_GetError() << std::endl;
        return false;
    }


    // Set world parameters for Box2D
    timeStep = 1.0f / 60.0f;
    velocityIterations = 6;
    positionIterations = 2;
 

    // Load all textures
    load_textures();
    
    
    // Set the position states for each car, and create the dynamic objects in the world
    // ------------------------------------------------------------------------
    
    // FIXME: add angle to addCarBody()
    addCarBody(carBody[0], couplerBody_F[0], couplerBody_R[0], 0.0, 0.0, 0); // locomotive
    addCarBody(carBody[1], couplerBody_F[1], couplerBody_R[1], 50.0, 0.0, 1);  // white car
    addCarBody(carBody[2], couplerBody_F[2], couplerBody_R[2], -50.0, -6.59138, 2);  // blue car
    addCarBody(carBody[3], couplerBody_F[3], couplerBody_R[3], -30.0, 14.245, 3); // red car
    addCarBody(carBody[4], couplerBody_F[4], couplerBody_R[4], 32.0, 14.245, 4); // light blue car
    addCarBody(carBody[5], couplerBody_F[5], couplerBody_R[5], 54.0, 14.245, 5); // gray car
        
    
    for (int incr_c = 0; incr_c < NUM_CARS; ++incr_c) {
        mycar[incr_c].setBody(carBody[incr_c]);
        mycar[incr_c].setCouplerBodies(couplerBody_F[incr_c], couplerBody_R[incr_c]);
    }
    
    
    
    world.SetContactListener(&myContactListener);
    
 
    
    // ------------------------------------------------------------------------
    
    // Set car textures
    mycar[0].setTexture(&locoTexture, &couplerTexture);
    mycar[1].setTexture(&tankTexture, &couplerTexture);
    mycar[2].setTexture(&blueboxTexture, &couplerTexture);
    mycar[3].setTexture(&boxTexture, &couplerTexture);
    mycar[4].setTexture(&bluetankTexture, &couplerTexture);
    mycar[5].setTexture(&flatTexture, &couplerTexture);
    
    
    // ------------------------------------------------------------------------

    
    // Create the track layout
//     create_track_simple();
    create_track_timesaver();
    
    
    myTracks.print();
    
    // Calculate the size of the layout for window scaling:
    computeLayoutLimits();
    
    
    // ------------------------------------------------------------------------

    
    // Create data display
    ddisp = {&mycar[0]};
    
    
    // initialize car controller for locomotive
    cc.init(&mycar[0]);
    
    
    // It's all good
    return true;
    
}



/** Close out game state
 */
bool game_state::close() {


    // Free loaded image
//     SDL_DestroyTexture(texture);
//     texture = NULL;

//     // Deallocate surface
//     SDL_FreeSurface(switchloco);
//     switchloco = NULL;



//     locoTexture.free();

    // FIXME: free other textures

    
    // Destroy renderer and window
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = NULL;
    renderer = NULL;

    // Quit SDL
    IMG_Quit();
    SDL_Quit();


    
//     gsl_odeiv2_driver_free(gsl_drv); // free up the shit


    
    return true;

}



/** Update game state
 */
bool game_state::update() {


    // Check window size, and compute the scale
    SDL_GetWindowSize(window, &SCREEN_WIDTH, &SCREEN_HEIGHT);
    computeWindowScale();
    
    if ((last_screen_width != SCREEN_WIDTH) || 
        (last_screen_height != SCREEN_HEIGHT)) {
        
        // trigger resize event
        last_screen_width = SCREEN_WIDTH;
        last_screen_height = SCREEN_HEIGHT;
        
        // reload all textures
        reload_textures();
    
        // reset dimensions of data display
        ddisp.set_dimensions();
    
    }

    
    // Traction controller
    cc.tractionControl();
   
    world.Step(timeStep, velocityIterations, positionIterations);
    
    
    // check for coupler collisions
    int nc = 0;
    std::array<b2Fixture*, MAX_COUPLER_CONTACTS> fixA;
    std::array<b2Fixture*, MAX_COUPLER_CONTACTS> fixB;
        
    nc = myContactListener.check_for_contacts();
        
    fixA = myContactListener.get_fixA();
    fixB = myContactListener.get_fixB();
        
    if (nc > 0) {
         
        for (int incr_c = 0; incr_c < nc; ++incr_c) {
                    
            b2Body* bodyA = fixA[incr_c]->GetBody();
            b2Body* bodyB = fixB[incr_c]->GetBody();
            
                
            // Check if these bodies are already joined
            bool bodyA_isCoupled = (bool)bodyA->GetUserData();
            bool bodyB_isCoupled = (bool)bodyB->GetUserData();
                
                
            if (!bodyA_isCoupled && !bodyB_isCoupled) {
                
                b2Vec2 anchor(0.3f, 0.0f);
                anchor += bodyA->GetPosition();
                    
                
                b2WeldJointDef jd;

                jd.Initialize(bodyA, bodyB, anchor);
                world.CreateJoint(&jd);
                  
                bodyA_isCoupled = true;
                bodyB_isCoupled = true;
                    
                bodyA->SetUserData( (void*)bodyA_isCoupled);
                bodyB->SetUserData( (void*)bodyB_isCoupled);
            }
        }
            
    }
        
        
        
    // drive couplers to zero
    // (disable controller if coupled)
        
    float32 gain = 30.0f;

    // step through each car, and each coupler joint

    for (int incr_c = 0; incr_c < NUM_CARS; ++incr_c) {
        
        
        // Get the bodies
        b2Body* j_bodyA = coupler_joint_a[incr_c]->GetBodyA();
        b2Body* j_bodyB = coupler_joint_a[incr_c]->GetBodyB();
        
        // Check if these bodies are already joined
        bool bodyA_isCoupled = (bool)j_bodyA->GetUserData();
        bool bodyB_isCoupled = (bool)j_bodyB->GetUserData();
            
        if (!bodyA_isCoupled && !bodyB_isCoupled) {
            float32 angleError = coupler_joint_a[incr_c]->GetJointAngle();
            coupler_joint_a[incr_c]->SetMotorSpeed(-gain * angleError);
        }
            
        
        
        // ---------
        
        
        // Get the bodies
        j_bodyA = coupler_joint_a[incr_c]->GetBodyA();
        j_bodyB = coupler_joint_a[incr_c]->GetBodyB();
        
        // Check if these bodies are already joined
        bodyA_isCoupled = (bool)j_bodyA->GetUserData();
        bodyB_isCoupled = (bool)j_bodyB->GetUserData();
        
        if (!bodyA_isCoupled && !bodyB_isCoupled) {
            float32 angleError = coupler_joint_b[incr_c]->GetJointAngle();
            coupler_joint_b[incr_c]->SetMotorSpeed(-gain * angleError);
        }
            
    }
    
    
    return true;

}




/** Draw the game state
 */
bool game_state::draw() {

//     double l_x = 50.0;
//     double l_y = 50.0;
//     double l_phi = 0.0;

    if (NULL == renderer) {
        std::cout << "What the fuck happened to the renderer??: SDL error: " << SDL_GetError() << std::endl;
        return false;
    }
    
    
    

    
    // Clear screen
    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_RenderClear(renderer);



    // FIXME: find a better way to make a background:
    // Fill the surface gray
    SDL_Rect fillRect = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};
    SDL_SetRenderDrawColor(renderer, 0x6E, 0xAC, 0x58, 0xFF);
    SDL_RenderFillRect(renderer, &fillRect);
 

    
    // Draw the track
    myTracks.draw(renderer, &tieTexture, &ballastTexture);
    
       
    // Draw the locomotive
    // FIXME: set up the car class to store its own texture
    for (int incr_i = 0; incr_i < NUM_CARS; ++incr_i) {
        mycar[incr_i].draw(renderer);
    }
    
    
    
    // Draw the data display
    ddisp.draw(renderer);
    
    
    // Update screen
    SDL_RenderPresent(renderer);

    return true;


}


/** Send a control code to the car
 */
void game_state::send_control(unsigned short int control_code) {

    

    if ((control_code >= 0) && (control_code < 9)) {
        // Throttle setting
        mycar[0].setThrottle(control_code);
    }
    
    if (9 == control_code) {
        // flip direction
        mycar[0].flipDirection();
    }

    if ((control_code >= 10) && (control_code < 18)) {
        // Brake setting
        mycar[0].setBrake(control_code - 10);
    }
    
    if (19 == control_code) {
        // all stop
        mycar[0].setThrottle(0);
        mycar[0].setBrake(8);
        // FIXME: maybe default the direction to forward too
    }
    
  
        
    
}




/**
 * Process mouse click
 */
void game_state::process_click(int xp, int yp) {
            
    // FIXME: check to see if the click is within the info display region
    
    
    
    // convert the click to world coordinates
    double xpos = SXtoX(xp);
    double ypos = SYtoY(yp);
    
    
    // Search through the turnouts
    int to_idx = myTracks.findTurnout(xpos, ypos);
    if (to_idx >= 0) {
        myTracks.flipTurnout(to_idx);
    }
    
    
    // FIXME: Search through the cars
    
}



void game_state::create_track_simple() {
    
    
    trackSegment temp_seg;
    
    
    // one car length
    trackWP sp (-70.0, 0.0);
    trackWP ep (-50.0, 0.0);
    
    // Initialize the track
    myTracks.init(sp, ep, &world);
    
    myTracks.add_straight_20(true);
    
    
    myTracks.add_curved_L8(true);
    myTracks.add_curved_R8(true);
    myTracks.add_straight_20(true);
    
    
}



/**
 * Create the classic timesaver layout
 */
void game_state::create_track_timesaver() {
    
    trackSegment temp_seg;
    
    // Main straight:
    // ==============
    
    std::cout << "Initializing track with leftside straight segment." << std::endl;
    
    // one car length
    trackWP sp (-80.0, 0.0);
    trackWP ep (-60.0, 0.0); // turnout point to middle track
    
    // Initialize the track
    myTracks.init(sp, ep, &world);
    
    
    // Add leftmost bumper (0)
    b2Vec2 bv(-81.0, 0.0);
    myTracks.addBumper(&world, bv, M_PI, &bumperTexture);
    
    myTracks.add_straight_20(true); // segment 1
    
    
    // Point for first turnout (left)
    trackWP sp_to1 = myTracks.get_endpoint();
    std::cout << "Setting point for first turnout at [" << sp_to1.x() << ", " << sp_to1.y() << "]" << std::endl;

    
    // The runaround track
    std::cout << "String runaround track with initial L turn." << std::endl;
    myTracks.add_curved_L8(false); // segment 2
    
    // Point for second turnout (middle)
    trackWP sp_to2 = myTracks.get_endpoint();
    std::cout << "Setting point for second turnout at [" << sp_to2.x() << ", " << sp_to2.y() << "]" << std::endl;

    
    std::cout << "Building runaround track: R, R, L." << std::endl;
    myTracks.add_curved_R8(true); // segment 3
    myTracks.add_curved_R8(true); // segment 4
    myTracks.add_curved_L8(false); // segment 5
    
    // Point for third turnout (right)
    trackWP sp_to3 = myTracks.get_endpoint(); 
    std::cout << "Setting point for third turnout (on right) at [" << sp_to3.x() << ", " << sp_to3.y() << "]" << std::endl;

    
    std::cout << "Adding two straights for right side track." << std::endl;
    myTracks.add_straight_20(true); // segment 6
    myTracks.add_straight_20(true); // segment 7
    
    
    
    // Go back to first turnout
    std::cout << "Going back to first turnout at [" << sp_to1.x() << ", " << sp_to1.y() << "]" << std::endl;
    temp_seg.init_straight_15(sp_to1, &world, true);
    myTracks.add_segment(temp_seg); // segment 8
    
    myTracks.add_straight_15(true); // segment 9

   
    // Add turnout 1
    myTracks.add_turnout(8, 2);
    
    
    // Point for fourth (bottom) turnout
    trackWP sp_to4 = myTracks.get_endpoint();
    std::cout << "Setting point for fourth turnout (to bottom track) at [" << sp_to4.x() << ", " << sp_to4.y() << "]" << std::endl;

    
    std::cout << "Building main track right to third turnout." << std::endl;
    myTracks.add_straight_20(true); // segment 10
    myTracks.add_straight_20(true); // segment 11
    
//     trackSegment* temp_seg_ptr = myTracks.get_last_segment_ptr();
//     temp_seg_ptr->setInactive();
    
    
 

    
    // go back to second turnout
    std::cout << "Going back to second turnout at [" << sp_to2.x() << ", " << sp_to2.y() << "]" << std::endl;
    temp_seg.init_straight_20(sp_to2, &world, false);
    myTracks.add_segment_inactive(temp_seg); // segment 12
    
    
    // Add turnout 2
    // FIXME: segfaults!!:
//     myTracks.add_turnout(13, 6, sp_to4);
    myTracks.add_turnout(12, 3);
    
    // Add turnout 3 (right)
    myTracks.add_turnout(11, 5);
    // should be at [26.2742, 0]
    trackWP pt_i(26.2742, 0.0);
    myTracks.setTurnout_pt(2, pt_i);
    

    
    myTracks.add_curved_R8(true); // segment 13
    
    // Point for fifth (top) turnout
    trackWP sp_to5 = myTracks.get_endpoint();
    std::cout << "Setting point for fifth turnout (top) at [" << sp_to5.x() << ", " << sp_to5.y() << "]" << std::endl;

    
    
    std::cout << "Building top track right." << std::endl;
    myTracks.add_straight_20(true); // segment 14
    myTracks.add_straight_20(true); // segment 15
    myTracks.add_straight_20(true); // segment 16
    
    
    // go back to fifth (top) turnout
    std::cout << "Going back to fifth turnout (top) at [" << sp_to5.x() << ", " << sp_to5.y() << "]" << std::endl;
    sp_to5.flip();
    temp_seg.init_straight_20(sp_to5, &world, false);
    myTracks.add_segment_inactive(temp_seg); // segment 17
    
    
    // Add turnout 4 (bottom)
    myTracks.add_turnout(13, 17);
    
    std::cout << "Building top track left." << std::endl;
    myTracks.add_straight_20(true); // segment 18
    myTracks.add_straight_20(true); // segment 19
    myTracks.add_straight_20(true); // segment 20
    
    
    // go back to fourth (bottom) turnout
    std::cout << "Going back to fourth turnout (bottom) at [" << sp_to4.x() << ", " << sp_to4.y() << "]" << std::endl;
    sp_to4.flip();
    temp_seg.init_curved_L8(sp_to4, &world, false);
    myTracks.add_segment_inactive(temp_seg); // segment 21
    
    // Add turnout 5 (top)
    myTracks.add_turnout(21, 9);
    
    std::cout << "Building bottom track." << std::endl;
    myTracks.add_curved_R8(true);
    myTracks.add_straight_20(true);
//     myTracks.add_straight_20();
    
    
    // Add rightmost bumper (1)
    bv.Set(67.2742, 0.0);
    myTracks.addBumper(&world, bv, 0.0, &bumperTexture);
    
    // Add bottom left bumper (2)
    bv.Set(-64.1371, -6.59138);
    myTracks.addBumper(&world, bv, M_PI, &bumperTexture);
    
    // Add top left bumper (3)
    bv.Set(-69.3853, 14.245);
    myTracks.addBumper(&world, bv, M_PI, &bumperTexture);
    
    // Add top right bumper (4)
    bv.Set(72.6147, 14.245);
    myTracks.addBumper(&world, bv, 0.0, &bumperTexture);
    
}






// Dynamics initialization functions:
void game_state::addWheel(b2Body* tb, b2BodyDef bd, b2FixtureDef fd, float x, float y, float xl, float yl) {
        
        
    b2RevoluteJointDef revoluteJointDef;
    b2RevoluteJoint* w_joint;

    fd.filter.categoryBits = WHEEL;
    fd.filter.maskBits = RAIL;
        
        
    bd.position.Set(x, y);
    wheelBody = world.CreateBody(&bd);
    wheelBody->CreateFixture(&fd);
        
    revoluteJointDef.bodyA = tb;
    revoluteJointDef.bodyB = wheelBody;
    revoluteJointDef.collideConnected = false;
    revoluteJointDef.localAnchorA.Set(xl, yl);
    revoluteJointDef.localAnchorB.Set(0,0);
        
    w_joint = (b2RevoluteJoint*)world.CreateJoint(&revoluteJointDef);
        
        
}



void game_state::addCoupler(b2Body* &couplerBody, float x, float y) {
    
    b2BodyDef BodyDef;
    BodyDef.type = b2_dynamicBody;
    b2FixtureDef fixtureDef;
    fixtureDef.density = 4000.0;
        
        
    b2PolygonShape boxShape;
    // FIXME: set coupler dimensions
    boxShape.SetAsBox(COUPLER_LENGTH / 2.0, COUPLER_WIDTH / 2.0);
        
    BodyDef.position.Set(x, y);
    BodyDef.angle = 0;
    BodyDef.gravityScale = 0.0f;
        
    fixtureDef.shape = &boxShape;
        
    // collide with other cars, and bumpers
    fixtureDef.filter.categoryBits = COUPLER;
    fixtureDef.filter.maskBits = COUPLER | CARBODY | BUMPER;
//     fixtureDef.filter.maskBits = COUPLER | CARBODY;
    fixtureDef.restitution = 0.0f;

        
    couplerBody = world.CreateBody(&BodyDef);
    couplerBody->CreateFixture(&fixtureDef);
        
    bool isCoupled = false;
    couplerBody->SetUserData( (void*)isCoupled);
        
}


void game_state::addTruck(b2Body* &truckBody, float x, float y) {
    

        
    b2BodyDef BodyDef;
    BodyDef.type = b2_dynamicBody;
    b2FixtureDef fixtureDef;
    fixtureDef.density = 4268.1224;

//         b2RevoluteJointDef revoluteJointDef;
        
    // truck shape and wheel shape
    b2PolygonShape boxShape;
    boxShape.SetAsBox(1.37, 0.71755);
        
    b2CircleShape circleShape;
    circleShape.m_radius = 0.25;
        
        
        
    // Create the main truck body
    BodyDef.position.Set(x, y);
    BodyDef.angle = 0;
    BodyDef.gravityScale = 0.0f;
        
    fixtureDef.shape = &boxShape;
        
    // collide with nothing!
    fixtureDef.filter.categoryBits = 0;
    fixtureDef.filter.maskBits = 0;
    
    truckBody = world.CreateBody(&BodyDef);
    truckBody->CreateFixture(&fixtureDef);

        

        
    // switch to wheel shape
    fixtureDef.shape = &circleShape;

    // Add wheels
    addWheel(truckBody, BodyDef, fixtureDef, x + 1.37, y - 0.4676, 1.37, -0.4676);
    addWheel(truckBody, BodyDef, fixtureDef, x + 1.37, y + 0.4676, 1.37, 0.4676);
    addWheel(truckBody, BodyDef, fixtureDef, x - 1.37, y - 0.4676, -1.37, -0.4676);
    addWheel(truckBody, BodyDef, fixtureDef, x - 1.37, y + 0.4676, -1.37, 0.4676);
    
}
    
    
void game_state::addCarBody(b2Body* &mainBody, 
                            b2Body* &couplerBodyF, 
                            b2Body* &couplerBodyR, 
                            float x, 
                            float y, 
                            int idx) {
    

        
    b2BodyDef bd;
        
        
    b2RevoluteJointDef revoluteJointDef;
    b2RevoluteJoint* w_joint;
        
    bd.type = b2_dynamicBody;
    b2FixtureDef fd;
    fd.density = 1365.5704;
        
        
        
    // body shape
    b2PolygonShape boxShape;
//         boxShape.SetAsBox(9.017, 0.1);
    boxShape.SetAsBox(9.017, 1.5748);
    // FIXME: filter collisions and then make this bigger
        
    // FIXME: uint16
    // Set the category and mask bits to only collide with other car bodies
    fd.filter.categoryBits = CARBODY;
    fd.filter.maskBits = CARBODY | BUMPER;
        
        
    // Create the main car body
    bd.position.Set(x, y);
    bd.angle = 0;
    bd.gravityScale = 0.0f;
        
    fd.shape = &boxShape;
    mainBody = world.CreateBody(&bd);
    mainBody->CreateFixture(&fd);

        
    // Add trucks
    addTruck(truckBody_a, x - 6.5532, y);
    addTruck(truckBody_b, x + 6.5532, y);
        
    // Add joints
    revoluteJointDef.bodyA = mainBody;
    revoluteJointDef.bodyB = truckBody_a;
    revoluteJointDef.collideConnected = false;
    revoluteJointDef.localAnchorA.Set(-6.5532, 0.0);
    revoluteJointDef.localAnchorB.Set(0,0);
        
    w_joint = (b2RevoluteJoint*)world.CreateJoint(&revoluteJointDef);
        
        
    revoluteJointDef.bodyA = mainBody;
    revoluteJointDef.bodyB = truckBody_b;
    revoluteJointDef.collideConnected = false;
    revoluteJointDef.localAnchorA.Set(6.5532, 0.0);
    revoluteJointDef.localAnchorB.Set(0, 0);
        
    w_joint = (b2RevoluteJoint*)world.CreateJoint(&revoluteJointDef);
 
    
    
    // Add couplers
    addCoupler(couplerBodyR, x - 7.0, y);
    addCoupler(couplerBodyF, x + 7.0, y);
        
        

    // Add coupler joints
    revoluteJointDef.collideConnected = false;
    revoluteJointDef.lowerAngle = -0.20f * b2_pi;
    revoluteJointDef.upperAngle = 0.20f * b2_pi;
    revoluteJointDef.enableLimit = true;
    revoluteJointDef.maxMotorTorque = 1000.0f;
    revoluteJointDef.motorSpeed = 0.0f;
    revoluteJointDef.enableMotor = true;

    revoluteJointDef.bodyA = mainBody;
        
    
    
    revoluteJointDef.bodyB = couplerBodyR;
    revoluteJointDef.localAnchorA.Set(-8.5, 0.0);
    revoluteJointDef.localAnchorB.Set(0.6, 0.0);
        
        
    coupler_joint_a[idx] = (b2RevoluteJoint*)world.CreateJoint(&revoluteJointDef);
       
    
    
    revoluteJointDef.bodyB = couplerBodyF;
    revoluteJointDef.localAnchorA.Set(8.5, 0.0);
    revoluteJointDef.localAnchorB.Set(-0.6, 0.0);
        
    coupler_joint_b[idx] = (b2RevoluteJoint*)world.CreateJoint(&revoluteJointDef);
    
    
}

    
    
// Track constraints setup functions

/**
 * Compute the layout limits
 */
void game_state::computeLayoutLimits() {
    
    double track_xmin = 0.0;
    double track_xmax = 0.0;
    double track_ymin = 0.0;
    double track_ymax = 0.0;

    myTracks.find_limits(track_xmax, track_xmin, track_ymax, track_ymin);
  
   
    
    // Set width to the largest
    if (fabs(track_xmax) > fabs(track_xmin)) {
        layoutWidth = 2.0 * fabs(track_xmax);
    }
    else {
        layoutWidth = 2.0 * fabs(track_xmin);
    }
    
    if (fabs(track_ymax) > fabs(track_ymin)) {
        layoutHeight = 2.0 * fabs(track_ymax);
    }
    else {
        layoutHeight = 2.0 * fabs(track_ymin);
    }
    
    
//     layoutWidth = track_xmax - track_xmin;
//     layoutHeight = track_ymax - track_ymin;
    
}




// Window scale
void game_state::computeWindowScale() {

    // pad out screen size
    double p_screen_x = 0.95 * SCREEN_WIDTH;
    double p_screen_y = 0.95 * SCREEN_HEIGHT; // FIXME: leave room for data display overlay
    
    // calculate x scale
    double x_scale = p_screen_x / layoutWidth;
    
    // calculate y scale
    double y_scale = p_screen_y / layoutHeight;
    
    
    
    // Choose the smallest scale
    if (x_scale < y_scale) {
        SCREEN_SCALE = x_scale;
    }
    else {
        SCREEN_SCALE = y_scale;
    }
    
    
}



/**
 * Load all textures from file
 */
void game_state::load_textures() {
 
 
    // Load the coupler texture
    if (!couplerTexture.loadFromFile(renderer, "textures/coupler.png", COUPLER_LENGTH, COUPLER_WIDTH)) {
        std::cout << "Failed to load coupler.png" << std::endl;
    }
    
    
    // Load the locomotive image
    if (!locoTexture.loadFromFile(renderer, "textures/GP40.png", LOCOMOTIVE_LENGTH, LOCOMOTIVE_WIDTH)) {
        std::cout << "Failed to load GP40.png" << std::endl;
    }
    
    // Load the tank car image
    if (!tankTexture.loadFromFile(renderer, "textures/Tank_Car.png", TANKCAR_LENGTH, TANKCAR_WIDTH)) {
        std::cout << "Failed to load Tank_Car.png" << std::endl;
    }
    
    
    // Load the box car image
    if (!boxTexture.loadFromFile(renderer, "textures/Boxcar.png", BOXCAR_LENGTH, BOXCAR_WIDTH)) {
        std::cout << "Failed to load Boxcar.png" << std::endl;
    }
    
     // Load the box car image
    if (!blueboxTexture.loadFromFile(renderer, "textures/Blue_Boxcar.png", BOXCAR_LENGTH, BOXCAR_WIDTH)) {
        std::cout << "Failed to load Blue_Boxcar.png" << std::endl;
    }
    
    
    // Load the blue tank car image
    if (!bluetankTexture.loadFromFile(renderer, "textures/Blue_Tank_Car.png", TANKCAR_LENGTH, TANKCAR_WIDTH)) {
        std::cout << "Failed to load Blue_Tank_Car.png" << std::endl;
    }
    
    // Load the flat car image
    if (!flatTexture.loadFromFile(renderer, "textures/Flat_Car.png", TANKCAR_LENGTH, TANKCAR_WIDTH)) {
        std::cout << "Failed to load Flat_Car.png" << std::endl;
    }
    
    
 
    
    // Load the railroad tie image
    if (!tieTexture.loadFromFile(renderer, "textures/single_tie.png", TIE_LENGTH, TIE_WIDTH)) {
        std::cout << "Failed to load single_tie.png" << std::endl;
    }
    
    if (!ballastTexture.loadFromFile(renderer, "textures/ballast.png", 1.7 * TIE_LENGTH, 2 * TIE_PITCH)) {
        std::cout << "Failed to load ballast.png" << std::endl;
    }

    
    // Load the track bumper texture
    if (!bumperTexture.loadFromFile(renderer, "textures/bumper.png", BUMPER_DEPTH, BUMPER_WIDTH)) {
        std::cout << "Failed to load bumper.png" << std::endl;
    }
    
    
}


/**
 * Reload all textures
 */
void game_state::reload_textures() {

    couplerTexture.reload();
    
    locoTexture.reload();
    
    tankTexture.reload();
    
    bluetankTexture.reload();
    
    flatTexture.reload();
    
    boxTexture.reload();
    
    blueboxTexture.reload();
    
    tieTexture.reload();
    
    ballastTexture.reload();
    
    bumperTexture.reload();
    
    // FIXME: reload textTexture?
    
}



