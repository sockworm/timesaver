// Image loaders

#include <string>

#include "SDL.h"
#include "SDL_image.h"

#include "img_loaders.hh"





SDL_Surface *load_image(std::string fname) {
    // From tutorial at http://lazyfoo.net/SDL_tutorials/lesson02/index.php

    // Temporary storage for the image that's loaded
    SDL_Surface* loadedImage = NULL;

    // The optimized image that will be used
    SDL_Surface* optimizedImage = NULL;


    // Load the image using SDL_image
    loadedImage = IMG_Load(fname.c_str());


    // IF nothing went wrong in loading the image
    if (loadedImage != NULL) {
        // Create an optimized image
        optimizedImage = SDL_DisplayFormat(loadedImage);

        // Free the old image
        SDL_FreeSurface(loadedImage);

    }


    // Return the optimized image
    return optimizedImage;


}





void apply_surface(const int x, const int y, SDL_Surface* source, SDL_Surface* destination) {
    // From tutorial at http://lazyfoo.net/SDL_tutorials/lesson02/index.php
    
    // Make a temporary rectangle to hold the offsets
    SDL_Rect offset;

    // Give the offsets to the rectangle
    offset.x = x;
    offset.y = y;


    // Blit the surface
    SDL_BlitSurface(source, NULL, destination, &offset);


}


    