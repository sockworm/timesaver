
#include <cmath>
#include <iostream>
#include <string>


#include <SDL.h>
#include <SDL_image.h>

#include "LTexture.hh"

#include "trackWP.hh"


#include "trainCar.hh"
#include "carControl.hh"


#include "trackModel.hh"


// FIXME: write controller to set impulses instead of penalty method
// FIXME: write controller to constrain motion (from Witkin/Baraff/Kass - An introduction to Physically-Based Modeling)


carController::carController() {
 
    // Do something here?
    
}



carController::carController(trainCar* car) {

    // Pass it on down!
    init(car);

}



/** initialize the car controller
 */
void carController::init(trainCar* car) {

    tc = car;
    
    // FIXME: initialize the track segments to something!
}


/** Set the closest track segments for each axle
 */
void carController::set_trackSegments(trackSegment* Aa,
                                      trackSegment* Ab,
                                      trackSegment* Ba,
                                      trackSegment* Bb) {
    
    // FIXME: error checking needed!
    // FIXME: get and store these segments in an array
    
    ts_Aa = Aa;
    ts_Ab = Ab;
    ts_Ba = Ba;
    ts_Bb = Bb;
    
}

/** Track reaction forces controller
 */
void carController::trackControl(const double t) {
    
    
    // Contact flags (left and right sides)
    bool Aa_contact = false;
    bool Ab_contact = false;
    bool Ba_contact = false;
    bool Bb_contact = false;
    
    
    // check kinematics for contact
    
}

/*
double carController::trackControl_contact_force() {
}*/


/** Track reaction forces controller -- penalty method
 */
void carController::trackControl_continuous(const double t) {
    
    // FIXME: implement this as a slot, instead of min_offset
    // FIXME: check for penetration for both on one side, kitty-corner, etc. for each truck
    
    // Car is 9000 kg, so we need 2*9000 = 18000 N for 2 g acceleration
    // Proportional and derivative gains
//     const double kP = 800000.0;
//     const double kD = sqrt(2.0 * kP);
//     const double FR_max = 100000000.0;
    
    const double max_offset = 0.008; // 8 mm minimum offset

    // FIXME: tune wheel/rail reaction controller
    
    
    
    
    double FR_Aa = 0.0;
    double FR_Ab = 0.0;
    double FR_Ba = 0.0;
    double FR_Bb = 0.0; 

  
    // FIXME: calculate position and velocity offsets

    // FIXME: call functions, and use arrays here:
    
    /*
    
    if ((abs(Aa_offset) > min_offset) && (abs(Aa_offset) < max_offset)) {
        if (Aa_offset > 0.0) {
            FR_Aa = calc_F_hertz(Aa_offset - min_offset, Aa_v_offset);
//             std::cout << "FR_Aa + = " << FR_Aa << std::endl;
        }
        else {
            FR_Aa = calc_F_hertz(Aa_offset + min_offset, Aa_v_offset);
//             std::cout << "FR_Aa - = " << FR_Aa << std::endl;
        }
    }

    
    if ((abs(Ab_offset) > min_offset) && (abs(Ab_offset) < max_offset)) {
        if (Ab_offset > 0.0) {
            FR_Ab = calc_F_hertz(Ab_offset - min_offset, Ab_v_offset);
        }
        else {
            FR_Ab = calc_F_hertz(Ab_offset + min_offset, Ab_v_offset);
        }
    }
    
    if ((abs(Ba_offset) > min_offset) && (abs(Ba_offset) < max_offset)) {
        if (Ba_offset > 0.0) {
            FR_Ba = calc_F_hertz(Ba_offset - min_offset, Ba_v_offset);
        }
        else {
            FR_Ba = calc_F_hertz(Ba_offset + min_offset, Ba_v_offset);
        }
    }
    
    if ((abs(Bb_offset) > min_offset) && (abs(Bb_offset) < max_offset)) {
        if (Bb_offset > 0.0) {
            FR_Bb = calc_F_hertz(Bb_offset - min_offset, Bb_v_offset);
        }
        else {
            FR_Bb = calc_F_hertz(Bb_offset + min_offset, Bb_v_offset);
        }
    }

    


    // Saturate the reaction forces
    if (abs(FR_Aa) > FR_max) {
        if (FR_Aa > 0.0) {
            FR_Aa = FR_max;
        }
        else {
            FR_Aa = -FR_max;
        }
    }
    
    if (abs(FR_Ab) > FR_max) {
        if (FR_Ab > 0.0) {
            FR_Ab = FR_max;
        }
        else {
            FR_Ab = -FR_max;
        }
    }
    
    if (abs(FR_Ba) > FR_max) {
        if (FR_Ba > 0.0) {
            FR_Ba = FR_max;
        }
        else {
            FR_Ba = -FR_max;
        }
    }
    
    if (abs(FR_Bb) > FR_max) {
        if (FR_Bb > 0.0) {
            FR_Bb = FR_max;
        }
        else {
            FR_Bb = -FR_max;
        }
    }*/
    

//     tc->setReactionForce(FR_Aa, FR_Ab, FR_Ba, FR_Bb);
    
}



/** 
 * Calculate normal contact force
 */
double carController::calc_F_continuous_damped(double delt, double ddelt) {
    
    double b = 0.0;
    
    const double n = 3.0 / 2.0;
    const double p = 3.0 / 2.0;
    const double q = 1.0;
    const double k = 1.0; // FIXME: define
    
    // Solve for b to give a COR of 0
    if (0.0 == ddelt) {
        b = 1.0; // FIXME: need a better value
    }
    else {
        b = 3.0 * k / (2.0 * ddelt);
    }
    
    std::cout << "F = " << b * pow(delt,p) * pow(ddelt,q) + k * pow(delt,n) << std::endl;
    
    return b * pow(delt,p) * pow(ddelt,q) + k * pow(delt,n);
    
}


/** 
 * Simple Hertz contact law
 */
double carController::calc_F_hertz(double delt, double ddelt) {

    const double kP = 800000.0;
    const double kD = sqrt(2.0 * kP);    
    
    return -kP * delt - kD * ddelt;
    
}



/**
 * Hertz contact law with hysteresis damping
 * 
 * Adapted from Application of a wheel-rail contact model to railway dynamics
 * J. Pombo & J. Ambrosio
 * Multibody System Dynamics, vol 19, p91-114, 2008
 * 
 * Equation (9)
 */
double carController::calc_F_hertz_damped(const double delt, const double ddelt, const double ddelt_i) {
 
    
    // z1 = A1 * x^2 + B1 * y^2
    // z2 = A2 * x^2 + B2 * y^2
    
    // Where: A1 ~ 1 / (2 * r_n)  -- r_n, normal radius of the wheel, from R_o, the rolling radius
    //        B1 ~ 1 / (2 * R_wx) -- r_wx, radius of wheel in x
    //        B2 ~ 1 / (2 * R_rx) -- r_rx, radius of rail in x
    //   (x is along rail axis)
 
    
    
    double A_cval = 1.0; // FIXME: need to calculate, get from [14]
    double B_cval = 1.0; // FIXME: need to calculate, get from [14]
    
    double C_delt = 1.0; // FIXME: need to calculate, get from [40]
    
    
    // FIXME: move these to the class:
    double h_w = 1.0; // FIXME: rail material parameters, get from [14]
    double h_r = 1.0; // FIXME: rail material parameters, get from [14]
    
    
    
    
    // Hertzian constant (from eq (10))
    // (depends on surface curvatures and the elastic properties of the contacting bodies)
    const double K = (4.0 * C_delt) / (3.0 * (h_w + h_r) * sqrt(A_cval + B_cval)); 

    
    const double e = 0.5; // Coefficient of restitution
    
    return (K * (1.0 + ((3.0 * (1.0 - e*e)) / 4.0) * (ddelt / ddelt_i)) * delt);
    
    
}





// Traction controller
void carController::tractionControl() {

    
    // assuming 17.44% power lost to hotel load
    // assuming 2.24 MW power output
    // --> so 1.8493 MW total power available
    
    // Refer to Handbook of Railway Vehicle Dynamics, p255
    // 2006 Taylor & Francis Group, LLC
    
    double P_max = 1849300.0;
    double max_traction_force = 246431.0; // maximum drawbar force
    
    // FIXME: remove scaling factor in carController::tractionControl()
    // scale the maximum values down to improve control
    P_max *= 0.75;
    max_traction_force *= 0.75;
    
    // Calculate current locomotive velocity
    const double vel = tc->getTrackVelocity();
    const double N = tc->getThrottle();
    
    // Torque reduction (N/(m/sec))
    const double k_f = 7.0;
    
   
    
    double traction_force = (N / 8.0) * max_traction_force - k_f * vel;
    
    if (vel <= 10.0) {
        // reduce maximum power if speed is less than 10 m/s
        // Taking a guess here, but a real GP-40 does this at least
        
        P_max *= 0.15;
        
    }
        
    
    
    if (traction_force > (N * N / 64.0) * P_max) {
        // traction for is too high, use alternate model
        if (vel <= 5.0) {
            traction_force = (N * N / 64.0) * max_traction_force;
        }
        else {
            traction_force = (N * N / 64.0) * P_max / vel;
        }
    }
    
  
 
    
    // Set the direction
    if (!(tc->getDirection())) {
        traction_force = -traction_force;
    }

 
 
 
    
    
    double current_brake_force = 0.0;
    
//     // FIXME: brakes have a linear response (change this if real locomotives don't do this)
//     
//     // FIXME: calculate signed velocity at each axle
//     double vel_Aa = 0.0;
//     double vel_Ab = 0.0;
//     double vel_Ba = 0.0;
//     double vel_Bb = 0.0;
//     
//     const double mu_d = 0.0; // FIXME: get friction value
//     
//     // FIXME: calculate brake force at each axle
//     double bf_Aa = -mu_d * vel_Aa;
//     double bf_Ab = -mu_d * vel_Ab;
//     double bf_Ba = -mu_d * vel_Ba;
//     double bf_Bb = -mu_d * vel_Bb;
    
    
    tc->setTractionForce(traction_force);
    
    
}

