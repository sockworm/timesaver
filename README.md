# The timesaver train puzzle 
now with real dynamics

It's missing the puzzle logic, and uncoupling, but the dynamics and track model are there,
 along with a default track layout. A track editor and goal logic will be added soon.

## Requirements

* cmake
* SDL2
* Box2D


To run, it also requires png files of the locomotive, train cars, ties, and ballast. (Those will be distributed later, when things are prettier!)

## Build

Standard cmake build. Create a `build` directory, then `cd` to it and run:

* `cmake ..`
* `make`

Documentation can be created by

* `make doc`

## Playing

### Controls:

* 1 - 8 : set the throttle notch
* 0 : set throttle to idle
* d : flip direction
* s : reset controls (all stop)
* q : quit


