/**
 * Constants and parameters definitions
 * \author Daniel Montrallo Flickinger, PhD
 * \brief
 */

// FIXME: finish documentation

#ifndef CONSTANTS_H
#define CONSTANTS_H



// Screen, viewport settings
// Allow the screen size and scale to change

// #define SCREEN_WIDTH 1920
// #define SCREEN_HEIGHT 1080
extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;
extern double SCREEN_SCALE;

// #define SCREEN_SCALE 11.5
// #define SCREEN_SCALE 35.0


// Allocation
//! maximum track segments
#define MAX_TRACK_SEGMENTS 25
//! maximum turnouts
#define MAX_TURNOUTS 6
//! number of cars
#define NUM_CARS 6
//! maximum track bumpers
#define MAX_BUMPERS 5
//! maximum coupler contacts
#define MAX_COUPLER_CONTACTS 15


// Locomotive parameters:

// NOTE: sync these values from util/car.py

//! locomotive length, bolster to bolster
#define LOCOMOTIVE_BOLSTER_LENGTH 13.1064

//! truck length
#define LOCOMOTIVE_TRUCK_LENGTH 2.74


//! full locomotive length
#define LOCOMOTIVE_LENGTH 18.034

//! locomotive width
#define LOCOMOTIVE_WIDTH 3.14959

//! locomotive mass (111130.0 kg total)
#define LOCOMOTIVE_MASS 77564.0

//! mass of locomotive trucks
#define LOCOMOTIVE_TRUCK_MASS 16783.0

//! Number of axles on each car
#define NUM_AXLES 4

//! coupler length
#define COUPLER_LENGTH 1.2
//! coupler width
#define COUPLER_WIDTH 0.4


// Car dimensions
//! length of tank car
#define TANKCAR_LENGTH 18.034
//! width of tank car
#define TANKCAR_WIDTH 3.14959

//! length of box car
#define BOXCAR_LENGTH 18.034
//! width of box car
#define BOXCAR_WIDTH 3.14959

// Angle of conic section of wheels (1/20)
#define CONIC_ANGLE 0.05



// Track parameters:

//! Half rail width (m)
#define RAIL_WIDTH_2 0.71755

//! play allowed on track (/2)
#define RAIL_EPS 0.008

//! track tie pitch (m)
#define TIE_PITCH 0.4953 

//! length of each tie (m)
#define TIE_LENGTH 2.5908 

//! width of each tie (m)
#define TIE_WIDTH 0.2286 

//! Depth of track bumper (m)
#define BUMPER_DEPTH 1.4 


// Width of track bumper (m)
#define BUMPER_WIDTH 1.4






// Screen-world transformations
// ----------------------------------------------------------------------------

inline int XtoSX(double X) {
    
    return (int)((X * SCREEN_SCALE) + SCREEN_WIDTH / 2.0);
}

inline int YtoSY(double Y) {
    
    return (int)(SCREEN_HEIGHT / 2.0 - (Y * SCREEN_SCALE));
}

inline int PHItoSPHI(double phi) {
    
    return (int)(-phi * 180.0 / M_PI);
    
}

inline double SXtoX(int X) {
    
    return (double)((X - SCREEN_WIDTH / 2.0) / SCREEN_SCALE);
}

inline double SYtoY(int Y) {
    
    return (double)(-(Y - SCREEN_HEIGHT / 2.0) / SCREEN_SCALE);
}



//! Categories of bodies
enum _entityCategory {
    RAIL =     0x0001,
    WHEEL =    0x0002,
    CARBODY =  0x0004,
    COUPLER =  0x0006,
    BUMPER =   0x0008,
};


#endif

