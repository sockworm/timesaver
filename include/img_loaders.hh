/**
 * Image loaders
 * \author Daniel Montrallo Flickinger, PhD
 * \brief Global functions for loading images into SDL
 */
// FIXME: improve description


#ifndef IMG_LOADERS_HH
#define IMG_LOADERS_HH

// Image loaders


/**
 * Load an image and return an SDL surface
 * \param[in] fname file name
 */
SDL_Surface *load_image( std::string fname);


/**
 * Apply a surface
 * \param[in] x X position
 * \param[in] y Y position
 * \param[in] source Source surface
 * \param[in] destination Destination surface (to copy to)
 */
void apply_surface(const int x, const int y, SDL_Surface* source, SDL_Surface* destination);


#endif
