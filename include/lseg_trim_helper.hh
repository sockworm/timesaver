/**
 * Line segment trimming helper
 * \author Daniel Montrallo Flickinger, PhD
 * \brief Global functions to trim line segments around curves
 */
// FIXME: improve description

// FIXME: pull in formula into documentation


#ifndef LSEG_TRIM_HELPER_H
#define LSEG_TRIM_HELPER_H

#define LSEGDATA_DECL const double ax, const double ay, const double bx, const double by, const double cx, const double cy, const double r

//! calculate fx term
double lseg_calc_fx(LSEGDATA_DECL);

//! calculate fy term
double lseg_calc_fy(LSEGDATA_DECL);



//! calculate dx term
double lseg_calc_dx(LSEGDATA_DECL);

//! calculate dy term
double lseg_calc_dy(LSEGDATA_DECL);



//! calculate ex term
double lseg_calc_ex(LSEGDATA_DECL);

//! calculate ey term
double lseg_calc_ey(LSEGDATA_DECL);


//! calculate gamma term
double lseg_calc_gamma(const double dx, const double dy, const double ex, const double ey, const double fx, const double fy);


//! calculate l term
double lseg_calc_l(const double bx, const double by, const double dx, const double dy);


#endif


