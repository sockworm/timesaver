
/**
 * Texture
 * \author Daniel Montrallo Flickinger, PhD
 * \brief Helper class for texture support
 */
// FIXME: improve description




#ifndef LTEXTURE_HH
#define LTEXTURE_HH

// Texture wrapper class
// From: http://lazyfoo.net/tutorials/SDL/10_color_keying/index.php
//   and http://lazyfoo.net/tutorials/SDL/16_true_type_fonts/index.php


/**
 * Helper class for texture support
 * \author Daniel Montrallo Flickinger, PhD
 */
class LTexture {
    
    
private:
    
    //! The actual hardware texture
    SDL_Texture* mTexture;
    
    //! pixels
    void* mPixels;
    
    //! pitch
    int mPitch;

    
    
    // Image dimensions
    int mWidth; //!< image width
    int mHeight; //!< image height
    
    //! Width of the item (in world coordinates)
    double itemWidth;
    
    //! Height of the item (in world coordinates)
    double itemHeight;
    
    //! Filename of image for this texture
    std::string filename;
    
    //! Renderer (don't let it go out of scope!)
    SDL_Renderer *gsr;

public:
    //! Initializes variables
    LTexture();


    //! Deallocates memory
    ~LTexture();


    /**
     * Loads image at specified path
     * \param[in] rr renderer
     * \param[in] fname file name
     * \param[in]
     * \param[in]
     */
    // FIXME: fill out parameter names in declaration
    bool loadFromFile(SDL_Renderer *rr, std::string fname, double, double);
       
    //! Load the image from the stored filename
    void reload();
    
#ifdef _SDL_TTF_H
    /**
     * Creates image from font string
     * \param[in] gsr renderer
     * \param[in] myfont TTF font
     * \param[in] textureText Texture Text
     * \param[in] textColor Text color
     */
    bool loadFromRenderedText(SDL_Renderer *gsr, TTF_Font *myfont, std::string textureText, SDL_Color textColor);
    
#endif
    
    /**
     * Create a blank texture
     * \param[in] gsr renderer
     * \param[in] width texture width
     * \param[in] height texture height
     * \param[in]
     */
    // FIXME: complete documentation for createBlank function
    bool createBlank(SDL_Renderer *gsr, int width, int height, SDL_TextureAccess = SDL_TEXTUREACCESS_STREAMING );


    //! Deallocates texture
    void free();

    /**
     * Set color modulation
     * \param[in] red red channel
     * \param[in] green green channel
     * \param[in] blue blue channel
     */
    void setColor(Uint8 red, Uint8 green, Uint8 blue);

    /**
     * Set blending
     * \param[in] blending blending
     */
    void setBlendMode(SDL_BlendMode blending);

    /**
     * Set alpha modulation
     * \param[in] alpha alpha
     */
    void setAlpha(Uint8 alpha);
    

    /**
     * Renders texture at given point
     * \param[in] gsr renderer
     * \param[in] x X coordinate
     * \param[in] y Y coordinate
     * \param[in] clip clipping region
     * \param[in] angle angle of texture
     * \param[in] center center point of texture
     * \param[in] flip texture image flipping
     */
    void render(SDL_Renderer *gsr, int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);

    /**
     * Set self as render target
     * \param[in] gsr renderer
     */
    void setAsRenderTarget(SDL_Renderer *gsr);
    
    // Get image dimensions
    
    //! Get image width
    int getWidth();
    
    //! Get image height
    int getHeight();

    //Pixel manipulators
    
    //! Lock texture
    bool lockTexture();
    
    //! Unlock texture
    bool unlockTexture();
    
    //! Get pixels in this texture
    void* getPixels();
    
    /**
     * Copy pixels in this texture
     * \param[in] pixels pixels
     */
    void copyPixels( void* pixels );
    
    //! Get the texture pitch
    int getPitch();
    
    /**
     * Get a pixel value
     * \param[in] x X position
     * \param[in] y Y position
     */
    Uint32 getPixel32( unsigned int x, unsigned int y );


};


#endif
