/**
 * Data display
 * \author Daniel Montrallo Flickinger, PhD
 * \brief Displays locomotive parameters
 */
// FIXME: improve description


#ifndef DATADISPLAY_H
#define DATADISPLAY_H


/**
 * Data display
 * \author Daniel Montrallo Flickinger, PhD
 * \brief handles display of locomotive parameters
 */
class dataDisplay {
   
    //! current locomotive
    trainCar* mytc;
//     game_state* mygs;
    
    //! x position of top left corner
    int my_x;
    
    //! y position of top left corner
    int my_y;
    
    //! width of background box
    int my_w;
    
    //! height of background box
    int my_h;
  
    
    // Drawing helpers
    
    /**
     * Draw a bar showing the current throttle setting
     * \param[in] renderer
     * \param[in]
     * \param[in]
     * \param[in]
     */
    // FIXME: fill out parameter names in declaration
    void draw_throttle_bar(SDL_Renderer*, int, int, int, int);
    
    
    /**
     * Draw a single notch for the throttle bar
     * \param[in] renderer
     * \param[in]
     * \param[in]
     * \param[in]
     * \param[in]
     * \param[in]
     */
    // FIXME: fill out parameter names declaration
    void draw_throttle_bar_notch(SDL_Renderer*, int, int, int, int, bool);
    
    
    /**
     * Draw the idle notch for the throttle bar
     * \param[in] renderer
     * \param[in]
     * \param[in]
     * \param[in]
     * \param[in]
     * \param[in]
     */
    // FIXME: fill out parameter names declaration
    void draw_throttle_bar_idle(SDL_Renderer*, int, int, int, int, bool);
    
    /**
     * Draw the direction indicator
     * \param[in] renderer
     * \param[in]
     * \param[in]
     * \param[in]
     * \param[in]
     */
    void draw_direction(SDL_Renderer*, int, int, int, int);
    
    
    /**
     * Draw the background rectangle
     * \param[in] renderer
     */
    void draw_bg_rect(SDL_Renderer*);
    
    
    /**
     * Draw a filled bar
     * \param[in] renderer
     * \param[in] xx X position
     * \param[in] yy Y position
     * \param[in] length lenth of bar
     * \param[in] height height of bar
     * \param[in] curval current value
     * \param[in] maxval maximum value
     */
    void draw_filled_bar(SDL_Renderer*, int xx, int yy, int length, int height, double curval, double maxval);
//     void draw_filled_bar_center(SDL_Renderer*, int xx, int yy, int length, int height, double curval, double minval, double maxval, double centerval);
    
public:
    
    /**
     * default constructor
     */
    dataDisplay();
    
    /**
     * construct a data display for a specified train car
     * \param[in] tc train car
     */
    // FIXME: fill out parameter name in declaration
    dataDisplay(trainCar*);
    
    /**
     * Set dimensions of data display
     */
    void set_dimensions();
    
    
    /**
     * Draw the data display
     * \param[in] renderer
     */
    // FIXME: fill out parameter name in declaration
    void draw(SDL_Renderer*);
    
};


#endif
