/**
 * Track model
 * \author Daniel Montrallo Flickinger, PhD
 * \brief Functions for modeling track in the layout
 * \details models straight and curve segments
 */
// FIXME: improve description




#ifndef TRACKMODEL_H
#define TRACKMODEL_H


#include "constants.hh"

#include <Box2D/Box2D.h>


// Track model
// =======================



//! Curve types
enum CurveType { STRAIGHT, CIRCULAR, CORNU };


/**
 * trackSegment
 * \author Daniel Montrallo Flickinger, PhD
 * \brief Models a single segment of track. Supports tracks as outlined in the CurveType enum; that is, straight lines, circular arcs, and cornu spirals.
 */
class trackSegment {
    
private:
    
    //! Link to the main world
    b2World* myWorld;
    
    //! Rail edge constraint bodies (a)
    b2Body* rail_a;
    
    //! Rail edge constraint bodies (b)
    b2Body* rail_b;
    
    //! curve type of track segment
    CurveType Ctype;
    
    
    //! If track segment is active
    bool isActive;
    
    
    //! If track segment is part of a turnout
    bool isTurnout;
    
    
    //! start point of segment
    trackWP pt_start;
    
    //! end point of segment
    trackWP pt_end;
    
    //! center point
    trackWP pt_center;
    
    //! length of segment
    double my_length;
    
    
    
    //! start angle
    /*!
     * Angle tangent to the start point of the segment
     */
    // FIXME: deprecate start_angle
    double start_angle;
    
    //! end angle
    /*!
     * Angle tangent to the end point of the segment
     */
    // FIXME: deprecate end_angle
    double end_angle;
    

    //! sweep angle
    /*!
     * Angle swept out by the arc. (Zero for straight segments)
     */
    double gamma;
    
    
    //! sign of curve: left: true, right: false
    bool LHRH;
    
    //! segment radius (for circular arc only)
    double radius;  
    
    //! length trimmed from segments
    double l; // FIXME: maybe deprecate length trimmed from segments?
    
    
    // Segment building helper functions:
    
    //! Calculate the endpoint of a curved segment given a start point, LH/RH, radius, and sweep angle
    trackWP calc_endpoint_curved(const trackWP&, bool, double, double);
    
 
    //! initialize rail constraints
    void buildRails();
   
    
    
    /**
     * Quick function to rotate a Box2D vector
     * \todo maybe this is built in to Box2D?
     * \param[in] in input vector
     * \param[in] th theta (rotation angle)
     */
    b2Vec2 calc_rotate(b2Vec2 in, float th);
    
    
    
    
    // Drawing functions:
    // ------------------
    
    /**
     * Draw the ties and ballast of this segment
     * \param[in] rr SDL Renderer handle
     * \param[in] TT tie texture
     * \param[in] BT ballast texture
     */
    void draw_ties(SDL_Renderer* rr, LTexture* TT, LTexture* BT);
    

    /**
     * Draw a very short length of a rail segments (for curves)
     * \param[in] rr SDL Renderer handle
     * \param[in] current_angle current angle
     * \param[in] eps_angle epsilon angle (small angle to rotate through)
     */
    void draw_rail_eps(SDL_Renderer* rr, double current_angle, double eps_angle);

    
    
public:
    
    // Constructors:
    
    //! Default trackSegment constructor
    trackSegment();

    /**
     * Initialize a straight track segment
     * \param[out] start_WP Starting waypoint
     * \param[out] end_WP Ending waypoint
     * \param[in] bw Box2D world handle
     * \param[in] make_active make this segment active
     */
    void init(const trackWP& start_WP, const trackWP& end_WP, b2World* bw, bool make_active);
    
    /**
     * Initialize a circular arc track segment
     * \param[out] sp Starting waypoint
     * \param[out] ep Ending waypoint
     * \param[in] rad Curve radius
     * \param[in] gam Curve gamma
     * \param[in] lr Left/Right flag
     * \param[in] bw Box2D world handle
     * \param[in] make_active make this segment active
     */
    void init_curved(const trackWP& sp, const trackWP& ep, double rad, double gam, bool lr, b2World* bw, bool make_active);
    
    /**
     * Create a standard 10m straight from a start point
     * \param[out]
     * \param[in]
     * \param[in]
     */
    // FIXME: fill out names for parameters in declaration
    void init_straight_15(const trackWP&, b2World*, bool);
    
    
    
    /**
     * Create a standard 20m straight from a start point
     * \param[out]
     * \param[in]
     * \param[in]
     */
    // FIXME: fill out names for parameters in declaration
    void init_straight_20(const trackWP&, b2World*, bool);
    
    /**
     * Create a standard 1/8 LH turn from a start point
     * \param[out]
     * \param[in]
     * \param[in]
     */
    // FIXME: fill out names for parameters in declaration
    void init_curved_L8(const trackWP&, b2World*, bool);
    
    //! Create a standard 1/8 RH turn from a start point
    void init_curved_R8(const trackWP&, b2World*, bool);
    
    
    

    
    
    void setActive();
    void setInactive();
    
    bool Active() const { return isActive; }
    
    
    void enableTurnout() { isTurnout = true; }
    void disableTurnout() { isTurnout = false; }
    
    bool Turnout() const { return isTurnout; }
    
    
 
    
    
    // Data fetching functions:
    
    
    //! Return the start point of the segment
    trackWP startpoint() const { return pt_start; }
    
    //! Return the end point of the segment
    trackWP endpoint() const { return pt_end; }
    
    //! Return the length of the segment
    double length() const { return my_length; }
    
    
    //! Return the segment type
    CurveType get_type() const { return Ctype; }

    
    
    
    // Kinematic functions:
    
    //! Calculate the distance from a point to a track segment
    double calc_distance(const trackWP);

    //! Calculate the lateral distance from a point to a track segment
    // FIXME: change calc_lateral_distance to use direction in track wp
    double calc_lateral_distance(const trackWP, const trackWP, const trackWP);
    
    //! Calculate the lateral velocity from a point to a track segment
    // FIXME: change calc_lateral_velocity to use velocity in track wp
    double calc_lateral_velocity(const trackWP, const trackWP);

    //! Project a vector along the track segment
    double calc_projection(trackWP, double);
    
    //! Project a vector normal to the track segment
    double calc_norm_projection(trackWP, double);
    
    //! Calculate the normal direction of a track segment
    trackWP calc_norm_direction(double);
    
    //! Calculate the closest point on a segment to a given waypoint
    trackWP calc_closest_point(trackWP);
    
    //! Calculate the parametric distance along a track segment
    double calc_param_distance(trackWP);
    
    //! Calculate the point and direction corresponding to a distance along a track segment
    trackWP calc_pt_from_param_distance(const double);

    
    
    
    // Construction functions:

    //! Build a straight segment from end points
    void build_straight();
    

    
    
    // Drawing functions:
    
    //! Draw the track segment
    void draw(SDL_Renderer*, LTexture*, LTexture*);
    
 

    
    
    
};




class trackBumper {
    
private:
    
    //! x position
    double my_x;
    
    //! y position
    double my_y;
    
    //! phi (angle)
    double my_phi;
    
   
    
    //! b2Body
    b2Body* bumperBody;
   
    
    //! Texture for this bumper
    LTexture* bumperTexture;
    
    
public:
    
    //! Constructor
    trackBumper();
    
    //! Initialize track bumper, and add it to the world
    trackBumper(b2World*, b2Vec2, float, LTexture*);
    
    //! Draw the track bumper
    void draw(SDL_Renderer*);

    
    
};



class turnout {
private:
    
    //! First track segment
    trackSegment* segA;
    
    //! Second track segment
    trackSegment* segB;
    
    
    //! Intersection point
    trackWP intersection;
  
    
    //! Center of clicking region
    trackWP click_pt;
    
    //! Click area width
    int click_W;
    
    //! Click area height
    int click_H;
    
    
    //! Set the intersection point
    void set_intersection();
    
    
    //! Set the clicking region
    void set_click_area();
    
    
    //! Check that the active state of the segments is consistent
    void check_segments();
    
    
public:
    
    //! Default constructor
    turnout();
    
    //! Construct a turnout with two segments
    turnout(trackSegment*, trackSegment*);
    
    //! Construct a turnout with two segments, overriding the intersection pt
    turnout(trackSegment*, trackSegment*, const trackWP&);
    
    //! Flip the turnout
    void flip();
    
    
    //! Get the intersection point
    trackWP getIntersection() const { return intersection; }
    
    //! Override to set the intersection point
    void set_intersection(const trackWP& ip) { intersection = ip; }
    
    //! Check if a point is within the bounds of the switch
    bool check_click(double, double);
    
    //! Draw the turnout
    void draw(SDL_Renderer*);
    
    
    
    
};







//! Track
/*!
 * Models a track layout, containing track segments and turnouts
 * 
 */
class track {
    
private:
    
    //! Link to the main world
    b2World* myWorld;
    
    //! Array of track segments belonging to this track
    trackSegment TS[ MAX_TRACK_SEGMENTS ];  // track segments
        
        
    //! Array of turnouts belonging to this track
    turnout TTO[ MAX_TURNOUTS ]; // turnouts
    
    //! Array of track bumpers belonging to this track
    trackBumper myBumpers[MAX_BUMPERS];

    
//     int numWP;

    //! The number of track segments in this track
    int numSegments;
    
    //! The number of turnouts in this track
    int numTurnouts;
    
    //! The number of track bumpers in this track
    int numBumpers;
    
    
 
        
public:
    
    // Constructors:
    
    //! Default track constructor
    track();
//     track(const trackWP&, const trackWP&);
    
    
    //! Initialize a track with two waypoints
    void init(const trackWP&, const trackWP&, b2World*);

    
    // Data fetching functions:
    
    //! Return the number of segments in this track
    int segment_count() const { return numSegments; }
//     int waypoint_count() const { return numWP; }

    //! Return the number of turnouts in this track
    int turnout_count() const { return numTurnouts; }
    
    //! Get a segment belonging to this track
    // FIXME: check bounds, and move get_segment to .cc file:
    trackSegment get_segment(int seg_i) const { return TS[seg_i]; }
    
    //! Get the last segment
    trackSegment get_last_segment() const { return TS[numSegments-1]; }

    //! Get then endpoint of the last segment
    trackWP get_endpoint() const { return TS[numSegments-1].endpoint(); }
    
    //! Get a pointer to the last segment
    trackSegment* get_last_segment_ptr() { return &TS[numSegments-1]; }
    
    //! Get a pointer to the nth from last segment
    trackSegment* get_last_segment_ptr_n(int);
    
    // =========================================================================
    // Construction functions:
    
    // Track segments
    // ------------------------------------------------------------------------
    
    //! Add a segment to this track
    void add_segment(const trackSegment&);
    
    //! Add an initially inactive segment to this track
    void add_segment_inactive(trackSegment&);
    
    
    //! Add a straight segment to this track by single waypoint
    void add_straight_segment_by_WP(const trackWP&);
    
    
    //! Add a standard straight segment to the end of the track
    void add_straight_15(bool);
    
    //! Add a standard straight segment to the end of the track
    void add_straight_20(bool);
    
    //! Add a standard 1/8 LH turn to the end of the track
    void add_curved_L8(bool);
    
    //! Add a standard 1/8 RH turn to the end of the track
    void add_curved_R8(bool);
    
    
    // Turnouts
    // ------------------------------------------------------------------------
    
    //! Add a turnout to this track, try to find the segments by waypoint (BROKEN)
    bool add_turnout(const trackWP&);
    
    //! Add a turnout to this track, by segment indexes
    void add_turnout(int, int);
    
    //! Add a turnout to this track, by segment indexes, overriding the intersection point
    void add_turnout(int, int, const trackWP&);
    
    //! Set the point for a turnout manually
    // FIXME: error checking!
    void setTurnout_pt(int idx, trackWP pt) { TTO[idx].set_intersection(pt); }

    //! Flip a turnout
    void flipTurnout(int);
    
    //! Find a turnout by position
    int findTurnout(double, double);

    
    
    // Track bumpers
    // ------------------------------------------------------------------------
    
    
    //! Add a track bumper
    void addBumper(b2World*, b2Vec2, float, LTexture*);
    
    
    
    // =========================================================================

    
    
    
    
    // FIXME: ? method to find the closest segment given axle center position, and axle rail reaction force direction
    
    //! Find the segment closest to the given waypoint
    int find_closest_segment(const trackWP&);
    
    //! Return a pointer to the closest track segment to the given waypoint
    trackSegment* get_closest_segment(const trackWP&);
    
    
    //! Find the waypoint limits for this track
    void find_limits(double&, double&, double&, double&);
    
 
    
    // Utility functions:
    
    //! Draw all the segments in this track
    void draw(SDL_Renderer*, LTexture*, LTexture*);
    
    //! Print out information about this track, and its segments
    void print();
    
 

};






#endif

