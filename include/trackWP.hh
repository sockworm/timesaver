#ifndef TRACKWP_H
#define TRACKWP_H

#include<iostream>

// Track waypoint/vector
class trackWP {
    
private:
    double my_x;
    double my_y;
    
    double my_dir_x;
    double my_dir_y;
    
    double my_vel_x;
    double my_vel_y; 
    
    double my_acc_x;
    double my_acc_y;
    
public:
    double x() const { return my_x; }
    double y() const { return my_y; }
    
    double dir_x() const { return my_dir_x; }
    double dir_y() const { return my_dir_y; }
    
    double vel_x() const { return my_vel_x; }
    double vel_y() const { return my_vel_y; }
    
    double acc_x() const { return my_acc_x; }
    double acc_y() const { return my_acc_y; }
    
    trackWP();
    trackWP(double, double);
    trackWP(double, double, double, double);
    trackWP(double, double, double, double, double, double);
    trackWP(double, double, double, double, double, double, double, double);
    
    void setx(double xx)  { my_x = xx; }
    void sety(double yy)  { my_y = yy; }
    
    void setx_dir(double xxdir) { my_dir_x = xxdir; }
    void sety_dir(double yydir) { my_dir_y = yydir; }
    
    void setx_vel(double xxvel) { my_vel_x = xxvel; }
    void sety_vel(double yyvel) { my_vel_y = yyvel; }
    void set_vel(double xxvel, double yyvel) { my_vel_x = xxvel; my_vel_y = yyvel; }
    
    void setx_acc(double xxacc) { my_acc_x = xxacc; }
    void sety_acc(double yyacc) { my_acc_y = yyacc; }
    void set_acc(double xxacc, double yyacc) { my_acc_x = xxacc; my_acc_y = yyacc; }
    
    void set(double xx, double yy) { my_x = xx; my_y = yy; }
    void set(double xx, double yy, double xxdir, double yydir) { my_x = xx; my_y = yy; my_dir_x = xxdir; my_dir_y = yydir; }
    void set(double xx, double yy, double xxdir, double yydir, double xxvel, double yyvel) { my_x = xx; my_y = yy; my_dir_x = xxdir; my_dir_y = yydir; my_vel_x = xxvel; my_vel_y = yyvel; }
    void set(double xx, double yy, double xxdir, double yydir, double xxvel, double yyvel, double xxacc, double yyacc) { my_x = xx; my_y = yy; my_dir_x = xxdir; my_dir_y = yydir; my_vel_x = xxvel; my_vel_y = yyvel; my_acc_x = xxacc; my_acc_y = yyacc; }
    
    
    //! Move the position values to the direction values
    void move_to_dir();
    
    //! Rotate the direction by PI (flip)
    void flip() { my_dir_x = -my_dir_x; my_dir_y = -my_dir_y; }
    
    void copy_pos(trackWP);
    void copy_dir(trackWP);
    
    //! Move the point by distance d along the current direction
    void travel(double d);
    
    //! Scale the position values by a scalar
    void scale(double ss) { my_x *= ss; my_y *= ss; }
    
    
    
    //! Rotate the direction
    void rotate(double);
    
    //! Calculate the norm
    void calc_norm();
    
    //! Calculate distance
    double calc_distance();

    //! Return the direction as an angle in degrees
    int calc_degrees();
    
    trackWP operator+(const trackWP&);
    trackWP operator-(const trackWP&);
    
    
    
};
    





#endif
