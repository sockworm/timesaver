/**
 * \author Daniel Montrallo Flickinger, PhD
 * \date May 2014
 * \brief Models a train car
 */


#ifndef TRAINCAR_H
#define TRAINCAR_H

#include "constants.hh"
#include "trackModel.hh"

#include <Box2D/Box2D.h>
#include <array>
// FIXME: change from tr1/array to array, after getting C++11 working

// Global functions to convert positions to screen coordinates

// FIXME: write a destructor to free up the texture and stuff



/**
 * Coupler contact listener
 * \author Daniel Montrallo Flickinger, PhD
 * \brief Models coupler attaching through a callback in the collision system.
 */
class CouplerContactListener : public b2ContactListener {
    
    // FIXME: change std::tr1::array to std::array
    std::array<b2Fixture*, MAX_COUPLER_CONTACTS> myFixture_A; //!< fixture A
    std::array<b2Fixture*, MAX_COUPLER_CONTACTS> myFixture_B; //!< fixture B
    
    int num_contacts; //!< number of contacts
    
        
public:
   
    //! Callback for begin contact
    void BeginContact(b2Contact*);

    //! Check for coupler contacts stored in this class
    int check_for_contacts() const { return num_contacts; }
    
    //! Get fixture A for stored coupler contact
    std::array<b2Fixture*, MAX_COUPLER_CONTACTS> get_fixA() const { return myFixture_A; }
    
    //! Get fixture B for stored coupler contact
    std::array<b2Fixture*, MAX_COUPLER_CONTACTS> get_fixB() const { return myFixture_B; }
        
    
};


/**
 * Train car
 * \author Daniel Montrallo Flickinger, PhD
 * \brief Models a single train car/locomotive. Includes all state data, and methods to perform dynamics, kinematics, and drawing.
 */

class trainCar {

    // FIXME: move more stuff into private!
    
    
private:

    
    //! Dynamic body for this car
    b2Body* myBody;
    
    //! Front coupler body
    b2Body* myCouplerF;
    
    //! Rear coupler body
    b2Body* myCouplerR;
    
    //! Texture for this car
    LTexture* carTexture;
    
    //! Texture for the coupler
    LTexture* couplerTexture;


    
    // Control parameters:
    
    //! Current throttle position (notches 1-8, 0 for idle)
    unsigned short int throttle;
    
    //! Current brake setting
    unsigned short int brake;
    
    //! Forward/reverse setting: true for forward, false for reverse
    bool pw_direction;
    
    
    
    
    // Parameters defined in car.py:
    
    //! car length (bolster to bolster)
    double cL;
    
    double tL;  //!< truck length (axle to axle)
    
    double cFL; //!< full car length
    double cW; //!< car width
    
    double cM; //!< car mass
    double tM; //!< truck mass
    
    
    // Bolsters
    trackWP Ao; //!< bolster A
    trackWP Bo; //!< bolster B
    
 
    
    // kinematic positions, velocities, etc (axle centers)
    trackWP my_axles[NUM_AXLES]; //!< axle centers
    
    

   
    
    // Goal bounds
    
    //! Center point, direction of goal region
    trackWP my_goal_pt; 
    
    //! Length of goal region box
    double my_goal_l;
    
    //! Width of goal region box
    double my_goal_w;
    

    
    // Car color
    // TODO: move into a struct, use uint8 instead
    //! car color, red channel
    int carColor_R;
    
    //! car color, green channel
    int carColor_G;
    
    //! car color, blue channel
    int carColor_B;
  
    
    
    
public:

    
    // Constructors
    // TODO: use default parameters instead of multiple constructors
    trainCar();
    trainCar(double, double);
    trainCar(double, double, double);
    trainCar(double, double, double, double, double);
    
    
 
    

    
    //! Get waypoint of axle (position, direction, velocity)
    trackWP getAxle(int i) const { return my_axles[i]; } 
//     trackWP getAxles(trackWP[] &) const { } // FIXME: return the whole array

    //! Get track velocity
    double getTrackVelocity();
    
 
    //! Set the body
    void setBody(b2Body* cb) { myBody = cb; }
    
    //! Set the bodies for the couplers
    void setCouplerBodies(b2Body* cbf, b2Body* cbr) { myCouplerF = cbf; myCouplerR = cbr; }

    
    //! Set the textures
    void setTexture(LTexture* tt, LTexture* ct) { carTexture = tt; couplerTexture = ct; }
    
    
    // Control functions:
    // ------------------
    
    //! Set throttle position (0 - 8)
    // FIXME: move to uint8
    void setThrottle(unsigned short int);
    
    //! Get current throttle position (0 - 8) as a double
    double getThrottle() const { return (double)(throttle); }
    
    
    //! Set brake setting
    // FIXME: move to uint8
    void setBrake(unsigned short int);
    
    //! Get current brake setting as a double
    double getBrake() const { return (double)(brake); }
    
    //! Flip direction
    void flipDirection();
    
    //! Return current direction
    bool getDirection() const { return pw_direction; }
    
  

    // FIXME: include parameter names here!
    /**
     * Set the traction force
     * \param[in]
     */
    void setTractionForce(double);
    
    
    //! Set the friction force
    void setFrictionForce();
    
    
    /**
     * Apply forces to the wheels
     * \param[in]
     */
    void applyWheelForce(double);
 
    
    //! DEPRECATED: Calculate kinematics
//     void updateKinematics();
    
    
 
    //! DEPRECATED: Enforce limits
//     void enforceLimits();

//     // DEPRECATED: Return bolster positions
//     void getBolsters(double bv[]);

    


 

    //! Calculate the X screen position for this car
    int calcSX();
    
    //! Calculate the Y screen position for this car
    int calcSY();
    
    //! Calculate the screen rotation for this car
    int calcSP();
    

    /**
     * Draw this car
     * \param[in]
     */
    void draw(SDL_Renderer*);

 
    /**
     * Draw the goal region for this car
     * \param[in]
     */
    void drawGoal(SDL_Renderer*);
    
    
    //! Return the center and direction of the goal box
    trackWP getGoalCenter() const { return my_goal_pt; }
    
    //! Return true if car is currently inside goal box
    bool isInGoal();
    
    /**
     * Set goal box region
     * \param[in]
     * \param[in]
     * \param[in]
     */
    void setGoal(trackWP, double, double);


    
};

#endif


