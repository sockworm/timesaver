/**
 * Car controller
 * \author Daniel Montrallo Flickinger, PhD
 * \brief Controls car parameters
 */


#ifndef CARCONTROL_H
#define CARCONTROL_H

#include "trainCar.hh"

// FIXME: calculate lateral displacement (or shift), y, and yaw angle, alpha
// FIXME: model conicity, gamma

// FIXME: lateral displacement limit of +- 8mm

// FIXME: model wheel as a cone, with a flange at 8mm


/**
 * car controller
 * \author Daniel Montrallo Flickinger, PhD
 * \brief car controller class
 */
// FIXME: finish documentation for car controller class
class carController {

    trackSegment *ts_Aa; //!< track segment under Aa axle
    trackSegment *ts_Ab; //!< track segment under Ab axle
    
    trackSegment *ts_Ba; //!< track segment under Ba axle
    trackSegment *ts_Bb; //!< track segment under Bb axle
    
    trainCar *tc; //!< train car
    
    
public:
    
    // constructor:
  
    //! Default constructor
    carController();
    
    /**
     * Create a controller bound to a traincar
     * \param[in]
     */
    // FIXME: define parameter names
    carController(trainCar*);
    
    
    
    // Utility functions:
    
    /**
     * initialize
     * \param[in]
     */
    void init(trainCar*);
    
    /**
     * Set the closest track segments for each axle
     * \param[in]
     * \param[in]
     * \param[in]
     * \param[in]
     */
    void set_trackSegments(trackSegment*, trackSegment*, trackSegment*, trackSegment*);
    
    
    // Controllers:
    
    /**
     * Track reaction forces controller
     * \param[in]
     */
    void trackControl(const double);
    
    /**
     * Track reaction forces controller (penalty method)
     * \param[in]
     */
    void trackControl_continuous(const double);
    
    // DEPRECATED: Track controller that enforces the bilateral constraint exactly
//     void trackControl_constrained(const double);

    /**
     * Calculate delta (penetration distance)
     * \param[in]
     * \param[in]
     * \param[in]
     */
    void calc_delta(double&, double&, double&);
    
    
    /**
     * Continuous damping controller, penalty method
     * \param[in]
     * \param[in]
     */
    double calc_F_continuous_damped(double, double);
    
    /**
     * Simple Hertzian contact model
     * \param[in]
     * \param[in]
     */
    double calc_F_hertz(double, double);

    /**
     * Hertz contact law with hysteresis damping
     * \param[in]
     * \param[in]
     * \param[in]
     */
    double calc_F_hertz_damped(const double, const double, const double);
    
    //! Traction controller
    void tractionControl();

};

#endif
