/**
 * Game state
 * \author Daniel Montrallo Flickinger, PhD
 * \brief Main game state functionality, Initialization, updating, data storage
 */
// FIXME: improve description



#ifndef GAME_STATE_HH
#define GAME_STATE_HH

#include "constants.hh"

#include <Box2D/Box2D.h>


// Functions for game state: init, etc.




/**
 * Main game state class
 * \author Daniel Montrallo Flickinger
 * \brief The main class for all the game states, with init, update, draw, and close
 */
class game_state {

private:
 
    //! Last screen width (used to check for window resize events)
    int last_screen_width;
    
    //! Last screen height (used to check for window resize events)
    int last_screen_height;
    
    
    //! Gravity vector
    b2Vec2 gravity;
    
    //! The whole Box2D world
    b2World world;
    
    // FIXME: move to std::vector for main car bodies
    //! Main car bodies
    b2Body* carBody[NUM_CARS];
    
    //! Truck body A
    b2Body* truckBody_a;
    
    //! Truck body B
    b2Body* truckBody_b;
    
    //! Wheel body
    b2Body* wheelBody;
    
    // FIXME: move to std::vector for coupler bodies
    //! Coupler body A
    b2Body* couplerBody_F[NUM_CARS];
    
    // FIXME: move to std::vector for coupler bodies
    //! Coupler body B
    b2Body* couplerBody_R[NUM_CARS];
    
    // FIXME: move to std::vector for coupler joints
    //! Coupler joints (A)
    b2RevoluteJoint* coupler_joint_a[NUM_CARS];
    
    // FIXME: move to std::vector for coupler joints
    //! Coupler joints (B)
    b2RevoluteJoint* coupler_joint_b[NUM_CARS];
    
    
    //! Contact callback listener
    CouplerContactListener myContactListener;

    //! Timestep for Box2D
    float32 timeStep;
    
    //! Number of velocity iterations for Box2D
    int32 velocityIterations;
    
    //! Number of position iterations for Box2D
    int32 positionIterations;

    
    // FIXME: move to std::vector for train cars
    //! Train cars
    trainCar mycar[NUM_CARS];


    
    // TODO: pull in textures from an XML file or something
    //! Coupler texture
    LTexture couplerTexture;

    //! Locomotive texture
    LTexture locoTexture;
    
    //! Tank car texture
    LTexture tankTexture;
    
    //! Blue tank car texture
    LTexture bluetankTexture;
    
    //! Flat car texture
    LTexture flatTexture;
    
    //! Box car texture
    LTexture boxTexture;
    
    //! Blue box car texture
    LTexture blueboxTexture;
    
    //! Railway tie texture
    LTexture tieTexture;
    
    //! Railway ballast texture
    LTexture ballastTexture;
    
    //! Track bumper texture
    LTexture bumperTexture;
    
    //! Texture for rendering text
    LTexture textTexture;


//     track myTracks[];
    //! the main track
    track myTracks;

    //! Width of layout
    double layoutWidth;
    
    //! Height of layout
    double layoutHeight;
    
    
    //! Car controller
    carController cc;

    
    //! Data display
    dataDisplay ddisp;
    
    //! Flag for cheating at dynamics
    bool cheat_dynamics;
    
    
    
    // Dynamic system setup functions
    
    /**
     * Add a wheel
     * \param[in]
     * \param[in]
     * \param[in]
     * \param[in]
     * \param[in]
     * \param[in]
     * \param[in]
     */
    // FIXME: fill out parameter names in declaration
    void addWheel(b2Body*, b2BodyDef, b2FixtureDef, float, float, float, float);
    
    /**
     * Add a coupler
     * \param[out]
     * \param[in]
     * \param[in]
     */
    // FIXME: fill out parameter names in declaration
    void addCoupler(b2Body*&, float, float);
    
    /**
     * Add a truck
     * \param[out]
     * \param[in]
     * \param[in]
     */
    // FIXME: fill out parameter names in declaration
    void addTruck(b2Body*&, float, float);
    
    /**
     * Add a car to the world (including trucks and wheels)
     * \param[out]
     * \param[out]
     * \param[out]
     * \param[in]
     * \param[in]
     * \param[in]
     */
    // FIXME: fill out parameter names in declaration
    void addCarBody(b2Body*&, b2Body*&, b2Body*&, float, float, int);
    
    
    //! Compute the layout width and height
    void computeLayoutLimits();
    
    //! Compute the current window scale
    void computeWindowScale();
    
    //! Load all textures
    void load_textures();
    
    //! Reload all textures
    void reload_textures();
    
public:

    // Main window and surface
    
    //! Main SDL window
    SDL_Window *window;
    
    //! Main SDL surface
    SDL_Surface *screenSurface;

    // FIXME renderer was moved to global: OR WAS IT??
    //! The window renderer 
    SDL_Renderer *renderer;

#ifdef _SDL_TTF_H
    
    //! SDL font
    TTF_Font *font;

#endif    
    
    // Current displayed texture
//     SDL_Texture *texture;

//     // switch engine image
//     SDL_Su#include "trainCar.hh"

    
    //! Default constructor
    game_state();
    
    
    // Standard game loop methods:
    
    //! Initialize a new game
    bool init();
    
    //! Close and clean up game state
    bool close();

    //! Update the current game state
    bool update();
    
    //! Draw the current game state
    bool draw();

    
    // Input and control:
    
    /**
     * Send input to the controller
     * \param[in] control_code Control code
     */
    void send_control(unsigned short int control_code);

    /**
     * Process a mouse click
     * \param[in]
     * \param[in]
     */
    // FIXME: fill out parameter names in declaration
    void process_click(int, int);
    
    
    
    // Track setup functions:
    
    //! Create a simple track for testing
    void create_track_simple();
    
    //! Create a timesaver track layout
    void create_track_timesaver();
    
    
    //! Create a timesaver track layout (the old way!)
    // FIXME: deprecate create_track_timesaver_old()
    void create_track_timesaver_old();
    
    
};



#endif
