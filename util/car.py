
# Daniel Montrallo Flickinger, PhD
# May 2014

"""
Dynamics for a single train car
"""


# FIXME: put constraints into dynamics here

import numpy as np

import sympy
import sympy.physics.mechanics as mech


from sympy.physics.vector import *
from sympy.matrices import Matrix

# For printing out c code
from sympy import ccode

from sympy.solvers import solve


def calcCarAccel():
    
    # Parameters:
    # NOTE: change these values in src/trainCar.cc too
    #   -- trainCar::trainCar()
    
    # Data from an EMD GP40
    # http://www.thedieselshop.us/Data%20EMD%20GP40.HTML
    
    cL = 13.1064 # car length (bolster to bolster)
    cFL = 18.034 # full car length
    cW = 3.14959 # car width
    
    tL = 2.74 # truck length
    tW = 2.438 # truck width
    
    cM = 110130.0 # car mass
    tM = 500.0 # truck mass
    
    # Make the parameters symbolic instead:
    #cL = sympy.symbols('cL')   # car length (bolster to bolster)
    #cFL = sympy.symbols('cFL') # full car length
    #cW = sympy.symbols('cW')   # car width
    
    #tL = sympy.symbols('tL') # truck length
    #tW = sympy.symbols('tW') # truck width
    
    #cM = sympy.symbols('cM') # car mass
    #tM = sympy.symbols('tM') # truck mass
    
    rpAa_x = sympy.symbols('rpAa_x')
    rpAa_y = sympy.symbols('rpAa_y')
    
    rpAb_x = sympy.symbols('rpAb_x')
    rpAb_y = sympy.symbols('rpAb_y')
    
    rpBa_x = sympy.symbols('rpBa_x')
    rpBa_y = sympy.symbols('rpBa_y')
    
    rpBb_x = sympy.symbols('rpBb_x')
    rpBb_y = sympy.symbols('rpBb_y')
    
    
    
    # Define configuration parameters and their derivatives
    q1, q2, q3, q4, q5, u1, u2, u3, u4, u5 = mech.dynamicsymbols('q1 q2 q3 q4 q5 u1 u2 u3 u4 u5')
    q1d, q2d, q3d, q4d, q5d, u1d, u2d, u3d, u4d, u5d = mech.dynamicsymbols('q1 q2 q3 q4 q5 u1 u2 u3 u4 u5', 1)
    
    
    # Define forces
    # FT - traction force
    # Fa - Coupling force on A end
    # Fb - Coupling force on B end
    # RAa - reaction force on axle Aa (normal)
    # RAb - reaction force on axle Ab (normal)
    # RBa - reaction force on axle Ba (normal)
    # RBb - reaction force on axle Bb (normal)
    # RAaT - reaction force on axle Aa (tangential)
    # RAbT - reaction force on axle Ab (tangential)
    # RBaT - recation force on axle Ba (tangential)
    # RBbT - reaction force on axle Bb (tangential)
    FT, Fa, Fb, RAa, RAb, RBa, RBb, RAaT, RAbT, RBaT, RBbT = mech.dynamicsymbols('FT, Fa, Fb, RAa, RAb, RBa, RBb, RAaT, RAbT, RBaT, RBbT')
    
    
    # Inertial reference frame
    N = mech.ReferenceFrame('N')
    
    
    # Reference frames for each body
    TA = mech.ReferenceFrame('TA') # Truck A
    TB = mech.ReferenceFrame('TB') # Truck B
    FC = mech.ReferenceFrame('FC') # car body
    
    
    # Frame orientations
    FC.orient(N, 'Axis', [q3, N.z])
    TA.orient(FC, 'Axis', [q4, FC.z])
    TB.orient(FC, 'Axis', [q5, FC.z])
    
    
    # Points
    #--------
    
    
    # Body centers
    No = mech.Point('No')
    Co = mech.Point('Co')
    
    Ao = mech.Point('Ao')
    Bo = mech.Point('Bo')
    
    # Coupler points
    Ca = mech.Point('Ca')
    Cb = mech.Point('Cb')
    
    # Rail contact points
    Aa = mech.Point('Aa')
    Ab = mech.Point('Ab')
    
    Ba = mech.Point('Ba')
    Bb = mech.Point('Bb')
    
    # Points on rails for rail contact
    rpAa = mech.Point('rpAa')
    rpAb = mech.Point('rpAb')
    rpBa = mech.Point('rpBa')
    rpBb = mech.Point('rpBb')
    
    
    # Set positions
    #--------------
    Co.set_pos(No, q1 * N.x + q2 * N.y)
    
    Ca.set_pos(Co, (-cFL / 2) * FC.x)
    Cb.set_pos(Co, (cFL / 2) * FC.x)
    
    Ao.set_pos(Co, (-cL / 2) * FC.x)
    Bo.set_pos(Co, (cL / 2) * FC.x)
    
    Aa.set_pos(Ao, (-tL / 2) * TA.x)
    Ab.set_pos(Ao, (tL / 2) * TA.x)
    
    Ba.set_pos(Bo, (-tL / 2) * TB.x)
    Bb.set_pos(Bo, (tL / 2) * TB.x)
    
    
    rpAa.set_pos(No, rpAa_x * N.x + rpAa_y * N.y)
    rpAb.set_pos(No, rpAb_x * N.x + rpAb_y * N.y)
    rpBa.set_pos(No, rpBa_x * N.x + rpBa_y * N.y)
    rpBb.set_pos(No, rpBb_x * N.x + rpBb_y * N.y)
    
    
    # Velocities and accelerations
    FC.set_ang_vel(N, u3 * N.z)
    
    TA.set_ang_vel(FC, u4 * FC.z)
    TB.set_ang_vel(FC, u5 * FC.z)
    
    FC.set_ang_acc(N, FC.ang_vel_in(N).dt(N))
    
    TA.set_ang_acc(N, TA.ang_vel_in(N).dt(N))
    TB.set_ang_acc(N, TB.ang_vel_in(N).dt(N))
    
    
    # Define linear velocities and accelerations
    No.set_vel(N, 0)
    
    Co.set_vel(N, u1 * N.x + u2 * N.y)
    
    Ao.set_vel(FC, 0)
    Bo.set_vel(FC, 0)
    
    Ca.v2pt_theory(Co, N, FC)
    Ca.a2pt_theory(Co, N, FC)
    
    Cb.v2pt_theory(Co, N, FC)
    Cb.a2pt_theory(Co, N, FC)
    
    Ao.v2pt_theory(Co, N, FC)
    Ao.a2pt_theory(Co, N, FC)
    
    Bo.v2pt_theory(Co, N, FC)
    Bo.a2pt_theory(Co, N, FC)
    
    
    Aa.v2pt_theory(Ao, N, TA)
    Aa.a2pt_theory(Ao, N, TA)
    Ab.v2pt_theory(Ao, N, TA)
    Ab.a2pt_theory(Ao, N, TA)
    
    Ba.v2pt_theory(Bo, N, TB)
    Ba.a2pt_theory(Bo, N, TB)
    Bb.v2pt_theory(Bo, N, TB)
    Bb.a2pt_theory(Bo, N, TB)
    
    
    # Define inertias
    # (F, ixx, iyy, izz, ixy, iyz, ixz)
    C_inertia_dyadic = mech.inertia(FC, 0.0, 0.0, cM * (cW**2 + cL**2) / 12.0, 0.0, 0.0, 0.0)
    A_inertia_dyadic = mech.inertia(TA, 0.0, 0.0, tM * (tW**2 + tL**2) / 12.0, 0.0, 0.0, 0.0)
    B_inertia_dyadic = mech.inertia(TB, 0.0, 0.0, tM * (tW**2 + tL**2) / 12.0, 0.0, 0.0, 0.0)
    
    C_inertia_tuple = (C_inertia_dyadic, Co)
    A_inertia_tuple = (A_inertia_dyadic, Ao)
    B_inertia_tuple = (B_inertia_dyadic, Bo)
    
    
    
    # Define rigid bodies
    BDc = mech.RigidBody('carC', Co, FC, cM, C_inertia_tuple)
    BDa = mech.RigidBody('truckA', Ao, TA, tM, A_inertia_tuple)
    BDb = mech.RigidBody('truckB', Bo, TB, tM, B_inertia_tuple)
    
    
    ## Kinematical differential equations ##
    kinematical_diff_eqns = [q1d - q1.diff(),
    q2d - q2.diff(),
    q3d - q3.diff(),
    q4d - q4.diff(),
    q5d - q5.diff()]
    
    
    ## Kinetics ##
    
    ## Equations of motion with Kane's method ##
    
    # make a list of the bodies
    bodyList = [BDa, BDb, BDc]
    
    # Define forces
    FF_FT = (Bo, FT * TB.x) # tractive force
    
    FF_Fa = (Ca, Fa * FC.x) # drawbar force (A)
    FF_Fb = (Cb, Fb * FC.x) # drawbar force (B)
    
    FF_RAa = (Aa, RAa * TA.y) # reaction force (rail)
    FF_RAb = (Ab, RAb * TA.y) # reaction force (rail)
    
    FF_RBa = (Ba, RBa * TB.y) # reaction force (rail)
    FF_RBb = (Bb, RBb * TB.y) # reaction force (rail)
    
    FF_RAaT = (Aa, RAaT * TA.x) # reaction force (rail, tangential)
    FF_RAbT = (Ab, RAbT * TA.x) # reaction force (rail, tangential)
    
    FF_RBaT = (Ba, RBaT * TB.x) # reaction force (rail, tangential)
    FF_RBbT = (Bb, RBbT * TB.x) # reaction force (rail, tangential)
    
    
    forceList = [FF_FT, FF_Fa, FF_Fb, FF_RAa, FF_RAb, FF_RBa, FF_RBb, FF_RAaT, FF_RAbT, FF_RBaT, FF_RBbT]
    
    
    # Generalized coordinates
    coordinates = [q1, q2, q3, q4, q5]
    
    # Generalized speeds
    speeds = [u1, u2, u3, u4, u5]
    
    
    # Solve constraints
    print 'Solving configuration constraints'
    
    cfg_constraints_unsolved = [Aa.pos_from(rpAa) & N.x, Aa.pos_from(rpAa) & N.y, Ab.pos_from(rpAb) & N.x, Ab.pos_from(rpAb) & N.y, Ba.pos_from(rpBa) & N.x, Ba.pos_from(rpBa) & N.y, Bb.pos_from(rpBb) & N.x, Bb.pos_from(rpBb) & N.y]
    
    #print cfg_constraints_unsolved
    #print solve(cfg_constraints_unsolved, coordinates)
    
    
    ## It's dynamics time! Yay!! ##
    print 'Solving dynamics'
    
    # Create Kane's method object
    kane = mech.KanesMethod(N, q_ind=coordinates, u_ind=speeds, kd_eqs=kinematical_diff_eqns)
    
    # Compute the equations of motion using Kane's method
    fr, frstar = kane.kanes_equations(forceList, bodyList)
    zero = fr + frstar
    
    
    
    kindiff_dict = kane.kindiffdict()
    
    
    mass_matrix = kane.mass_matrix.subs(kindiff_dict)
    
    
    print 'Mass matrix:'
    mech.mprint(mass_matrix)
    
    
    forcing_vector = kane.forcing.subs(kindiff_dict)
    
    print 'Forcing vector:'
    mech.mprint(forcing_vector)
    
    
    print 'Calculating mass matrix inverse ...'
    mass_matrix_inverse = mass_matrix.inv()
    print '   ... done'
    
    
    
    
    print 'Calculating velocities ...'
    print('Aa:')
    print Aa.vel(N).express(N)
    
    print('Ab:')
    print Ab.vel(N).express(N)
    
    print('Ba:')
    print Ba.vel(N).express(N)
    
    print('Bb:')
    print Bb.vel(N).express(N)
    
    
    
    #print 'Calculating Jacobian transpose'
    #Jactran = 
    
    
    print 'Calculating accelerations'
    accel = mass_matrix_inverse * forcing_vector
    
    
    
    print 'Accelerations:'
    mech.mprint(accel)
    
    
    
    #print 'Calculating constraints'
    #aux = kane.auxiliary_eqs.applyfunc(simplify_auxiliary_eqs)
    
    #mprint(aux)
    
    
    return accel
    
    
    
    
def simplify_auxiliary_eqs(w):
    return signsimp(trigsimp(factor_terms(w)))
    
    
def writeAccel(accel):

    fname = 'mdlDerivatives'

    print 'Transferring closed form equations for accelerations into C++ source files ' + fname + '.cc and ' + fname + '.hh'

    aa_i, aa_j = accel.shape    
    print "Acceleration vector is " + str(aa_i) + 'x' + str(aa_j)


    # open the .cc and .hh files for writing
    fh = open(fname + '.cc', 'w')
    fh_h = open(fname + '.hh', 'w')

    # Start writing the first parts of the .cc and .hh files
    writeFileHeaders(fname, fh, fh_h, aa_i)
    fh.write('    double const *params = static_cast<double const *>(_params);\n\n')
    
    
    

    
    ## Write out derivatives:
    fh.write('    // Pass through velocities\n')
    for incr_u in range(0, aa_i):
        fh.write('    dqdt[' + str(incr_u) + '] = q[' + str(incr_u + aa_i) + '];\n')
    fh.write('\n\n')
    
    
    for incr_i in range(0, aa_i):
        
        this_code = ccode(accel[incr_i, 0], assign_to='dqdt[' + str(incr_i + aa_i) + ']')
        this_code = subsQs(this_code, aa_i)
        
        fh.write(this_code)
        fh.write('\n\n')
    
        
    
    # Write footer
    fh.write('\n\nreturn GSL_SUCCESS;\n')
    fh.write('\n\n\n}\n\n\n')
    
    
    fh_h.write('#endif\n')
    
    
    fh.close()
    fh_h.close()
    
    
    
    
def writeFileHeaders(fname, fh, fh_h, nj):


    function_declare = 'int mdlDerivatives(double t, const double q[], double dqdt[], void *_params)'

    file_header = '// Daniel Montrallo Flickinger, PhD\n// Generated by generateDynamics.py\n\n// Calculates the derivatives of the dynamics system of a single rail car, given current states\n\n'
    
    # Write the C file
    fh.write(file_header)
    fh.write('\n#include<math.h>\n\n')
    fh.write('#include <gsl/gsl_errno.h>\n')
    fh.write('#include <gsl/gsl_odeiv2.h>\n')
    fh.write('#include <gsl/gsl_linalg.h>\n\n')
    fh.write('#include \"' + fname + '.hh\"\n\n')    
    fh.write(function_declare + ' {\n\n')


    # Write the header file
    fh_h.write(file_header)
    fh_h.write('#ifndef ' + fname.upper() + '_H\n#define ' + fname.upper() + '_H\n\n\n')
    fh_h.write('#define NUM_STATES ' + str(nj * 2) + '\n')
    fh_h.write('#define NUM_JOINTS ' + str(nj) + '\n\n')
    fh_h.write(function_declare + '; \n\n')

    
    
    
def subsQs(ccstr, qn):

    # Replace all qn(t) with q[n]
    for incr_q in range(0, qn):
    
        this_old = str(incr_q + 1) + '(t)'

        
        this_old_q = 'q' + this_old
        this_new_q = 'q[' + str(incr_q) + ']'
        
        this_old_u = 'u' + this_old
        this_new_u = 'q[' + str(incr_q + qn) + ']'
        
        ccstr = ccstr.replace(this_old_q, this_new_q)
        ccstr = ccstr.replace(this_old_u, this_new_u)

        
    # Replace all forces
    ccstr = ccstr.replace('FT(t)', 'params[0]')
    ccstr = ccstr.replace('Fa(t)', 'params[1]')
    ccstr = ccstr.replace('Fb(t)', 'params[2]')
    ccstr = ccstr.replace('RAa(t)', 'params[3]')
    ccstr = ccstr.replace('RAb(t)', 'params[4]')
    ccstr = ccstr.replace('RBa(t)', 'params[5]')
    ccstr = ccstr.replace('RBb(t)', 'params[6]')
    ccstr = ccstr.replace('RAaT(t)', 'params[7]')
    ccstr = ccstr.replace('RAbT(t)', 'params[8]')
    ccstr = ccstr.replace('RBaT(t)', 'params[9]')
    ccstr = ccstr.replace('RBbT(t)', 'params[10]')
        
        
    return ccstr
    
 