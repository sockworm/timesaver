 
# Solve inverse kinematics for a train car


from sympy.solvers import solve
from sympy import Symbol

# For printing out c code
from sympy import ccode

q0 = Symbol('q0')
q1 = Symbol('q1')

# cos and sin of q2
cq2 = Symbol('cq2')
sq2 = Symbol('sq2')

cq3 = Symbol('cq3')
sq3 = Symbol('sq3')

cq4 = Symbol('cq4')
sq4 = Symbol('sq4')

q5 = Symbol('q5')
q6 = Symbol('q6')
q7 = Symbol('q7')
q8 = Symbol('q8')
q9 = Symbol('q9')


cL = Symbol('cL')
tL = Symbol('tL')


# Positions:
Aa_x = Symbol('Aa_x')
Aa_y = Symbol('Aa_y')

Ab_x = Symbol('Ab_x')
Ab_y = Symbol('Ab_y')

Ba_x = Symbol('Ba_x')
Ba_y = Symbol('Ba_y')

Bb_x = Symbol('Bb_x')
Bb_y = Symbol('Bb_y')


# Velocities:
vAa_x = Symbol('vAa_x')
vAa_y = Symbol('vAa_y')

vAb_x = Symbol('vAb_x')
vAb_y = Symbol('vAb_y')

vBa_x = Symbol('vBa_x')
vBa_y = Symbol('vBa_y')

vBb_x = Symbol('vBb_x')
vBb_y = Symbol('vBb_y')


Ao_x = q0 - (cL / 2) * cq2
Ao_y = q1 - (cL / 2) * sq2

Bo_x = q0 + (cL / 2) * cq2
Bo_y = q1 + (cL / 2) * sq2
    
# calculate axle centers:
eq1 = Aa_x - Ao_x - (tL / 2) * cq3 * cq2
eq2 = Aa_y - Ao_y - (tL / 2) * sq3 * sq2
    
eq3 = Ab_x - Ao_x + (tL / 2) * cq3 * cq2
eq4 = Ab_y - Ao_y + (tL / 2) * sq3 * sq2
    
eq5 = Ba_x - Bo_x - (tL / 2) * cq4 * cq2
eq6 = Ba_y - Bo_y - (tL / 2) * sq4 * sq2
    
eq7 = Bb_x - Bo_x + (tL / 2) * cq4 * cq2
eq8 = Bb_y - Bo_y + (tL / 2) * sq4 * sq2
    
    
print solve([eq1, eq2, eq3, eq4, eq5, eq6, eq7, eq8], [q0, q1, cq2, sq2, cq3, sq3, cq4, sq4])


# Solve velocity inverse kinematics
eq9 = -vAa_x + (-sq2*cq3 - sq3*cq2)*(-(tL / 2)*q7 - (tL / 2)*q8) + q5 + (cL / 2) *q7*sq2
eq10 = -vAa_y + (-sq2*sq3 + cq2*cq3)*(-(tL / 2)*q7 - (tL / 2)*q8) + q6 - (cL / 2) *q7*cq2;


print "velocity kinematics (Aa)"
print solve([eq9, eq10], [q5,q6,q7])

eq11 =  -vAb_x + (-sq2*cq3 - sq3*cq2)*((tL / 2)*q7 + (tL / 2)*q8) + q5 + (cL / 2) *q7*sq2;
eq12 = -vAb_y + (-sq2*sq3 + cq2*cq3)*((tL / 2)*q7 + (tL / 2)*q8) + q6 - (cL / 2) *q7*cq2;

print "velocity kinematics (A)"
soln_A = solve([eq9, eq10, eq11, eq12], [q5, q6, q7, q8])
print soln_A


eq13 = -vBa_x + (-sq2*cq4 - sq4*cq2)*(-(tL / 2)*q7 - (tL / 2)*q9) + q5 - (cL / 2) *q7*sq2;
eq14 = -vBa_y + (-sq2*sq4 + cq2*cq4)*(-(tL / 2)*q7 - (tL / 2)*q9) + q6 + (cL / 2) *q7*cq2;

eq15 = -vBb_x + (-sq2*cq4 - sq4*cq2)*((tL / 2)*q7 + (tL / 2)*q9) + q5 - (cL / 2) *q7*sq2;
eq16 = -vBb_y + (-sq2*sq4 + cq2*cq4)*((tL / 2)*q7 + (tL / 2)*q9) + q6 + (cL / 2) *q7*cq2;



print "velocity kinematics (B)"
soln_B = solve([eq13, eq14, eq15, eq16], [q5, q6, q7, q9])
print soln_B


print "velocity kinematics (all)"
print solve([eq9 - eq10, eq11 - eq12, eq13 - eq14, eq15 - eq16], [q5, q6, q7, q8, q9])

#fh = open("vel_out.c", 'w')

#fh.write(ccode(soln_A))
#fh.write(ccode(soln_B))

