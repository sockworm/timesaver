 
#ifndef TRAINCARTEST_H
#define TRAINCARTEST_H
    
#include<iostream>
#include<tr1/array>
    
// Categories of bodies
enum _entityCategory {
    RAIL =     0x0001,
    WHEEL =    0x0002,
    CARBODY =  0x0004,
    COUPLER =  0x0006,
    BUMPER =   0x0008,
};
    


// Coupler contact class
class CouplerContactListener : public b2ContactListener {
    
    std::tr1::array<b2Fixture*, 5> myFixture_A;
    std::tr1::array<b2Fixture*, 5> myFixture_B;
    
    int num_contacts;
    
        
public:
    

    
    friend class TrainCarTest;
    
    void BeginContact(b2Contact* contact) {
    
        num_contacts = 0;
        
        bool fixA_isCoupler = false;
        bool fixB_isCoupler = false;
        
        b2Fixture* fixtureA = contact->GetFixtureA();
        b2Fixture* fixtureB = contact->GetFixtureB();
        
        b2Filter filterA = fixtureA->GetFilterData();
        b2Filter filterB = fixtureB->GetFilterData();
        
        fixA_isCoupler = (COUPLER == filterA.categoryBits);
        fixB_isCoupler = (COUPLER == filterB.categoryBits);
        
        if (fixA_isCoupler && fixB_isCoupler) {
            // Make the couplers lock
            
//             std::cout << "COUPLER!" << std::endl;

            
            myFixture_A[num_contacts] = fixtureA;
            myFixture_B[num_contacts] = fixtureB;
//             myFixture_A = fixtureA;
//             myFixture_B = fixtureB;
            
            ++num_contacts;
            

            
        }
        
       

    }
    
    int check_for_contacts() {
        
        return num_contacts;
        
    }
    
    std::tr1::array<b2Fixture*, 5> get_fixA() const { return myFixture_A; }
    std::tr1::array<b2Fixture*, 5> get_fixB() const { return myFixture_B; }
        
    
};



class TrainCarTest : public Test
{
    
    public:
        
    friend class CouplerContactListener;
        
    b2Body* carBody[5];
    b2Body* truckBody_a;
    b2Body* truckBody_b;
    b2Body* wheelBody;
    b2Body* couplerBody_a;
    b2Body* couplerBody_b;
    
//     b2RevoluteJoint* w_joint_a;
//     b2RevoluteJoint* w_joint_b;
//     b2RevoluteJoint* w_joint_c;
//     b2RevoluteJoint* w_joint_d;
    b2RevoluteJoint* coupler_joint_a[5];
    b2RevoluteJoint* coupler_joint_b[5];
    
    CouplerContactListener myContactListener;
    
 
    
    void addWheel(b2Body* tb, b2BodyDef bd, b2FixtureDef fd, float x, float y, float xl, float yl) {
        
        
        b2RevoluteJointDef revoluteJointDef;
        b2RevoluteJoint* w_joint;

        fd.filter.categoryBits = WHEEL;
        fd.filter.maskBits = RAIL;
        
        
        bd.position.Set(x, y);
        wheelBody = m_world->CreateBody(&bd);
        wheelBody->CreateFixture(&fd);
        
        revoluteJointDef.bodyA = tb;
        revoluteJointDef.bodyB = wheelBody;
        revoluteJointDef.collideConnected = false;
        revoluteJointDef.localAnchorA.Set(xl, yl);
        revoluteJointDef.localAnchorB.Set(0,0);
        
        w_joint = (b2RevoluteJoint*)m_world->CreateJoint(&revoluteJointDef);
        
        
    }
    
    void addCoupler(b2Body* &couplerBody, float x, float y) {
    
        b2BodyDef BodyDef;
        BodyDef.type = b2_dynamicBody;
        b2FixtureDef fixtureDef;
        fixtureDef.density = 4000.0;
        
        
        b2PolygonShape boxShape;
        // FIXME: set coupler dimensions
        boxShape.SetAsBox(0.6, 0.2);
        
        BodyDef.position.Set(x, y);
        BodyDef.angle = 0;
        BodyDef.gravityScale = 0.0f;
        
        fixtureDef.shape = &boxShape;
        
        // collide with other cars, and bumpers
        fixtureDef.filter.categoryBits = COUPLER;
        fixtureDef.filter.maskBits = COUPLER | CARBODY | BUMPER;
        
        couplerBody = m_world->CreateBody(&BodyDef);
        couplerBody->CreateFixture(&fixtureDef);
        
        bool isCoupled = false;
        couplerBody->SetUserData( (void*)isCoupled);
        
    }
    
    void addTruck(b2Body* &truckBody, float x, float y) {
    

        
        b2BodyDef BodyDef;
        BodyDef.type = b2_dynamicBody;
        b2FixtureDef fixtureDef;
        fixtureDef.density = 4268.1224;

//         b2RevoluteJointDef revoluteJointDef;
        
        // truck shape and wheel shape
        b2PolygonShape boxShape;
        boxShape.SetAsBox(1.37, 0.71755);
        
        b2CircleShape circleShape;
        circleShape.m_radius = 0.25;
        
        
        
        // Create the main truck body
        BodyDef.position.Set(x, y);
        BodyDef.angle = 0;
        BodyDef.gravityScale = 0.0f;
        
        fixtureDef.shape = &boxShape;
        
        // collide with nothing!
        fixtureDef.filter.categoryBits = 0;
        fixtureDef.filter.maskBits = 0;
        
        truckBody = m_world->CreateBody(&BodyDef);
        truckBody->CreateFixture(&fixtureDef);

        

        
        // switch to wheel shape
        fixtureDef.shape = &circleShape;
//         fixtureDef.density = 4268.1224;


        // Add wheels
        addWheel(truckBody, BodyDef, fixtureDef, x + 1.37, y - 0.4676, 1.37, -0.4676);
        addWheel(truckBody, BodyDef, fixtureDef, x + 1.37, y + 0.4676, 1.37, 0.4676);
        addWheel(truckBody, BodyDef, fixtureDef, x - 1.37, y - 0.4676, -1.37, -0.4676);
        addWheel(truckBody, BodyDef, fixtureDef, x - 1.37, y + 0.4676, -1.37, 0.4676);
    
    }
    
    
    void addMainBody(b2Body* &mainBody, float x, float y, int idx) {
    

        
        b2BodyDef bd;
        
        
        b2RevoluteJointDef revoluteJointDef;
        b2RevoluteJoint* w_joint;
        
        bd.type = b2_dynamicBody;
        b2FixtureDef fd;
        fd.density = 1365.5704;
        
        
        
        // body shape
        b2PolygonShape boxShape;
//         boxShape.SetAsBox(9.017, 0.1);
        boxShape.SetAsBox(9.017, 1.5748);
        // FIXME: filter collisions and then make this bigger
        
        // FIXME: uint16
        // Set the category and mask bits to only collide with other car bodies
        fd.filter.categoryBits = CARBODY;
        fd.filter.maskBits = CARBODY | BUMPER;
        
        
        // Create the main car body
        bd.position.Set(x, y);
        bd.angle = 0;
        bd.gravityScale = 0.0f;
        
        fd.shape = &boxShape;
        mainBody = m_world->CreateBody(&bd);
        mainBody->CreateFixture(&fd);

        
        // Add trucks
        addTruck(truckBody_a, x - 6.5532, y);
        addTruck(truckBody_b, x + 6.5532, y);
        
        // Add joints
        revoluteJointDef.bodyA = mainBody;
        revoluteJointDef.bodyB = truckBody_a;
        revoluteJointDef.collideConnected = false;
        revoluteJointDef.localAnchorA.Set(-6.5532, 0.0);
        revoluteJointDef.localAnchorB.Set(0,0);
        
        w_joint = (b2RevoluteJoint*)m_world->CreateJoint(&revoluteJointDef);
        
        
        revoluteJointDef.bodyA = mainBody;
        revoluteJointDef.bodyB = truckBody_b;
        revoluteJointDef.collideConnected = false;
        revoluteJointDef.localAnchorA.Set(6.5532, 0.0);
        revoluteJointDef.localAnchorB.Set(0, 0);
        
        w_joint = (b2RevoluteJoint*)m_world->CreateJoint(&revoluteJointDef);
    
        
        
        
        // Add couplers
        addCoupler(couplerBody_a, x - 7.0, y);
        addCoupler(couplerBody_b, x + 7.0, y);
        
        

        // Add coupler joints
        revoluteJointDef.collideConnected = false;
        revoluteJointDef.lowerAngle = -0.10f * b2_pi;
        revoluteJointDef.upperAngle = 0.10f * b2_pi;
        revoluteJointDef.enableLimit = true;
        revoluteJointDef.maxMotorTorque = 1000.0f;
        revoluteJointDef.motorSpeed = 0.0f;
        revoluteJointDef.enableMotor = true;

        revoluteJointDef.bodyA = mainBody;
        
        
        revoluteJointDef.bodyB = couplerBody_a;
        revoluteJointDef.localAnchorA.Set(-8.5, 0.0);
        revoluteJointDef.localAnchorB.Set(0.6, 0.0);
        
        
        coupler_joint_a[idx] = (b2RevoluteJoint*)m_world->CreateJoint(&revoluteJointDef);
        // FIXME: set pointers for each coupler joint
        
        revoluteJointDef.bodyB = couplerBody_b;
        revoluteJointDef.localAnchorA.Set(8.5, 0.0);
        revoluteJointDef.localAnchorB.Set(-0.6, 0.0);
        
        coupler_joint_b[idx] = (b2RevoluteJoint*)m_world->CreateJoint(&revoluteJointDef);
        
    }

    
    
    void addStraightSegment(b2Vec2 sp, b2Vec2 ep) {
    
        // Make the rails
        b2BodyDef bd;
        b2EdgeShape edge;

        // half width of rail is 0.71755
        
        
        // Calculate direction
        b2Vec2 dir = ep - sp;
        float dir_norm = sqrt(dir.x * dir.x + dir.y * dir.y);
        dir.Set(dir.x / dir_norm, dir.y / dir_norm);
        
        // rotate pi/2 to get segment normal direction
        dir.Set(-dir.y, dir.x);
        
        
        const float rail_width_2 = 0.71755;
        
        // build endpoints
        b2Vec2 sp_a(sp.x + dir.x * rail_width_2, sp.y + dir.y * rail_width_2);
        b2Vec2 ep_a(ep.x + dir.x * rail_width_2, ep.y + dir.y * rail_width_2);
        
        b2Vec2 sp_b(sp.x - dir.x * rail_width_2, sp.y - dir.y * rail_width_2);
        b2Vec2 ep_b(ep.x - dir.x * rail_width_2, ep.y - dir.y * rail_width_2);
        
        // build edges
        b2Body* rail_a = m_world->CreateBody(&bd);
        edge.Set(sp_a, ep_a);
        rail_a->CreateFixture(&edge, 0.0f);
        
        b2Body* rail_b = m_world->CreateBody(&bd);
        edge.Set(sp_b, ep_b);
        rail_b->CreateFixture(&edge, 0.0f);
        
    }
    
    void addCurvedSegment(bool LHRH, b2Vec2 pt_start, float gamma, b2Vec2 pt_center) {
        
        
        b2BodyDef bd;
        b2ChainShape chain_a;
        b2ChainShape chain_b;
        
        
        const float eps_a = fabs(gamma / 100.0f);
        
        const float rail_width_2 = 0.71755;

        int idx_a = 0;
        b2Vec2 tmp_a[101];
        b2Vec2 tmp_b[101];
        
        b2Vec2 rad_init = pt_start - pt_center; // initial radius vector
        float curve_radius = sqrt(rad_init.x * rad_init.x + rad_init.y * rad_init.y);
        b2Vec2 dir_init(rad_init.x / curve_radius, rad_init.y / curve_radius);
        
        b2Vec2 dirvec; // direction vector for current angle
        
        b2Vec2 radvec_a; // radius vector for current angle (left rail)
        b2Vec2 radvec_b; // radius vector for current angle (right rail)
        // at least with left handed curves

        
        if (LHRH) {
            
            if (gamma > eps_a) {
                for (float incr_a = 0.0; incr_a <= gamma; incr_a += eps_a) {
                
                    dirvec = calc_rotate(dir_init, incr_a);
                    
                    radvec_a.Set(dirvec.x * (curve_radius - rail_width_2),
                                 dirvec.y * (curve_radius - rail_width_2));
                    radvec_b.Set(dirvec.x * (curve_radius + rail_width_2),
                                 dirvec.y * (curve_radius + rail_width_2));
                    
                    tmp_a[idx_a] = pt_center + radvec_a;
                    tmp_b[idx_a] = pt_center + radvec_b;
                    ++idx_a;
               
                }
            }
        }
        else {
            
            if (gamma < -eps_a) {
                for (float incr_a = 0.0; incr_a >= gamma; incr_a -= eps_a) {
                
                    dirvec = calc_rotate(dir_init, incr_a);
                    radvec_a.Set(dirvec.x * (curve_radius - rail_width_2),
                                 dirvec.y * (curve_radius - rail_width_2));
                    radvec_b.Set(dirvec.x * (curve_radius + rail_width_2),
                                 dirvec.y * (curve_radius + rail_width_2));
                    
                    tmp_a[idx_a] = pt_center + radvec_a;
                    tmp_b[idx_a] = pt_center + radvec_b;
                    ++idx_a;
                }
                
            }
            
        }
        
                       
        // Create the left rail
        b2Body* rail_a = m_world->CreateBody(&bd);
        chain_a.CreateChain(tmp_a, idx_a);
        rail_a->CreateFixture(&chain_a, 0.0f);
        
        // Create the right rail
        b2Body* rail_b = m_world->CreateBody(&bd);
        chain_b.CreateChain(tmp_b, idx_a);
        rail_b->CreateFixture(&chain_b, 0.0f);
        
    }
    
    
    b2Vec2 calc_rotate(b2Vec2 in, float th) {
        // rotate a vector by angle theta
     
        
        
        b2Vec2 tmp(in.x * cos(th) - in.y * sin(th),
                   in.x * sin(th) + in.y * cos(th));
        
        // normalize it, just in case
        float dist = sqrt(tmp.x * tmp.x + tmp.y * tmp.y);
        
        b2Vec2 out(tmp.x / dist, tmp.y / dist);
        
        return out;
    }
    
    
    void addBumper(b2Vec2 bp, float phi) {
        // Add a bumper at the specified location
        
        b2BodyDef bd;
        bd.type = b2_staticBody;
        bd.position = bp;
        bd.angle = phi;
        
        b2Body* bumperBody = m_world->CreateBody(&bd);
        
        b2PolygonShape boxShape;
        boxShape.SetAsBox(0.4, 0.8);
        
        b2FixtureDef bumperFixtureDef;
        bumperFixtureDef.shape = &boxShape;
        
        bumperFixtureDef.filter.categoryBits = BUMPER;
        
        bumperBody->CreateFixture(&bumperFixtureDef);
        
    }
    
    
    TrainCarTest() {

        
        addMainBody(carBody[0], -3.5, 20.0, 0);
        addMainBody(carBody[1], -25.0, 20.0, 1);
        
      
        b2Vec2 v1(-40.0, 20.0);
        b2Vec2 v2(5.0, 20.0);
        addStraightSegment(v1, v2);
        
        
        v1.Set(5.0, 45.0);
        addCurvedSegment(true, v2, 1.5708f, v1);

        v1.Set(-40.5, 20.0);
        addBumper(v1, 0.0);
        
        v1.Set(30.0, 45.0);
        addBumper(v1, 1.5708f);
        
        
//         myContactListener.give_world(m_world);
        m_world->SetContactListener(&myContactListener);
        
//         // Another set of rails
//         v1.Set(-5.0f, 20.7256f);
//         v2.Set(15.0f, 20.7256f - 2.0f);
//         
//         rail = m_world->CreateBody(&bd);
//         edge.Set(v1, v2);
//         rail->CreateFixture(&edge, 0.0f);
//         
//         v1.Set(-5.0f, 19.2745f);
//         v2.Set(15.0f, 19.2745f - 2.0f);
//         
//         rail = m_world->CreateBody(&bd);
//         edge.Set(v1, v2);
//         rail->CreateFixture(&edge, 0.0f);
        
    }
    
    void Step(Settings* settings) {
        //run the default physics and rendering
        Test::Step(settings); 
    
        
        // check for collisions
        int nc = 0;
        std::tr1::array<b2Fixture*, 5> fixA;
        std::tr1::array<b2Fixture*, 5> fixB;
        
        nc = myContactListener.check_for_contacts();
        
        fixA = myContactListener.get_fixA();
        fixB = myContactListener.get_fixB();
        
        if (nc > 0) {
//             std::cout << nc << " contacts detected" << std::endl;
         
            for (int incr_c = 0; incr_c < nc; ++incr_c) {
                
            
                b2Body* bodyA = fixA[incr_c]->GetBody();
                b2Body* bodyB = fixB[incr_c]->GetBody();
            
                
                // Check if these bodies are already joined
                bool bodyA_isCoupled = (bool)bodyA->GetUserData();
                bool bodyB_isCoupled = (bool)bodyB->GetUserData();
                
                
                if (!bodyA_isCoupled && !bodyB_isCoupled) {
                
                    b2Vec2 anchor(0.3f, 0.0f);
                    anchor += bodyA->GetPosition();
                    
            
                
                    b2WeldJointDef jd;

                    jd.Initialize(bodyA, bodyB, anchor);
                    m_world->CreateJoint(&jd);
                  
                    bodyA_isCoupled = true;
                    bodyB_isCoupled = true;
                    
                    bodyA->SetUserData( (void*)bodyA_isCoupled);
                    bodyB->SetUserData( (void*)bodyB_isCoupled);
                }
            }
            
        }
        
        
        
        // drive couplers to zero
        
        float32 gain = 30.0f;

        // step through each car, and each coupler joint

        for (int incr_c = 0; incr_c < 2; ++incr_c) {
        
            float32 angleError = coupler_joint_a[incr_c]->GetJointAngle();
            coupler_joint_a[incr_c]->SetMotorSpeed(-gain * angleError);
            
            angleError = coupler_joint_b[incr_c]->GetJointAngle();
            coupler_joint_b[incr_c]->SetMotorSpeed(-gain * angleError);
            
        }
        
        
        //show some text in the main screen
        m_debugDraw.DrawString(5, m_textLine, "Now we have a train car test");
        m_textLine += 15;
        
        b2Vec2 pos = carBody[0]->GetPosition();
        float angle = carBody[0]->GetAngle();
        b2Vec2 vel = carBody[0]->GetLinearVelocity();
        float angularVel = carBody[0]->GetAngularVelocity();
        
        m_debugDraw.DrawString(5, m_textLine, "Position: %.4f, %.4f Angle: %.4f", pos.x, pos.y, angle);
        
        m_textLine += 15;
        
        m_debugDraw.DrawString(5, m_textLine, "Velocity: %.3f, %.3f Angular velocity: %.3f", vel.x, vel.y, angularVel);
        
        m_textLine += 15;
        
    }
    
    static Test* Create()
    {
        return new TrainCarTest;
    }
};
  




#endif