
# Daniel Montrallo Flickinger, PhD
# February 2015

"""
Kinematics for a single train car
"""


import numpy as np

import sympy
import sympy.physics.mechanics as mech


from sympy.physics.vector import *
from sympy.matrices import Matrix

# For printing out c code
from sympy import ccode

from sympy import Symbol


## Configuration parameters

## Positions
#q0 = Symbol('q0') # X for body center
#q1 = Symbol('q1') # Y for body center
#q2 = Symbol('q2') # phi (rotation) for body center
#q3 = Symbol('q3') # phi_A (rotation) for truck A
#q4 = Symbol('q4') # phi_B (rotation) for truck B

## Velocities
#q5 = Symbol('q5') # X velocity for body center
#q6 = Symbol('q6') # Y velocity for body center
#q7 = Symbol('q7') # omega (rotation) for body center
#q8 = Symbol('q8') # omega_A (rotation) for truck A
#q9 = Symbol('q9') # omega_B (rotation) for truck B



# Car parameters
cL = Symbol('cL') # car length (bolster to bolster)
tL = Symbol('tL') # truck length

    

# w x r